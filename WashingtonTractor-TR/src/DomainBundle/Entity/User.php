<?php

namespace DomainBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;

class User extends BaseUser
{
    const STATUS_NEW                = 'NEW';
    const STATUS_REGISTERED         = 'REGISTERED';
    const STATUS_DECLINED           = 'DECLINED';
    const ROLE_MANAGER_SALE         = 'ROLE_MANAGER_SALE';
    const ROLE_MANAGER_STORE        = 'ROLE_MANAGER_STORE';
    const ROLE_ADMIN                = 'ROLE_ADMIN';
    const ROLE_DRIVER               = 'ROLE_DRIVER';
    const ROLE_TRANSFER_COORDINATOR = 'ROLE_TRANSFER_COORDINATOR';

    /**
     * @var integer
     */
    protected $id;

    /**
     * @var integer
     */
    private $isDeleted;

    /**
     * @var string
     */
    private $firstName;

    /**
     * @var string
     */
    private $lastName;

    /**
     * @var string
     */
    private $status;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $stores;

    /**
     * @var array
     */
    protected $roles = [];

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->stores = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get isDeleted
     *
     * @return integer
     */
    public function getIsDeleted()
    {
        return $this->isDeleted;
    }

    /**
     * Set isDeleted
     *
     * @param integer $isDeleted
     *
     * @return User
     */
    public function setIsDeleted($isDeleted)
    {
        $this->isDeleted = $isDeleted;

        return $this;
    }

    /**
     * Get firstName
     *
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Set firstName
     *
     * @param string $firstName
     *
     * @return User
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * Get lastName
     *
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * Set lastName
     *
     * @param string $lastName
     *
     * @return User
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * Add store
     *
     * @param \DomainBundle\Entity\Store\Store $store
     *
     * @return User
     */
    public function addStore(\DomainBundle\Entity\Store\Store $store)
    {
        $this->stores[] = $store;

        return $this;
    }

    /**
     * Remove store
     *
     * @param \DomainBundle\Entity\Store\Store $store
     */
    public function removeStore(\DomainBundle\Entity\Store\Store $store)
    {
        $this->stores->removeElement($store);
    }

    /**
     * Get stores
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getStores()
    {
        return $this->stores;
    }

    /**
     * has store
     *
     * @param \DomainBundle\Entity\Store\Store $store
     * @return bool
     */
    public function hasStore(\DomainBundle\Entity\Store\Store $store)
    {
        foreach ($this->getStores() as $s) {
            if ($s->getId() === $store->getId()) {
                return true;
            }
        }


        return false;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set status
     *
     * @param string $status
     *
     * @return User
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }
}
