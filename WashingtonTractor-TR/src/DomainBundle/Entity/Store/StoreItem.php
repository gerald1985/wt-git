<?php

namespace DomainBundle\Entity\Store;

/**
 * StoreItem
 */
class StoreItem
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var \DomainBundle\Entity\Item\Item
     */
    private $item;

    /**
     * @var \DomainBundle\Entity\Store\Store
     */
    private $store;
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $transferRequests;
    /**
     * @var integer
     */
    private $stockNumber;
    /**
     * @var integer
     */
    private $meter;
    /**
     * @var integer
     */
    private $daysInStock;
    /**
     * @var integer
     */
    private $salesPerson;
    /**
     * @var string
     */
    private $usage;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->transferRequests = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get item
     *
     * @return \DomainBundle\Entity\Item\Item
     */
    public function getItem()
    {
        return $this->item;
    }

    /**
     * Set item
     *
     * @param \DomainBundle\Entity\Item\Item $item
     *
     * @return StoreItem
     */
    public function setItem(\DomainBundle\Entity\Item\Item $item = null)
    {
        $this->item = $item;

        return $this;
    }

    /**
     * Get store
     *
     * @return \DomainBundle\Entity\Store\Store
     */
    public function getStore()
    {
        return $this->store;
    }

    /**
     * Set store
     *
     * @param \DomainBundle\Entity\Store\Store $store
     *
     * @return StoreItem
     */
    public function setStore(\DomainBundle\Entity\Store\Store $store = null)
    {
        $this->store = $store;

        return $this;
    }

    /**
     * Add transferRequest
     *
     * @param \DomainBundle\Entity\TransferRequest\TransferRequest $transferRequest
     *
     * @return StoreItem
     */
    public function addTransferRequest(\DomainBundle\Entity\TransferRequest\TransferRequest $transferRequest)
    {
        $this->transferRequests[] = $transferRequest;

        return $this;
    }

    /**
     * Remove transferRequest
     *
     * @param \DomainBundle\Entity\TransferRequest\TransferRequest $transferRequest
     */
    public function removeTransferRequest(\DomainBundle\Entity\TransferRequest\TransferRequest $transferRequest)
    {
        $this->transferRequests->removeElement($transferRequest);
    }

    /**
     * Get transferRequests
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTransferRequests()
    {
        return $this->transferRequests;
    }

    /**
     * Get stockNumber
     *
     * @return integer
     */
    public function getStockNumber()
    {
        return $this->stockNumber;
    }

    /**
     * Set stockNumber
     *
     * @param integer $stockNumber
     *
     * @return StoreItem
     */
    public function setStockNumber($stockNumber)
    {
        $this->stockNumber = $stockNumber;

        return $this;
    }

    /**
     * Get meter
     *
     * @return integer
     */
    public function getMeter()
    {
        return $this->meter;
    }

    /**
     * Set meter
     *
     * @param integer $meter
     *
     * @return StoreItem
     */
    public function setMeter($meter)
    {
        $this->meter = $meter;

        return $this;
    }

    /**
     * Get daysInStock
     *
     * @return integer
     */
    public function getDaysInStock()
    {
        return $this->daysInStock;
    }

    /**
     * Set daysInStock
     *
     * @param integer $daysInStock
     *
     * @return StoreItem
     */
    public function setDaysInStock($daysInStock)
    {
        $this->daysInStock = $daysInStock;

        return $this;
    }

    /**
     * Get salesPerson
     *
     * @return integer
     */
    public function getSalesPerson()
    {
        return $this->salesPerson;
    }

    /**
     * Set salesPerson
     *
     * @param integer $salesPerson
     *
     * @return StoreItem
     */
    public function setSalesPerson($salesPerson)
    {
        $this->salesPerson = $salesPerson;

        return $this;
    }

    /**
     * Get usage
     *
     * @return string
     */
    public function getUsage()
    {
        return $this->usage;
    }

    /**
     * Set usage
     *
     * @param string $usage
     *
     * @return StoreItem
     */
    public function setUsage($usage)
    {
        $this->usage = $usage;

        return $this;
    }
}
