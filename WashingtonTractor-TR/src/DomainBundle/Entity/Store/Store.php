<?php

namespace DomainBundle\Entity\Store;

/**
 * Store
 */
class Store
{
    const STATUS_ACTIVE   = 'ACTIVE';
    const STATUS_INACTIVE = 'INACTIVE';
    const IS_DELETED  = 1;

    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $status;

    /**
     * @var string
     */
    private $name;

    /**
     * @var integer
     */
    private $number;

    /**
     * @var string
     */
    private $address;

    /**
     * @var \DomainBundle\Entity\User
     */
    private $manager;
    /**
     * @var integer
     */
    private $isDeleted = 0;
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $employees;
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $storeItems;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->employees = new \Doctrine\Common\Collections\ArrayCollection();
        $this->storeItems = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set status
     *
     * @param string $status
     *
     * @return Store
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Store
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get number
     *
     * @return integer
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * Set number
     *
     * @param integer $number
     *
     * @return Store
     */
    public function setNumber($number)
    {
        $this->number = $number;

        return $this;
    }

    /**
     * Get address
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set address
     *
     * @param string $address
     *
     * @return Store
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get manager
     *
     * @return \DomainBundle\Entity\User
     */
    public function getManager()
    {
        return $this->manager;
    }

    /**
     * Set manager
     *
     * @param \DomainBundle\Entity\User $manager
     *
     * @return Store
     */
    public function setManager(\DomainBundle\Entity\User $manager = null)
    {
        $this->manager = $manager;

        return $this;
    }

    /**
     * Get isDeleted
     *
     * @return integer
     */
    public function getIsDeleted()
    {
        return $this->isDeleted;
    }

    /**
     * Set isDeleted
     *
     * @param integer $isDeleted
     *
     * @return Store
     */
    public function setIsDeleted($isDeleted)
    {
        $this->isDeleted = $isDeleted;

        return $this;
    }

    /**
     * Add employee
     *
     * @param \DomainBundle\Entity\User $employee
     *
     * @return Store
     */
    public function addEmployee(\DomainBundle\Entity\User $employee)
    {
        $this->employees[] = $employee;

        return $this;
    }

    /**
     * Remove employee
     *
     * @param \DomainBundle\Entity\User $employee
     */
    public function removeEmployee(\DomainBundle\Entity\User $employee)
    {
        $this->employees->removeElement($employee);
    }

    /**
     * Get employees
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEmployees()
    {
        return $this->employees;
    }

    /**
     * Add storeItem
     *
     * @param \DomainBundle\Entity\Store\StoreItem $storeItem
     *
     * @return Store
     */
    public function addStoreItem(\DomainBundle\Entity\Store\StoreItem $storeItem)
    {
        $this->storeItems[] = $storeItem;

        return $this;
    }

    /**
     * Remove storeItem
     *
     * @param \DomainBundle\Entity\Store\StoreItem $storeItem
     */
    public function removeStoreItem(\DomainBundle\Entity\Store\StoreItem $storeItem)
    {
        $this->storeItems->removeElement($storeItem);
    }

    /**
     * Get storeItems
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getStoreItems()
    {
        return $this->storeItems;
    }
}
