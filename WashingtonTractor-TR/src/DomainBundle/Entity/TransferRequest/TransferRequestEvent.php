<?php

namespace DomainBundle\Entity\TransferRequest;

/**
 * TransferRequestEvent
 */
class TransferRequestEvent
{
    const EVENT_STATUS_CHANGED           = 'STATUS_CHANGED';
    const EVENT_NOTE                     = 'NOTE';
    const EVENT_EXPIRED                  = 'EXPIRED';
    const EVENT_PLACEMENT                = 'PLACEMENT';
    const EVENT_UPDATED                  = 'UPDATED';
    const EVENT_RESUBMITTED              = 'RESUBMITTED';

    /**
     * @var integer
     */
    private $id;

    /**
     * @var \DateTime
     */
    private $eventDatetime;

    /**
     * @var string
     */
    private $eventType;

    /**
     * @var string
     */
    private $note;

    /**
     * @var \DomainBundle\Entity\TransferRequest\TransferRequest
     */
    private $transferRequest;

    /**
     * @var \DomainBundle\Entity\User
     */
    private $addedBy;
    /**
     * @var string
     */
    private $message;
    /**
     * @var string
     */
    private $historyJson;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get eventDatetime
     *
     * @return \DateTime
     */
    public function getEventDatetime()
    {
        return $this->eventDatetime;
    }

    /**
     * Set eventDatetime
     *
     * @param \DateTime $eventDatetime
     *
     * @return TransferRequestEvent
     */
    public function setEventDatetime($eventDatetime)
    {
        $this->eventDatetime = $eventDatetime;

        return $this;
    }

    /**
     * Get eventType
     *
     * @return string
     */
    public function getEventType()
    {
        return $this->eventType;
    }

    /**
     * Set eventType
     *
     * @param string $eventType
     *
     * @return TransferRequestEvent
     */
    public function setEventType($eventType)
    {
        $this->eventType = $eventType;

        return $this;
    }

    /**
     * Get note
     *
     * @return string
     */
    public function getNote()
    {
        return $this->note;
    }

    /**
     * Set note
     *
     * @param string $note
     *
     * @return TransferRequestEvent
     */
    public function setNote($note)
    {
        $this->note = $note;

        return $this;
    }

    /**
     * Get transferRequest
     *
     * @return \DomainBundle\Entity\TransferRequest\TransferRequest
     */
    public function getTransferRequest()
    {
        return $this->transferRequest;
    }

    /**
     * Set transferRequest
     *
     * @param \DomainBundle\Entity\TransferRequest\TransferRequest $transferRequest
     *
     * @return TransferRequestEvent
     */
    public function setTransferRequest(\DomainBundle\Entity\TransferRequest\TransferRequest $transferRequest = null)
    {
        $this->transferRequest = $transferRequest;

        return $this;
    }

    /**
     * Get addedBy
     *
     * @return \DomainBundle\Entity\User
     */
    public function getAddedBy()
    {
        return $this->addedBy;
    }

    /**
     * Set addedBy
     *
     * @param \DomainBundle\Entity\User $addedBy
     *
     * @return TransferRequestEvent
     */
    public function setAddedBy(\DomainBundle\Entity\User $addedBy = null)
    {
        $this->addedBy = $addedBy;

        return $this;
    }

    /**
     * Get message
     *
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * Set message
     *
     * @param string $message
     *
     * @return TransferRequestEvent
     */
    public function setMessage($message)
    {
        $this->message = $message;

        return $this;
    }

    /**
     * Get historyJson
     *
     * @return string
     */
    public function getHistoryJson()
    {
        return $this->historyJson;
    }

    /**
     * Set historyJson
     *
     * @param string $historyJson
     *
     * @return TransferRequestEvent
     */
    public function setHistoryJson($historyJson)
    {
        $this->historyJson = $historyJson;

        return $this;
    }
}
