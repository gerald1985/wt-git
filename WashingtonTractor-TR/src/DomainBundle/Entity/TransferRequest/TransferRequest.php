<?php

namespace DomainBundle\Entity\TransferRequest;

/**
 * TransferRequest
 */
class TransferRequest
{
    const STATUS_PENDING   = 'PENDING';
    const STATUS_APPROVED  = 'APPROVED';
    const STATUS_DECLINED  = 'DECLINED';
    const STATUS_CANCELLED = 'CANCELLED';

    const PURPOSE_EQUIPMENT_SALE = 'EQUIPMENT_SALE';
    const PURPOSE_RENTAL         = 'RENTAL';
    const PURPOSE_DEMO           = 'DEMO';
    const PURPOSE_STOCK_LEVELING = 'STOCK_LEVELING';


    /**
     * @var integer
     */
    private $id;

    /**
     * @var \DateTime
     */
    private $requestDatetime;

    /**
     * @var string
     */
    private $purpose;

    /**
     * @var string
     */
    private $status;

    /**
     * @var \DateTime
     */
    private $shipmentDate;

    /**
     * @var \DomainBundle\Entity\User
     */
    private $requestedBy;

    /**
     * @var \DomainBundle\Entity\Store\StoreItem
     */
    private $storeItem;
    /**
     * @var \DomainBundle\Entity\Store\Store
     */
    private $storeReceiver;
    /**
     * @var string
     */
    private $receiverUserName;
    /**
     * @var integer
     */
    private $getEmailUpdates;
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $events;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->events = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get requestDatetime
     *
     * @return \DateTime
     */
    public function getRequestDatetime()
    {
        return $this->requestDatetime;
    }

    /**
     * Set requestDatetime
     *
     * @param \DateTime $requestDatetime
     *
     * @return TransferRequest
     */
    public function setRequestDatetime($requestDatetime)
    {
        $this->requestDatetime = $requestDatetime;

        return $this;
    }

    /**
     * Get purpose
     *
     * @return string
     */
    public function getPurpose()
    {
        return $this->purpose;
    }

    /**
     * Set purpose
     *
     * @param string $purpose
     *
     * @return TransferRequest
     */
    public function setPurpose($purpose)
    {
        $this->purpose = $purpose;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set status
     *
     * @param string $status
     *
     * @return TransferRequest
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get shipmentDate
     *
     * @return \DateTime
     */
    public function getShipmentDate()
    {
        return $this->shipmentDate;
    }

    /**
     * Set shipmentDate
     *
     * @param \DateTime $shipmentDate
     *
     * @return TransferRequest
     */
    public function setShipmentDate($shipmentDate)
    {
        $this->shipmentDate = $shipmentDate;

        return $this;
    }

    /**
     * Get requestedBy
     *
     * @return \DomainBundle\Entity\User
     */
    public function getRequestedBy()
    {
        return $this->requestedBy;
    }

    /**
     * Set requestedBy
     *
     * @param \DomainBundle\Entity\User $requestedBy
     *
     * @return TransferRequest
     */
    public function setRequestedBy(\DomainBundle\Entity\User $requestedBy)
    {
        $this->requestedBy = $requestedBy;

        return $this;
    }

    /**
     * Get storeItem
     *
     * @return \DomainBundle\Entity\Store\StoreItem
     */
    public function getStoreItem()
    {
        return $this->storeItem;
    }

    /**
     * Set storeItem
     *
     * @param \DomainBundle\Entity\Store\StoreItem $storeItem
     *
     * @return TransferRequest
     */
    public function setStoreItem(\DomainBundle\Entity\Store\StoreItem $storeItem = null)
    {
        $this->storeItem = $storeItem;

        return $this;
    }

    /**
     * Get storeReceiver
     *
     * @return \DomainBundle\Entity\Store\Store
     */
    public function getStoreReceiver()
    {
        return $this->storeReceiver;
    }

    /**
     * Set storeReceiver
     *
     * @param \DomainBundle\Entity\Store\Store $storeReceiver
     *
     * @return TransferRequest
     */
    public function setStoreReceiver(\DomainBundle\Entity\Store\Store $storeReceiver = null)
    {
        $this->storeReceiver = $storeReceiver;

        return $this;
    }

    /**
     * Get receiverUserName
     *
     * @return string
     */
    public function getReceiverUserName()
    {
        return $this->receiverUserName;
    }

    /**
     * Set receiverUserName
     *
     * @param string $receiverUserName
     *
     * @return TransferRequest
     */
    public function setReceiverUserName($receiverUserName)
    {
        $this->receiverUserName = $receiverUserName;

        return $this;
    }

    /**
     * Get getEmailUpdates
     *
     * @return integer
     */
    public function getGetEmailUpdates()
    {
        return $this->getEmailUpdates;
    }

    /**
     * Set getEmailUpdates
     *
     * @param integer $getEmailUpdates
     *
     * @return TransferRequest
     */
    public function setGetEmailUpdates($getEmailUpdates)
    {
        $this->getEmailUpdates = $getEmailUpdates;

        return $this;
    }

    /**
     * Add event
     *
     * @param \DomainBundle\Entity\TransferRequest\TransferRequestEvent $event
     *
     * @return TransferRequest
     */
    public function addEvent(\DomainBundle\Entity\TransferRequest\TransferRequestEvent $event)
    {
        $this->events[] = $event;

        return $this;
    }

    /**
     * Remove event
     *
     * @param \DomainBundle\Entity\TransferRequest\TransferRequestEvent $event
     */
    public function removeEvent(\DomainBundle\Entity\TransferRequest\TransferRequestEvent $event)
    {
        $this->events->removeElement($event);
    }

    /**
     * Get events
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEvents()
    {
        return $this->events;
    }
}
