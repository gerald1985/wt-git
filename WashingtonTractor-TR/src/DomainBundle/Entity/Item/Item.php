<?php

namespace DomainBundle\Entity\Item;

/**
 * Item
 */
class Item
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $pin;

    /**
     * @var string
     */
    private $tagNumber;

    /**
     * @var string
     */
    private $attachedTo;

    /**
     * @var integer
     */
    private $attachmentNumber;

    /**
     * @var string
     */
    private $make;

    /**
     * @var string
     */
    private $category;

    /**
     * @var string
     */
    private $modelName;

    /**
     * @var integer
     */
    private $yearMfrd;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $storesItems;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->storesItems = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set pin
     *
     * @param string $pin
     *
     * @return Item
     */
    public function setPin($pin)
    {
        $this->pin = $pin;

        return $this;
    }

    /**
     * Get pin
     *
     * @return string
     */
    public function getPin()
    {
        return $this->pin;
    }

    /**
     * Set tagNumber
     *
     * @param string $tagNumber
     *
     * @return Item
     */
    public function setTagNumber($tagNumber)
    {
        $this->tagNumber = $tagNumber;

        return $this;
    }

    /**
     * Get tagNumber
     *
     * @return string
     */
    public function getTagNumber()
    {
        return $this->tagNumber;
    }

    /**
     * Set attachedTo
     *
     * @param string $attachedTo
     *
     * @return Item
     */
    public function setAttachedTo($attachedTo)
    {
        $this->attachedTo = $attachedTo;

        return $this;
    }

    /**
     * Get attachedTo
     *
     * @return string
     */
    public function getAttachedTo()
    {
        return $this->attachedTo;
    }

    /**
     * Set attachmentNumber
     *
     * @param integer $attachmentNumber
     *
     * @return Item
     */
    public function setAttachmentNumber($attachmentNumber)
    {
        $this->attachmentNumber = $attachmentNumber;

        return $this;
    }

    /**
     * Get attachmentNumber
     *
     * @return integer
     */
    public function getAttachmentNumber()
    {
        return $this->attachmentNumber;
    }

    /**
     * Set make
     *
     * @param string $make
     *
     * @return Item
     */
    public function setMake($make)
    {
        $this->make = $make;

        return $this;
    }

    /**
     * Get make
     *
     * @return string
     */
    public function getMake()
    {
        return $this->make;
    }

    /**
     * Set category
     *
     * @param string $category
     *
     * @return Item
     */
    public function setCategory($category)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return string
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Set modelName
     *
     * @param string $modelName
     *
     * @return Item
     */
    public function setModelName($modelName)
    {
        $this->modelName = $modelName;

        return $this;
    }

    /**
     * Get modelName
     *
     * @return string
     */
    public function getModelName()
    {
        return $this->modelName;
    }

    /**
     * Set yearMfrd
     *
     * @param integer $yearMfrd
     *
     * @return Item
     */
    public function setYearMfrd($yearMfrd)
    {
        $this->yearMfrd = $yearMfrd;

        return $this;
    }

    /**
     * Get yearMfrd
     *
     * @return integer
     */
    public function getYearMfrd()
    {
        return $this->yearMfrd;
    }

    /**
     * Add storesItem
     *
     * @param \DomainBundle\Entity\Store\StoreItem $storesItem
     *
     * @return Item
     */
    public function addStoresItem(\DomainBundle\Entity\Store\StoreItem $storesItem)
    {
        $this->storesItems[] = $storesItem;

        return $this;
    }

    /**
     * Remove storesItem
     *
     * @param \DomainBundle\Entity\Store\StoreItem $storesItem
     */
    public function removeStoresItem(\DomainBundle\Entity\Store\StoreItem $storesItem)
    {
        $this->storesItems->removeElement($storesItem);
    }

    /**
     * Get storesItems
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getStoresItems()
    {
        return $this->storesItems;
    }
}
