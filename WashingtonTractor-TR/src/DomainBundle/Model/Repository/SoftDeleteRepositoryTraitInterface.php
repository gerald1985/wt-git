<?php


namespace DomainBundle\Model\Repository;


interface SoftDeleteRepositoryTraitInterface
{
    /**
     * @param string $alias
     * @param string|null $indexBy
     * @return mixed
     */
    public function softCreateQueryBuilder($alias, $indexBy = null);

    /**
     * Finds a single entity by a set of criteria.
     *
     * @param array $criteria
     * @param array $params
     * @param array $orderBy
     *
     * @return object|null The entity instance or NULL if the entity can not be found.
     */
    public function softFindOneBy(array $criteria, array $params = [], array $orderBy = []);

    /**
     * @param int $id
     * @return null|object
     */
    public function softFind($id);
}
