<?php


namespace DomainBundle\Model\Repository\Item;


use DomainBundle\Model\RepositoryInterface;


interface ItemRepositoryInterface extends RepositoryInterface
{
}
