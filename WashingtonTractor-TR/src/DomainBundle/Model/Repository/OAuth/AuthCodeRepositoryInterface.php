<?php


namespace DomainBundle\Model\Repository\OAuth;


use DomainBundle\Model\RepositoryInterface;


interface AuthCodeRepositoryInterface extends RepositoryInterface
{
}
