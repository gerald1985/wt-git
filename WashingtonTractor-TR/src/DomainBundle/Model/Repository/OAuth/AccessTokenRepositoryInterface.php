<?php


namespace DomainBundle\Model\Repository\OAuth;


use DomainBundle\Model\RepositoryInterface;


interface AccessTokenRepositoryInterface extends RepositoryInterface
{
}
