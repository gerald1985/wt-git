<?php


namespace DomainBundle\Model\Repository\OAuth;


use DomainBundle\Model\RepositoryInterface;


interface RefreshTokenRepositoryInterface extends RepositoryInterface
{
}
