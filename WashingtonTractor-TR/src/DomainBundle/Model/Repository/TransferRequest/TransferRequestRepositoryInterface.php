<?php


namespace DomainBundle\Model\Repository\TransferRequest;


use DomainBundle\Model\RepositoryInterface;


interface TransferRequestRepositoryInterface extends RepositoryInterface
{
}
