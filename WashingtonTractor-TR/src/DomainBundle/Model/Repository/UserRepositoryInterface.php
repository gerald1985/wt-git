<?php


namespace DomainBundle\Model\Repository;


use DomainBundle\Model\RepositoryInterface;


interface UserRepositoryInterface extends RepositoryInterface,SoftDeleteRepositoryTraitInterface
{
}
