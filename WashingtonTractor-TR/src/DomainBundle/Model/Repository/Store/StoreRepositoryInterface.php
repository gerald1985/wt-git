<?php


namespace DomainBundle\Model\Repository\Store;


use DomainBundle\Model\Repository\SoftDeleteRepositoryTraitInterface;
use DomainBundle\Model\RepositoryInterface;


interface StoreRepositoryInterface extends RepositoryInterface,SoftDeleteRepositoryTraitInterface
{
}
