<?php


namespace DomainBundle\Model\Repository\Store;


use DomainBundle\Model\RepositoryInterface;


interface StoreItemRepositoryInterface extends RepositoryInterface
{
}
