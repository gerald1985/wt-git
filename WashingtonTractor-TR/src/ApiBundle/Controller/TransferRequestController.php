<?php

namespace ApiBundle\Controller;

use Doctrine\ORM\Query;
use DomainBundle\Entity\TransferRequest\TransferRequest;
use FOS\RestBundle\Controller\FOSRestController;
use ServiceBundle\CommandBus\Command\TransferRequest\TransferRequestApproveCommand;
use ServiceBundle\CommandBus\Command\TransferRequest\TransferRequestCancelCommand;
use ServiceBundle\CommandBus\Command\TransferRequest\TransferRequestCreateCommand;
use ServiceBundle\CommandBus\Command\TransferRequest\TransferRequestDeclineCommand;
use ServiceBundle\CommandBus\Command\TransferRequest\TransferRequestEventReadListCommand;
use ServiceBundle\CommandBus\Command\TransferRequest\TransferRequestGetEquipmentExtractCommand;
use ServiceBundle\CommandBus\Command\TransferRequest\TransferRequestNoteCreateCommand;
use ServiceBundle\CommandBus\Command\TransferRequest\TransferRequestReadCommand;
use ServiceBundle\CommandBus\Command\TransferRequest\TransferRequestReadListCommand;
use ServiceBundle\CommandBus\Command\TransferRequest\TransferRequestResubmitCommand;
use ServiceBundle\CommandBus\Command\TransferRequest\TransferRequestUpdateCommand;
use ServiceBundle\Transformers\TransferRequestEventReadTransformer;
use ServiceBundle\Transformers\TransferRequestReadTransformer;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use InvalidArgumentException;

class TransferRequestController extends FOSRestController
{
    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function postTransferRequestAction(Request $request)
    {
        $command = new TransferRequestCreateCommand(
            $request->request->all()
        );

        $commandBus = $this->get('tactician.commandbus');
        $entity = $commandBus->handle($command);

        $data = $this->get('fractal.manager')->toArray($entity, new TransferRequestReadTransformer());

        return $this->json($data);
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function putTransferRequestAction(Request $request)
    {
        $command = new TransferRequestUpdateCommand(
            array_merge(
                $request->request->all(),
                [
                    'id' => (int)$request->get('id')
                ]
            )
        );
        $commandBus = $this->get('tactician.commandbus');
        $entity = $commandBus->handle($command);

        $data = $this->get('fractal.manager')->toArray($entity, new TransferRequestReadTransformer());

        return $this->json($data);
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function putTransferRequestApproveAction(Request $request)
    {
        $command = new TransferRequestApproveCommand(
            [
                'id' => (int)$request->get('id')
            ]
        );
        $commandBus = $this->get('tactician.commandbus');
        $entity = $commandBus->handle($command);

        $data = $this->get('fractal.manager')->toArray($entity, new TransferRequestReadTransformer());

        return $this->json($data);
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function putTransferRequestDeclineAction(Request $request)
    {
        $command = new TransferRequestDeclineCommand(
            array_merge(
                $request->request->all(),
                [
                    'id' => (int)$request->get('id')
                ]
            )
        );
        $commandBus = $this->get('tactician.commandbus');
        $entity = $commandBus->handle($command);

        $data = $this->get('fractal.manager')->toArray($entity, new TransferRequestReadTransformer());

        return $this->json($data);
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function putTransferRequestCancelAction(Request $request)
    {
        $command = new TransferRequestCancelCommand(
            [
                'id' => (int)$request->get('id')
            ]
        );
        $commandBus = $this->get('tactician.commandbus');
        $entity = $commandBus->handle($command);

        $data = $this->get('fractal.manager')->toArray($entity, new TransferRequestReadTransformer());

        return $this->json($data);
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function putTransferRequestResubmitAction(Request $request)
    {
        $command = new TransferRequestResubmitCommand(
            array_merge(
                $request->request->all(),
                [
                    'id' => (int)$request->get('id')
                ]
            )
        );
        $commandBus = $this->get('tactician.commandbus');
        $entity = $commandBus->handle($command);

        $data = $this->get('fractal.manager')->toArray($entity, new TransferRequestReadTransformer());

        return $this->json($data);
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function postTransferRequestNoteAction(Request $request)
    {
        $command = new TransferRequestNoteCreateCommand(
            array_merge(
                $request->request->all(),
                [
                    'transferRequestId' => (int)$request->get('id')
                ]
            )
        );

        $commandBus = $this->get('tactician.commandbus');
        $entity = $commandBus->handle($command);

        $data = $this->get('fractal.manager')->toArray($entity, new TransferRequestEventReadTransformer());

        return $this->json($data);
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function getTransferRequestListAction(Request $request)
    {
        $data = $request->query->all();
        if (isset($data['purposes'])) {
            $data['purposes'] = explode('|', $data['purposes']);
        }
        if (isset($data['itemModelNames'])) {
            $data['itemModelNames'] = explode('|', $data['itemModelNames']);
        }
        if (isset($data['statuses'])) {
            $data['statuses'] = explode('|', $data['statuses']);
        }
        if (isset($data['itemStoreIds'])) {
            $data['itemStoreIds'] = explode('|', $data['itemStoreIds']);
        }
        if (isset($data['receivingStoreIds'])) {
            $data['receivingStoreIds'] = explode('|', $data['receivingStoreIds']);
        }
        if (isset($data['requestedByIds'])) {
            $data['requestedByIds'] = explode('|', $data['requestedByIds']);
        }

        $command = new TransferRequestReadListCommand($data);

        $commandBus = $this->get('tactician.commandbus');
        $query = $commandBus->handle($command);

        $pagination = $this->get('fractal.paginator')->setPagination($query, $command->getPage(), $command->getPerPage());

        $tmp = [];
        foreach ($query->getResult() as $store) {
            /** @var Store $store */
            if (!isset($tmp[$store->getStatus()])) {
                $tmp[$store->getStatus()] = 0;
            }
            $tmp[$store->getStatus()]++;
        }

        $meta = [
            'counters' => [
                [
                    'status' => TransferRequest::STATUS_PENDING,
                    'counter' => $tmp[TransferRequest::STATUS_PENDING]
                ]
            ]
        ];

        $data = $this->get('fractal.manager')->toArray($pagination->getPageItems(), new TransferRequestReadTransformer(), $pagination, $meta);

        return $this->json($data);
    }

    /**
     * @param $id
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function getTransferRequestAction($id)
    {
        $command = new TransferRequestReadCommand(
            [
                'id' => (int)$id
            ]
        );
        $commandBus = $this->get('tactician.commandbus');
        $entity = $commandBus->handle($command);


        $data = $this->get('fractal.manager')->toArray($entity, new TransferRequestReadTransformer());

        return $this->json($data);
    }

    /**
     * @param $id
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function getTransferRequestEventsAction($id)
    {
        $command = new TransferRequestEventReadListCommand(
            [
                'transferRequestId' => (int)$id
            ]
        );
        $commandBus = $this->get('tactician.commandbus');
        /** @var Query $q */
        $q = $commandBus->handle($command);


        $data = $this->get('fractal.manager')->toArray($q->getResult(), new TransferRequestEventReadTransformer());

        return $this->json($data);
    }

    /**
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws InvalidArgumentException
     */
    public function getEquipmentExtractAction(Request $request)
    {
        $command = new TransferRequestGetEquipmentExtractCommand(
            $request->query->all()
        );

        $responseContent = $this->get('tactician.commandbus')->handle($command);

        $response = new Response($responseContent);
        $responseHeaders = $response->headers;
        $responseHeaders->set('Content-Type', 'text/csv');
        $responseHeaders->set('Content-Transfer-Encoding', 'binary');
        $responseHeaders->set('Content-Disposition', 'attachment; filename="extract.csv"');

        return $response;
    }
}
