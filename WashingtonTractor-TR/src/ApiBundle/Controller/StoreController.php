<?php

namespace ApiBundle\Controller;


use Doctrine\ORM\Query;
use DomainBundle\Entity\Store\Store;
use FOS\RestBundle\Controller\FOSRestController;
use ServiceBundle\CommandBus\Command\Store\StoreCreateCommand;
use ServiceBundle\CommandBus\Command\Store\StoreDeleteCommand;
use ServiceBundle\CommandBus\Command\Store\StoreReadListCommand;
use ServiceBundle\CommandBus\Command\Store\StoreReadMaxNumberCommand;
use ServiceBundle\CommandBus\Command\Store\StoreUpdateCommand;
use ServiceBundle\Transformers\StoreReadMaxNumberTransformer;
use ServiceBundle\Transformers\StoreReadTransformer;
use Symfony\Component\HttpFoundation\Request;

class StoreController extends FOSRestController
{
    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function postStoreAction(Request $request)
    {
        $command = new StoreCreateCommand(
            $request->request->all()
        );

        $commandBus = $this->get('tactician.commandbus');
        $store = $commandBus->handle($command);

        $data = $this->get('fractal.manager')->toArray($store, new StoreReadTransformer());

        return $this->json($data);
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function putStoreAction(Request $request)
    {
        $command = new StoreUpdateCommand(
            array_merge(
                $request->request->all(),
                [
                    'id' => (int)$request->get('id')
                ]
            )
        );

        $commandBus = $this->get('tactician.commandbus');
        $store = $commandBus->handle($command);

        $data = $this->get('fractal.manager')->toArray($store, new StoreReadTransformer());

        return $this->json($data);
    }

    /**
     * @param $id
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function deleteStoreAction($id)
    {
        $command = new StoreDeleteCommand(
            [
                'id' => (int)$id
            ]
        );

        $commandBus = $this->get('tactician.commandbus');
        $store = $commandBus->handle($command);

        return $this->json([]);
    }

    /**
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function getStoresListAction()
    {
        $command = new StoreReadListCommand();

        $commandBus = $this->get('tactician.commandbus');
        /** @var Query $query */
        $query = $commandBus->handle($command);
        $tmp = [];

//        echo $query;

        foreach ($query->getResult() as $store) {
            /** @var Store $store */
            if (!isset($tmp[$store->getStatus()])) {
                $tmp[$store->getStatus()] = 0;
            }
            $tmp[$store->getStatus()]++;
        }

        $meta = [
            'counters' => [
                [
                    'status' => Store::STATUS_ACTIVE,
                    'counter' => $tmp[Store::STATUS_ACTIVE]
                ],
                [
                    'status' => Store::STATUS_INACTIVE,
                    'counter' => $tmp[Store::STATUS_INACTIVE]
                ]
            ]
        ];

        $data = $this->get('fractal.manager')->toArray($query->getResult(), new StoreReadTransformer(), null, $meta);

        return $this->json($data);
    }

    /**
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function getStoresMaxNumberAction()
    {
        $command = new StoreReadMaxNumberCommand();

        $commandBus = $this->get('tactician.commandbus');
        $entity = $commandBus->handle($command);

        $data = $this->get('fractal.manager')->toArray($entity, new StoreReadMaxNumberTransformer());

        return $this->json($data);
    }
}
