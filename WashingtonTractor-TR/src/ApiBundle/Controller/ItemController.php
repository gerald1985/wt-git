<?php

namespace ApiBundle\Controller;


use FOS\RestBundle\Controller\FOSRestController;
use ServiceBundle\CommandBus\Command\Item\ItemModelsReadListCommand;
use ServiceBundle\CommandBus\Command\Store\StoreItemReadListCommand;
use ServiceBundle\Transformers\ItemModelsReadTransformer;
use ServiceBundle\Transformers\StoreItemReadTransformer;
use Symfony\Component\HttpFoundation\Request;

class ItemController extends FOSRestController
{
    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function getItemsListAction(Request $request)
    {
        $data = array_merge($request->request->all(), $request->query->all());

        if (isset($data['excludeStores'])) {
            $data['excludeStores'] = explode('|', $data['excludeStores']);
        }
        if (isset($data['excludeInTRWithStatuses'])) {
            $data['excludeInTRWithStatuses'] = explode('|', $data['excludeInTRWithStatuses']);
        }

        $command = new StoreItemReadListCommand($data);

        $commandBus = $this->get('tactician.commandbus');
        $users = $commandBus->handle($command);

        $pagination = $this->get('fractal.paginator')->setPagination($users, $command->getPage(), $command->getPerPage());

        $data = $this->get('fractal.manager')->toArray($pagination->getPageItems(), new StoreItemReadTransformer(), $pagination);

        return $this->json($data);
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function getItemsModelsListAction(Request $request)
    {
        $command = new ItemModelsReadListCommand([]);

        $commandBus = $this->get('tactician.commandbus');
        $users = $commandBus->handle($command);

        $pagination = $this->get('fractal.paginator')->setPagination($users, $command->getPage(), $command->getPerPage());

        $data = $this->get('fractal.manager')->toArray($pagination->getPageItems(), new ItemModelsReadTransformer(), $pagination);

        return $this->json($data);
    }
}
