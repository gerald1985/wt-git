<?php

namespace ApiBundle\Controller;

use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use League\Tactician\Bundle\Middleware\InvalidCommandException;
use ServiceBundle\Exception\Validator\UniqueException;
use ServiceBundle\Exception\Validator\ValidationException;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use OAuth2\OAuth2ServerException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Log\DebugLoggerInterface;
use Symfony\Component\Validator\ConstraintViolation;


class ExceptionController extends Controller
{
    public function showAction(Request $request, \Exception $exception, DebugLoggerInterface $logger = null)
    {
        $currentContent = $this->getAndCleanOutputBuffering($request->headers->get('X-Php-Ob-Level', -1));

        return new JsonResponse(
            [
                'errorCode'    => $this->getCode($exception),
                'errorMessage' => $this->getMessage($exception)
            ],
            $this->getStatusCode($exception),
            []
        );
    }

    /**
     * Gets and cleans any content that was already outputted.
     *
     * This code comes from Symfony and should be synchronized on a regular basis
     * see src/Symfony/Bundle/TwigBundle/Controller/ExceptionController.php
     *
     * @return string
     */
    private function getAndCleanOutputBuffering($startObLevel)
    {
        if (ob_get_level() <= $startObLevel) {
            return '';
        }
        Response::closeOutputBuffers($startObLevel + 1, true);

        return ob_get_clean();
    }

    /**
     * Determines the status code to use for the response.
     *
     * @param \Exception $exception
     *
     * @return int
     */
    protected function getStatusCode(\Exception $exception)
    {
        // If matched
//        if ($statusCode = $this->get('fos_rest.exception.codes_map')->resolveException($exception)) {
//            return $statusCode;
//        }

        // Otherwise, default
//        if (
//            $exception instanceof HandlerExceptionInterface
//            || $exception instanceof ValidatorExceptionInterface
//        ) {
//            return $exception->getStatusCode();
//        }


        //DB errors
        if ($exception instanceof UniqueConstraintViolationException) {
            return 500;
        }

        if ($exception instanceof OAuth2ServerException) {
            return $exception->getHttpCode();
        }

        // Otherwise, default
        if (method_exists($exception, 'getStatusCode')) {
            return $exception->getStatusCode();
        }

        return 500;
    }

    /**
     * Determines the status code to use for the response.
     *
     * @param \Exception $exception
     *
     * @return int
     */
    protected function getCode(\Exception $exception)
    {
        //Tactition validation errors
        if ($exception instanceof InvalidCommandException) {
            return 7000;
        }

        if ($exception instanceof OAuth2ServerException) {
            return 5000;
        }

        // Otherwise, default
        return $exception->getCode();
    }

    /**
     * Determines the status code to use for the response.
     *
     * @param \Exception $exception
     *
     * @return int
     */
    protected function getMessage(\Exception $exception)
    {
        $errors = [];

        //Tactition validation errors
        if ($exception instanceof InvalidCommandException) {
            $errors = [];
            $errors['message'] = $this->get('translator')->trans('validation.command_bus.exceptions', [], 'validators');
            $tmp = [];
            foreach ($exception->getViolations() as $violation) {
                /**
                 * @var ConstraintViolation $violation
                 */
                $tmp[$violation->getPropertyPath()][] = $violation->getMessage();
            }
            foreach ($tmp as $field => $errorsList) {
                $errors['attributes'][] = [
                    'field'  => $field,
                    'errors' => $errorsList
                ];
            }

            return $errors;
        }


        if (
            $exception instanceof UniqueException
            || $exception instanceof ValidationException
        ) {
            $tmp = [];
            $errors['message'] = $this->get('translator')->trans($exception->getMessage(), [], 'validators');
            foreach ($exception->getErrors() as $field => $messages) {
                /**
                 * @var ConstraintViolation $violation
                 */
                foreach ($messages as $message) {
                    $tmp[$field][] = $this->get('translator')->trans($message, [], 'validators');
                }
            }
            foreach ($tmp as $field => $errorsList) {
                $errors['attributes'][] = [
                    'field'  => $field,
                    'errors' => $errorsList
                ];
            }

            return $errors;
        }

        // Otherwise, default
        if ($exception instanceof OAuth2ServerException) {
            $errors['message'] = $exception->getDescription();
            return $errors;
        }

        $msg = $exception->getMessage();
//        if ($this->getStatusCode($exception) === 500) {
//            $msg = 'Server error';
//        }

        $errors['message'] = $this->get('translator')->trans($msg, [], 'exceptions');

        return $errors;
    }
}