<?php

namespace ApiBundle\Controller;


use FOS\RestBundle\Controller\FOSRestController;
use ServiceBundle\CommandBus\Command\User\UserApproveCommand;
use ServiceBundle\CommandBus\Command\User\UserChangePasswordCommand;
use ServiceBundle\CommandBus\Command\User\UserChangePasswordWithTokenCommand;
use ServiceBundle\CommandBus\Command\User\UserCreateCommand;
use ServiceBundle\CommandBus\Command\User\UserDeclineCommand;
use ServiceBundle\CommandBus\Command\User\UserDeleteCommand;
use ServiceBundle\CommandBus\Command\User\UserReadCommand;
use ServiceBundle\CommandBus\Command\User\UserReadListCommand;
use ServiceBundle\CommandBus\Command\User\UserResetPasswordCommand;
use ServiceBundle\CommandBus\Command\User\UserUpdateCommand;
use ServiceBundle\Transformers\UserReadTransformer;
use Symfony\Component\HttpFoundation\Request;

class UserController extends FOSRestController
{
    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function postUserAction(Request $request)
    {
        $command = new UserCreateCommand(
            $request->request->all()
        );

        $commandBus = $this->get('tactician.commandbus');
        $store = $commandBus->handle($command);

        $data = $this->get('fractal.manager')->toArray($store, new UserReadTransformer());

        return $this->json($data);
    }

    /**
     * @return mixed
     */
    public function putUserAction(Request $request)
    {
        $command = new UserUpdateCommand(
            array_merge(
                $request->request->all(),
                [
                    'id' => (int)$request->get('id')
                ]
            )
        );

        $commandBus = $this->get('tactician.commandbus');
        $entity = $commandBus->handle($command);

        $data = $this->get('fractal.manager')->toArray($entity, new UserReadTransformer());

        return $this->json($data);
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function putUserApproveAction(Request $request)
    {
        $command = new UserApproveCommand(
            array_merge(
                $request->request->all(),
                [
                    'id' => (int)$request->get('id')
                ]
            )
        );

        $commandBus = $this->get('tactician.commandbus');
        $entity = $commandBus->handle($command);

        return $this->json([]);
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function putUserDeclineAction(Request $request)
    {
        $command = new UserDeclineCommand(
            array_merge(
                $request->request->all(),
                [
                    'id' => (int)$request->get('id')
                ]
            )
        );

        $commandBus = $this->get('tactician.commandbus');
        $entity = $commandBus->handle($command);

        return $this->json([]);
    }

    /**
     * @param $id
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function deleteUserAction($id)
    {
        $command = new UserDeleteCommand(
            [
                'id' => (int)$id
            ]
        );

        $commandBus = $this->get('tactician.commandbus');
        $commandBus->handle($command);

        return $this->json([]);
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function getUsersListAction(Request $request)
    {
        $data = $request->query->all();
        if (isset($data['statuses'])) {
            $data['statuses'] = explode('|', $data['statuses']);
        }
        if (isset($data['roles'])) {
            $data['roles'] = explode('|', $data['roles']);
        }
        $command = new UserReadListCommand($data);
        $commandBus = $this->get('tactician.commandbus');
        $users = $commandBus->handle($command);

        $pagination = $this->get('fractal.paginator')->setPagination($users, $command->getPage(), $command->getPerPage());

        $data = $this->get('fractal.manager')->toArray($pagination->getPageItems(), new UserReadTransformer(), $pagination);

        return $this->json($data);
    }

    /**
     * @param $id
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function getUserAction($id)
    {
        $command = new UserReadCommand(
            [
                'id' => (int)$id
            ]
        );
        $commandBus = $this->get('tactician.commandbus');
        $entity = $commandBus->handle($command);


        $data = $this->get('fractal.manager')->toArray($entity, new UserReadTransformer());

        return $this->json($data);
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function postUserResetPasswordAction(Request $request)
    {
        $command = new UserResetPasswordCommand(
            $request->request->all()
        );

        $commandBus = $this->get('tactician.commandbus');
        $entity = $commandBus->handle($command);

        return $this->json([]);
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function putUserChangePasswordWithTokenAction(Request $request)
    {
        $command = new UserChangePasswordWithTokenCommand(
            $request->request->all()
        );

        $commandBus = $this->get('tactician.commandbus');
        $entity = $commandBus->handle($command);

        return $this->json([]);
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function putUserChangePasswordAction(Request $request)
    {
        $command = new UserChangePasswordCommand(
            $request->request->all()
        );

        $commandBus = $this->get('tactician.commandbus');
        $entity = $commandBus->handle($command);

        return $this->json([]);
    }
}
