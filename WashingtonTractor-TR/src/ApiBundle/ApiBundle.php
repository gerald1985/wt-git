<?php

namespace ApiBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;
use ApiBundle\DependencyInjection\ApiExtension;

class ApiBundle extends Bundle
{
    public function getContainerExtension()
    {
        return new ApiExtension();
    }
}
