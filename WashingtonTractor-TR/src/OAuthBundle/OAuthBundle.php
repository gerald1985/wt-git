<?php

namespace OAuthBundle;

use OAuthBundle\DependencyInjection\Compiler\OverrideServiceOAuthListener;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class OAuthBundle extends Bundle
{
    public function getParent()
    {
        return 'FOSOAuthServerBundle';
    }

    public function build(ContainerBuilder $container)
    {
        parent::build($container);

        $container->addCompilerPass(new OverrideServiceOAuthListener());
    }
}
