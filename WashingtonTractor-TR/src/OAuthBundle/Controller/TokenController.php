<?php

namespace OAuthBundle\Controller;

use OAuth2\OAuth2;
use ServiceBundle\Transformers\UserReadTransformer;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\User\UserCheckerInterface;

class TokenController
{
    /** @var OAuth2  */
    protected $server;
    /** @var CommandBus  */
    protected $userChecker;

    /**
     * TokenController constructor.
     * @param OAuth2 $server
     * @param UserCheckerInterface $userChecker
     */
    public function __construct(OAuth2 $server, UserCheckerInterface $userChecker)
    {
        $this->server = $server;
        $this->userChecker = $userChecker;
    }

    /**
     * @param Request $request
     *
     * @return Response
     */
    public function tokenAction(Request $request)
    {
        /** @var Response $response */
        $response = $this->server->grantAccessToken($request);

        //add info about user
        $data = json_decode($response->getContent(), true);
        if (!empty($data['access_token'])) {
            $accessToken = $this->server->verifyAccessToken($data['access_token']);
            $this->userChecker->checkPreAuth($accessToken->getUser());
            $this->userChecker->checkPostAuth($accessToken->getUser());
            $data['user'] = UserReadTransformer::transform($accessToken->getUser());
            $response->setContent(json_encode($data));
        }

        return $response;
    }
}
