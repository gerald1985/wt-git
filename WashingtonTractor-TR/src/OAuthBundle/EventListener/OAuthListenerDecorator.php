<?php

namespace OAuthBundle\EventListener;


use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use FOS\OAuthServerBundle\Security\Authentication\Token\OAuthToken;
use FOS\OAuthServerBundle\Security\Firewall\OAuthListener;
use OAuth2\OAuth2ServerException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\Security\Core\Exception\AuthenticationException;


class OAuthListenerDecorator extends OAuthListener
{
    /**
     * @param GetResponseEvent $event
     * @throws \Exception
     */
    public function handle(GetResponseEvent $event)
    {
        if (null === $oauthToken = $this->serverService->getBearerToken($event->getRequest(), true)) {
            return;
        }

        $token = new OAuthToken();
        $token->setToken($oauthToken);

        try {
            $returnValue = $this->authenticationManager->authenticate($token);

            if ($returnValue instanceof TokenInterface) {
                return $this->securityContext->setToken($returnValue);
            }

            if ($returnValue instanceof Response) {
                return $event->setResponse($returnValue);
            }
        } catch (AuthenticationException $e) {
            if (null !== $p = $e->getPrevious()) {
                /** @var OAuth2ServerException $p */
                $event->setResponse(
                    new JsonResponse(
                        [
                            'error_code'    => 5000,
                            'error_message' => $p->getDescription()
                        ],
                        $p->getHttpCode(),
                        [
                            'Content-Type'  => 'application/json',
                            'Cache-Control' => 'no-store',
                            'Pragma'        => 'no-cache',
                        ]
                    )
                );
            }
        }
    }
}