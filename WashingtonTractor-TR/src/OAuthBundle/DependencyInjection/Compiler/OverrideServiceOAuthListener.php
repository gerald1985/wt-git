<?php


namespace OAuthBundle\DependencyInjection\Compiler;

use OAuthBundle\EventListener\OAuthListenerDecorator;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class OverrideServiceOAuthListener implements CompilerPassInterface
{
    public function process(ContainerBuilder $container)
    {
        $definition = $container->getDefinition('fos_oauth_server.security.authentication.listener');
        $definition->setClass(OAuthListenerDecorator::class);
    }
}