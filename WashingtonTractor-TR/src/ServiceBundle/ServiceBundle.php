<?php

namespace ServiceBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;
use ServiceBundle\DependencyInjection\ServiceExtension;

class ServiceBundle extends Bundle
{
    public function getContainerExtension()
    {
        return new ServiceExtension();
    }
}
