<?php


namespace ServiceBundle\Model\Repository\Store;

use DomainBundle\Model\Repository\Store\StoreItemRepositoryInterface;
use ServiceBundle\Model\EntityRepository;


class StoreItemRepository extends EntityRepository implements StoreItemRepositoryInterface
{
}
