<?php


namespace ServiceBundle\Model\Repository\Store;

use DomainBundle\Model\Repository\Store\StoreRepositoryInterface;
use ServiceBundle\Model\EntityRepository;
use ServiceBundle\Model\Repository\SoftDeleteRepositoryTrait;


class StoreRepository extends EntityRepository implements StoreRepositoryInterface
{
    use SoftDeleteRepositoryTrait;
}
