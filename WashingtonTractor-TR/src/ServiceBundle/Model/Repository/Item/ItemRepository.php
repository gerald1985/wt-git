<?php


namespace ServiceBundle\Model\Repository\Item;

use DomainBundle\Model\Repository\Item\ItemRepositoryInterface;
use ServiceBundle\Model\EntityRepository;


class ItemRepository extends EntityRepository implements ItemRepositoryInterface
{
}
