<?php


namespace ServiceBundle\Model\Repository;


trait SoftDeleteRepositoryTrait
{
    /** @var int  */
    protected $isDeleted = 1;
    /** @var string  */
    protected $softDeleteField = 'isDeleted';

    /**
     * @param string $alias
     * @param string|null $indexBy
     * @return mixed
     */
    public function softCreateQueryBuilder($alias, $indexBy = null)
    {
        $q = $this->createQueryBuilder($alias, $indexBy);
        $q->andWhere($this->_em->getExpressionBuilder()->neq($alias . '.' . $this->softDeleteField, ':is_deleted'));
        $q->setParameter('is_deleted', $this->isDeleted);

        return $q;
    }

    /**
     * Finds a single entity by a set of criteria.
     *
     * @param array $criteria
     * @param array $params
     * @param array $orderBy
     *
     * @return object|null The entity instance or NULL if the entity can not be found.
     */
    public function softFindOneBy(array $criteria, array $params = [], array $orderBy = [])
    {
        $q = $this->softCreateQueryBuilder('o');
        foreach ($criteria as $expr) {
            $q->andWhere($expr);
        }
        foreach ($params as $key => $val) {
            $q->setParameter($key, $val);
        }
        foreach ($orderBy as $key => $order) {
            $q->orderBy($key, $order);
        }

        return $q->getQuery()->getOneOrNullResult();
    }

    /**
     * @param int $id
     * @return null|object
     */
    public function softFind($id)
    {
        return $this->softFindOneBy(
            [
                $this->_em->getExpressionBuilder()->eq('o.id', ':id')
            ],
            [
                ':id'     => $id
            ]
        );
    }
}
