<?php


namespace ServiceBundle\Model\Repository\TransferRequest;

use DomainBundle\Model\Repository\TransferRequest\TransferRequestEventRepositoryInterface;
use ServiceBundle\Model\EntityRepository;


class TransferRequestEventRepository extends EntityRepository implements TransferRequestEventRepositoryInterface
{
}
