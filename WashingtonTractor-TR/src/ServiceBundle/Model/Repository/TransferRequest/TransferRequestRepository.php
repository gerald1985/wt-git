<?php


namespace ServiceBundle\Model\Repository\TransferRequest;

use DomainBundle\Model\Repository\TransferRequest\TransferRequestRepositoryInterface;
use ServiceBundle\Model\EntityRepository;


class TransferRequestRepository extends EntityRepository implements TransferRequestRepositoryInterface
{
}
