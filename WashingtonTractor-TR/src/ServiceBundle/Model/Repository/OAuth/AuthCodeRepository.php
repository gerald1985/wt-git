<?php


namespace ServiceBundle\Model\Repository\OAuth;

use DomainBundle\Model\Repository\OAuth\AuthCodeRepositoryInterface;
use ServiceBundle\Model\EntityRepository;


class AuthCodeRepository extends EntityRepository implements AuthCodeRepositoryInterface
{

}
