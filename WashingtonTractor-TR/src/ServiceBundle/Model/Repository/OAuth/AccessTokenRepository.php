<?php


namespace ServiceBundle\Model\Repository\OAuth;

use DomainBundle\Model\Repository\OAuth\AccessTokenRepositoryInterface;
use ServiceBundle\Model\EntityRepository;


class AccessTokenRepository extends EntityRepository implements AccessTokenRepositoryInterface
{
}
