<?php


namespace ServiceBundle\Model\Repository\OAuth;

use DomainBundle\Model\Repository\OAuth\ClientRepositoryInterface;
use ServiceBundle\Model\EntityRepository;


class ClientRepository extends EntityRepository implements ClientRepositoryInterface
{

}
