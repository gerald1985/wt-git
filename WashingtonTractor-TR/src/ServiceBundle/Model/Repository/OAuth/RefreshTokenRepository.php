<?php


namespace ServiceBundle\Model\Repository\OAuth;

use DomainBundle\Model\Repository\OAuth\RefreshTokenRepositoryInterface;
use ServiceBundle\Model\EntityRepository;


class RefreshTokenRepository extends EntityRepository implements RefreshTokenRepositoryInterface
{

}
