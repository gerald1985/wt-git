<?php


namespace ServiceBundle\Model\Repository;

use DomainBundle\Model\Repository\UserRepositoryInterface;
use ServiceBundle\Model\EntityRepository;
use ServiceBundle\Model\Repository\SoftDeleteRepositoryTrait;


class UserRepository extends EntityRepository implements UserRepositoryInterface
{
    use SoftDeleteRepositoryTrait;
}
