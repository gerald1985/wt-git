<?php

namespace ServiceBundle\CommandBus\Handler\Store;

use DomainBundle\Entity\Store\Store;
use DomainBundle\Entity\TransferRequest\TransferRequest;
use DomainBundle\Entity\User;
use Doctrine\ORM\EntityManager;
use DomainBundle\Model\RepositoryInterface;
use FOS\UserBundle\Doctrine\UserManager;
use FOS\UserBundle\Model\UserManagerInterface;
use ServiceBundle\CommandBus\Command\Store\StoreUpdateCommand;
use ServiceBundle\Event\Email\SendEmailEvent;
use ServiceBundle\Exception\Handler\Manager\UserIsNotManagerHandlerException;
use ServiceBundle\Exception\Handler\Store\StoreIsNotExistHandlerException;
use ServiceBundle\Exception\Validator\UniqueException;
use ServiceBundle\Exception\Validator\ValidationException;
use ServiceBundle\Model\Repository\Store\StoreRepository;
use ServiceBundle\Security\OrderVoter;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;


class StoreUpdateHandler
{
    /**
     * @var User
     */
    protected $tokenUser;

    /**
     * @var EntityManager
     */
    protected $em;

    /**
     * @var AuthorizationCheckerInterface
     */
    protected $authorizationChecker;

    /**
     * @var UserManager
     */
    protected $userManager;

    /**
     * @var StoreRepository
     */
    protected $storeRepository;
    /**
     * @var TransferRequestRepository
     */
    protected $transferRequestRepository;
    /** @var EventDispatcherInterface */
    protected $eventDispatcher;

    public function __construct(
        User $tokenUser,
        EntityManager $em,
        AuthorizationCheckerInterface $authorizationChecker,
        UserManagerInterface $userManager,
        RepositoryInterface $StoreRepository,
        RepositoryInterface $transferRequestRepository,
        EventDispatcherInterface $eventDispatcher
    ) {
        $this->em = $em;
        $this->tokenUser = $tokenUser;
        $this->authorizationChecker = $authorizationChecker;
        $this->userManager = $userManager;
        $this->storeRepository = $StoreRepository;
        $this->transferRequestRepository = $transferRequestRepository;
        $this->eventDispatcher = $eventDispatcher;
    }


    public function handle(StoreUpdateCommand $command)
    {
        /** @var Store $entity */
        $entity = $this->storeRepository->softFind($command->getId());
        if (!($entity instanceof Store)) {
            throw new StoreIsNotExistHandlerException();
        }

        $this->uniqueChecker($entity, $command);

        //MANAGER
        if ($command->getManagerId() > 0) {
            /** @var User $manager */
            $manager = $this->userManager->findUserBy(['id' => $command->getManagerId()]);
            if (empty($manager)) {
                throw new ManagerIsNotExistHandlerException();
            }
            if (!$manager->hasRole(User::ROLE_MANAGER_STORE)) {
                throw new UserIsNotManagerHandlerException();
            }

            //MANAGER deleting old store and store employee
            $oldStore = $this->storeRepository->findOneBy(['manager' => $manager]);
            if ($oldStore instanceof Store && $oldStore->getId() !== $entity->getId()) {
                $this->checkPendingTransferRequests($command);

                $managerOld = $oldStore->getManager();
                if ($managerOld instanceof User) {
                    $managerOld->removeStore($oldStore);
                    $this->em->persist($managerOld);
                }

                /** @var Store $oldStore */
                $oldStore->setManager(null);
                $oldStore->setStatus(Store::STATUS_INACTIVE);
                $this->em->persist($oldStore);
                $this->em->flush($oldStore); //FLUSH
            }
            //MANAGER set to store and update store employee
            if (!($oldStore instanceof Store) || $oldStore->getId() !== $entity->getId()) {
                $entity->setManager($manager);
                $entity->setStatus(Store::STATUS_ACTIVE);
                if (
                    !$manager->getStores()->count()
                    || $manager->getStores()->first()->getId() !== $entity->getId()
                ) {
                    $manager->addStore($entity);
                }
            }
        } else {
            if ($entity->getManager() instanceof User) {
                $entity->getManager()->removeStore($entity);
                $entity->setManager(null);
            }
            $entity->setStatus(Store::STATUS_INACTIVE);
        }


        if ($command->getName()) {
            $entity->setName($command->getName());
        }
        if ($command->getNumber()) {
            $entity->setNumber($command->getNumber());
        }
        if ($command->getAddress()) {
            $entity->setAddress($command->getAddress());
        }

//        if (!$this->authorizationChecker->isGranted(StoreVoter::CREATE_STORE, $entity)) {
//            throw new VoterException(StoreVoter::CREATE_STORE);
//        }

        $this->em->persist($entity);

        return $entity;
    }


    /**
     * @param Store $entity
     * @param StoreUpdateCommand $command
     * @throws UniqueException
     */
    public function uniqueChecker(Store $entity, StoreUpdateCommand $command)
    {
        $errors = [];
        if ($command->getName()) {
//            $find = $this->storeRepository->softFindOneBy(
//                [
//                    $this->em->getExpressionBuilder()->eq('o.name', ':name'),
//
//                ],
//                [
//                    ':name'     => $command->getName()
//                ]
//            );
            $find = $this->storeRepository->findOneBy(
                [
                    'name'     => $command->getName()
                ]
            );
            if (($find instanceof Store) && $find->getId() !== $entity->getId()) {
                $errors['name'][] = 'validation.unique.store.name';
            }
        }
        if ($command->getNumber()) {
//            $find = $this->storeRepository->softFindOneBy(
//                [
//                    $this->em->getExpressionBuilder()->eq('o.number', ':number'),
//
//                ],
//                [
//                    ':number'     => $command->getNumber()
//                ]
//            );
            $find = $this->storeRepository->findOneBy(
                [
                    'number'     => $command->getNumber()
                ]
            );
            if (($find instanceof Store) && $find->getId() !== $entity->getId()) {
                $errors['number'][] = 'validation.unique.store.number';
            }
        }
        if ($command->getAddress()) {
//            $find = $this->storeRepository->softFindOneBy(
//                [
//                    $this->em->getExpressionBuilder()->eq('o.address', ':address'),
//
//                ],
//                [
//                    ':address'     => $command->getAddress()
//                ]
//            );
            $find = $this->storeRepository->findOneBy(
                [
                    'address'     => $command->getAddress()
                ]
            );
            if (($find instanceof Store) && $find->getId() !== $entity->getId()) {
                $errors['address'][] = 'validation.unique.store.address';
            }
        }

        if (!empty($errors)) {
            throw new UniqueException($errors);
        }
    }


    /**
     * @param StoreUpdateCommand $command
     * @throws ValidationException
     */
    public function checkPendingTransferRequests(StoreUpdateCommand $command)
    {
        /** @var QueryBuilder $q */
        $q = $this->transferRequestRepository->CreateQueryBuilder('o');
        $q->andWhere($q->expr()->in('o.status', [TransferRequest::STATUS_PENDING]));

        $q->leftJoin('o.storeItem', 'osi');
        $q->leftJoin('osi.store', 'osis');
        $q->leftJoin('osis.manager', 'osism');
        $q->andWhere($q->expr()->eq('osism.id', $command->getManagerId()));
        $q->setMaxResults(1);

        $first = $q->getQuery()->getOneOrNullResult();
        if ($first instanceof TransferRequest) {
            throw new ValidationException(
                [
                    'managerId' => ['store.manager.has.pending.outgoing.tr']
                ]
            );
        }
    }
}