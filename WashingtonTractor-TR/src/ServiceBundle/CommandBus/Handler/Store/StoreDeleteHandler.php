<?php

namespace ServiceBundle\CommandBus\Handler\Store;

use Doctrine\ORM\QueryBuilder;
use DomainBundle\Entity\Store\Store;
use Doctrine\ORM\EntityManager;
use DomainBundle\Entity\TransferRequest\TransferRequest;
use DomainBundle\Entity\User;
use DomainBundle\Model\RepositoryInterface;
use ServiceBundle\CommandBus\Command\Store\StoreDeleteCommand;
use ServiceBundle\Exception\Handler\Store\StoreHasPendingTransferRequestsHandlerException;
use ServiceBundle\Exception\Handler\Store\StoreIsNotExistHandlerException;
use ServiceBundle\Model\Repository\Store\StoreRepository;
use ServiceBundle\Model\Repository\TransferRequest\TransferRequestRepository;
use ServiceBundle\Security\OrderVoter;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;


class StoreDeleteHandler
{
    /**
     * @var EntityManager
     */
    protected $em;

    /**
     * @var AuthorizationCheckerInterface
     */
    protected $authorizationChecker;

    /**
     * @var StoreRepository
     */
    protected $storeRepository;
    /**
     * @var TransferRequestRepository
     */
    protected $transferRequestRepository;

    public function __construct(
        EntityManager $em,
        AuthorizationCheckerInterface $authorizationChecker,
        RepositoryInterface $StoreRepository,
        RepositoryInterface $transferRequestRepository
    ) {
        $this->em = $em;
        $this->authorizationChecker = $authorizationChecker;
        $this->storeRepository = $StoreRepository;
        $this->transferRequestRepository = $transferRequestRepository;
    }


    public function handle(StoreDeleteCommand $command)
    {
        /** @var Store $entity */
        $entity = $this->storeRepository->softFind($command->getId());
        if (!($entity instanceof Store)) {
            throw new StoreIsNotExistHandlerException();
        }

        $this->checkPendingTransferRequests($entity, $command);

        $entity->setIsDeleted(Store::IS_DELETED);
        /** @var User $manager */
        $manager = $entity->getManager();
        if ($manager instanceof User) {
            $entity->setManager(null);
            $manager->removeStore($entity);
        }
        $entity->setStatus(Store::STATUS_INACTIVE);

//        if (!$this->authorizationChecker->isGranted(StoreVoter::CREATE_STORE, $entity)) {
//            throw new VoterException(StoreVoter::CREATE_STORE);
//        }

//        $this->em->persist($entity);

        return $entity;
    }


    /**
     * @param Store $entity
     * @param StoreDeleteCommand $command
     * @throws StoreHasPendingTransferRequestsHandlerException
     */
    public function checkPendingTransferRequests(Store $entity, StoreDeleteCommand $command)
    {
        /** @var QueryBuilder $q */
        $q = $this->transferRequestRepository->CreateQueryBuilder('o');
        $q->andWhere($q->expr()->in('o.status', [TransferRequest::STATUS_PENDING, TransferRequest::STATUS_APPROVED]));

        $q->leftJoin('o.storeReceiver', 'osr');
        $q->leftJoin('o.storeItem', 'osi');
        $q->leftJoin('osi.store', 'osis');
        $q->andWhere(
            $q->expr()->orX(
                $q->expr()->eq('osis.id', $entity->getId()),
                $q->expr()->eq('osr.id', $entity->getId())
            )
        );
        $q->setMaxResults(1);

        $first = $q->getQuery()->getOneOrNullResult();
        if ($first instanceof TransferRequest) {
            throw new StoreHasPendingTransferRequestsHandlerException();
        }
    }
}