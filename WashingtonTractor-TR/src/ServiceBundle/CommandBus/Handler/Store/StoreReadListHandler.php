<?php

namespace ServiceBundle\CommandBus\Handler\Store;

use Doctrine\ORM\QueryBuilder;
use DomainBundle\Entity\Store\Store;
use DomainBundle\Model\RepositoryInterface;
use ServiceBundle\CommandBus\Command\Store\StoreReadListCommand;
use ServiceBundle\Model\Repository\Store\StoreRepository;
use ServiceBundle\Security\OrderVoter;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;


class StoreReadListHandler
{
    /**
     * @var AuthorizationCheckerInterface
     */
    protected $authorizationChecker;

    /**
     * @var StoreRepository
     */
    protected $storeRepository;

    public function __construct(
        AuthorizationCheckerInterface $authorizationChecker,
        RepositoryInterface $StoreRepository
    ) {
        $this->authorizationChecker = $authorizationChecker;
        $this->storeRepository = $StoreRepository;
    }


    public function handle(StoreReadListCommand $command)
    {
        /** @var QueryBuilder $q */
        $q = $this->storeRepository->createQueryBuilder('o');
        $q->andWhere($q->expr()->neq('o.isDeleted', ':not_status'));
        $q->setParameter(':not_status', Store::IS_DELETED);
        $q->orderBy('o.number', 'asc');

        return $q->getQuery();
    }
}