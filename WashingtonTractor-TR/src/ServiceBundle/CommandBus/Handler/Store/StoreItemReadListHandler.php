<?php

namespace ServiceBundle\CommandBus\Handler\Store;

use Doctrine\ORM\QueryBuilder;
use DomainBundle\Entity\Store\Store;
use DomainBundle\Model\RepositoryInterface;
use ServiceBundle\CommandBus\Command\Store\StoreItemReadListCommand;
use ServiceBundle\Model\Repository\Store\StoreItemRepository;
use ServiceBundle\Security\OrderVoter;


class StoreItemReadListHandler
{
    /**
     * @var StoreItemRepository
     */
    protected $storeItemRepository;

    public function __construct(
        RepositoryInterface $storeItemRepository
    )
    {
        $this->storeItemRepository = $storeItemRepository;
    }


    /**
     * @param StoreItemReadListCommand $command
     * @return \Doctrine\ORM\Query
     */
    public function handle(StoreItemReadListCommand $command)
    {
        /** @var QueryBuilder $q */
        $q = $this->storeItemRepository->createQueryBuilder('o');
        $q->leftJoin('o.store', 'os');
        $q->andWhere($q->expr()->eq('os.status', ':status'));
        $q->andWhere($q->expr()->neq('os.isDeleted', ':is_deleted'));
        $q->setParameter('status', Store::STATUS_ACTIVE);
        $q->setParameter('is_deleted', Store::IS_DELETED);

        if ($command->getExcludeStores()) {
            $q->andWhere($q->expr()->notIn('os.id', $command->getExcludeStores()));
        }

        if ($command->getStockNumber()) {
            $q->andWhere($q->expr()->like('o.stockNumber', ':stock_number'));
            $q->setParameter('stock_number', "{$command->getStockNumber()}%");
        }
        //if ($command->getModelName()) {
            $q->leftJoin('o.item', 'oi');
            $q->andWhere($q->expr()->like('oi.modelName', ':model_number'));
            $q->setParameter('model_number', "{$command->getModelName()}%");
        //}
        if (!empty($command->getExcludeInTRWithStatuses())) {

            /**
             * @var QueryBuilder $subq
             */
            $subq = $this->storeItemRepository->createQueryBuilder('osub');
            $subq->select('COUNT(osub.id)')
                ->leftJoin('osub.transferRequests', 'osub_tr')
                ->where('osub = o')
                ->andWhere($q->expr()->in('osub_tr.status', $command->getExcludeInTRWithStatuses()));
            $q->andWhere($q->expr()->eq('0', "({$subq->getDQL()})"));
        }
        $q->orderBy('o.daysInStock', 'DESC');
        $q->addOrderBy('oi.modelName', 'ASC');
        return $q->getQuery();
    }
}
