<?php

namespace ServiceBundle\CommandBus\Handler\Store;

use Doctrine\ORM\QueryBuilder;
use DomainBundle\Entity\Store\Store;
use DomainBundle\Model\RepositoryInterface;
use ServiceBundle\CommandBus\Command\Store\StoreReadMaxNumberCommand;
use ServiceBundle\Model\Repository\Store\StoreRepository;
use ServiceBundle\Security\OrderVoter;


class StoreReadMaxNumberHandler
{
    /**
     * @var StoreRepository
     */
    protected $storeRepository;

    public function __construct(
        RepositoryInterface $StoreRepository
    ) {
        $this->storeRepository = $StoreRepository;
    }


    public function handle(StoreReadMaxNumberCommand $command)
    {
        /** @var QueryBuilder $q */
        $q = $this->storeRepository->createQueryBuilder('o');
        $q->andWhere($q->expr()->neq('o.isDeleted', ':not_status'));
        $q->setParameter(':not_status', Store::IS_DELETED);
        $q->orderBy('o.number', 'desc');
        $q->setMaxResults(1);

        /** @var Store $entity */
        $entity = $q->getQuery()->getOneOrNullResult();

        return $entity;
    }
}