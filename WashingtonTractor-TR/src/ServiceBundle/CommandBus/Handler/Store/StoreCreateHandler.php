<?php

namespace ServiceBundle\CommandBus\Handler\Store;

use DomainBundle\Entity\Store\Store;
use DomainBundle\Entity\TransferRequest\TransferRequest;
use DomainBundle\Entity\User;
use Doctrine\ORM\EntityManager;
use DomainBundle\Model\RepositoryInterface;
use FOS\UserBundle\Doctrine\UserManager;
use FOS\UserBundle\Model\UserManagerInterface;
use ServiceBundle\CommandBus\Command\Store\StoreCreateCommand;
use ServiceBundle\Exception\Handler\Manager\ManagerIsNotExistHandlerException;
use ServiceBundle\Exception\Handler\Manager\UserIsNotManagerHandlerException;
use ServiceBundle\Exception\Validator\UniqueException;
use ServiceBundle\Exception\Validator\ValidationException;
use ServiceBundle\Exception\Voter\VoterException;
use ServiceBundle\Model\Repository\Store\StoreRepository;
use ServiceBundle\Security\OrderVoter;
use ServiceBundle\Security\StoreVoter;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;


class StoreCreateHandler
{
    /**
     * @var User
     */
    protected $tokenUser;

    /**
     * @var EntityManager
     */
    protected $em;

    /**
     * @var AuthorizationCheckerInterface
     */
    protected $authorizationChecker;

    /**
     * @var UserManager
     */
    protected $userManager;

    /**
     * @var StoreRepository
     */
    protected $storeRepository;
    /**
     * @var TransferRequestRepository
     */
    protected $transferRequestRepository;

    public function __construct(
        User $tokenUser,
        EntityManager $em,
        AuthorizationCheckerInterface $authorizationChecker,
        UserManagerInterface $userManager,
        RepositoryInterface $StoreRepository,
        RepositoryInterface $transferRequestRepository
    )
    {
        $this->em = $em;
        $this->tokenUser = $tokenUser;
        $this->authorizationChecker = $authorizationChecker;
        $this->userManager = $userManager;
        $this->storeRepository = $StoreRepository;
        $this->transferRequestRepository = $transferRequestRepository;
    }


    public function handle(StoreCreateCommand $command)
    {
        $class = $this->storeRepository->getClassName();
        /** @var Store $entity */
        $entity = new $class;

        $this->uniqueChecker($command);

        $entity->setName($command->getName());
        $entity->setNumber($command->getNumber());
        $entity->setAddress($command->getAddress());
        //MANAGER
        if ($command->getManagerId()) {
            /** @var User $manager */
            $manager = $this->userManager->findUserBy(['id' => $command->getManagerId()]);
            if (empty($manager)) {
                throw new ManagerIsNotExistHandlerException();
            }

            if (!$manager->hasRole(User::ROLE_MANAGER_STORE)) {
                throw new UserIsNotManagerHandlerException();
            }
            $entity->setManager($manager);
            $manager->addStore($entity);


            $oldStore = $this->storeRepository->findOneBy(['manager' => $manager]);
            $entity->setStatus(Store::STATUS_ACTIVE);
        } else {
            $entity->setStatus(Store::STATUS_INACTIVE);
        }



        if (!$this->authorizationChecker->isGranted(StoreVoter::CREATE_STORE, $entity)) {
            throw new VoterException(StoreVoter::CREATE_STORE);
        }

        if ($oldStore instanceof Store) {
            $this->checkPendingTransferRequests($command);

            /** @var Store $oldStore */
            $oldStore->setManager(null);
            $oldStore->setStatus(Store::STATUS_INACTIVE);
            $this->em->persist($oldStore);
            $this->em->flush($oldStore);
        }

        $this->em->persist($entity);

        return $entity;
    }


    /**
     * @param StoreCreateCommand $command
     * @throws UniqueException
     */
    public function uniqueChecker(StoreCreateCommand $command)
    {
        $errors = [];
        if ($command->getName()) {

//            $find = $this->storeRepository->softFindOneBy(
//                [
//                    $this->em->getExpressionBuilder()->eq('o.name', ':name'),
//
//                ],
//                [
//                    ':name' => $command->getName()
//                ]
//            );

            $find = $this->storeRepository->findOneBy(
                [
                    'name' => $command->getName()
                ]
            );
            if ($find instanceof Store) {
                $errors['name'][] = 'validation.unique.store.name';
            }
        }
        if ($command->getNumber()) {
//            $find = $this->storeRepository->softFindOneBy(
//                [
//                    $this->em->getExpressionBuilder()->eq('o.number', ':number'),
//
//                ],
//                [
//                    ':number' => $command->getNumber()
//                ]
//            );
            $find = $this->storeRepository->findOneBy(
                [
                    'number' => $command->getNumber()
                ]
            );
            if ($find instanceof Store) {
                $errors['number'][] = 'validation.unique.store.number';
            }
        }
        if ($command->getAddress()) {
//            $find = $this->storeRepository->softFindOneBy(
//                [
//                    $this->em->getExpressionBuilder()->eq('o.address', ':address'),
//
//                ],
//                [
//                    ':address' => $command->getAddress()
//                ]
//            );
            $find = $this->storeRepository->findOneBy(
                [
                    'address' => $command->getAddress()
                ]
            );
            if ($find instanceof Store) {
                $errors['address'][] = 'validation.unique.store.address';
            }
        }

        if (!empty($errors)) {
            throw new UniqueException($errors);
        }
    }


    /**
     * @param StoreCreateCommand $command
     * @throws ValidationException
     */
    public function checkPendingTransferRequests(StoreCreateCommand $command)
    {
        /** @var QueryBuilder $q */
        $q = $this->transferRequestRepository->CreateQueryBuilder('o');
        $q->andWhere($q->expr()->in('o.status', [TransferRequest::STATUS_PENDING]));

        $q->leftJoin('o.storeItem', 'osi');
        $q->leftJoin('osi.store', 'osis');
        $q->leftJoin('osis.manager', 'osism');
        $q->andWhere($q->expr()->eq('osism.id', $command->getManagerId()));
        $q->setMaxResults(1);

        $first = $q->getQuery()->getOneOrNullResult();
        if ($first instanceof TransferRequest) {
            throw new ValidationException(
                [
                    'managerId' => ['store.has.pending.outgoing.tr']
                ]
            );
        }
    }
}