<?php

namespace ServiceBundle\CommandBus\Handler\Item;

use Doctrine\ORM\QueryBuilder;
use DomainBundle\Model\RepositoryInterface;
use ServiceBundle\CommandBus\Command\Item\ItemModelsReadListCommand;
use ServiceBundle\Model\Repository\Item\ItemRepository;
use ServiceBundle\Security\OrderVoter;


class ItemModelsReadListHandler
{
    /**
     * @var ItemRepository
     */
    protected $storeRepository;

    public function __construct(
        RepositoryInterface $itemRepository
    ) {
        $this->itemRepository = $itemRepository;
    }


    public function handle(ItemModelsReadListCommand $command)
    {
        /** @var QueryBuilder $q */
        $q = $this->itemRepository->createQueryBuilder('o');
        $q->orderBy('o.modelName', 'asc');
        $q->groupBy('o.modelName');

        return $q->getQuery();
    }
}