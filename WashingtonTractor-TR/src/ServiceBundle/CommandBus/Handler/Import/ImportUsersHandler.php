<?php

namespace ServiceBundle\CommandBus\Handler\Import;

use Doctrine\ORM\ORMInvalidArgumentException;
use DomainBundle\Entity\User;
use Doctrine\ORM\EntityManager;
use DomainBundle\Model\RepositoryInterface;
use FOS\UserBundle\Model\UserManagerInterface;
use ServiceBundle\CommandBus\Command\Import\ImportUsersCommand;
use ServiceBundle\Model\Repository\UserRepository;
use ServiceBundle\Services\ImportUsersService;


class ImportUsersHandler
{
    /**
     * @var EntityManager
     */
    protected $em;

    /**
     * @var UserRepository
     */
    protected $userRepository;
    /**
     * @var ImportUsersService
     */
    protected $importUsersService;
    /**
     * @var UserManager
     */
    protected $userManager;


    public function __construct(
        EntityManager $em,
        RepositoryInterface $userRepository,
        ImportUsersService $importUsersService,
        UserManagerInterface $userManager
    )
    {
        $this->em = $em;
        $this->userRepository = $userRepository;
        $this->importUsersService = $importUsersService;
        $this->userManager = $userManager;
    }


    /**
     * @param ImportUsersCommand $command
     *
     * @throws \Doctrine\DBAL\DBALException
     * @throws ORMInvalidArgumentException
     * @throws \Doctrine\DBAL\ConnectionException If the rollback operation failed.
     *
     * @return array
     */
    public function handle(ImportUsersCommand $command)
    {
        $list = $this->importUsersService->getUsersDataFromImportDb();

        $listEntities = [];
        foreach ($list as $user) {
            //skip user if exist
            $entity = $this->userRepository->softFindOneBy(
                [
                    $this->em->getExpressionBuilder()->eq('o.email', $this->em->getExpressionBuilder()->literal($user['email']))
                ]
            );
            if (
                $entity instanceof User
                || !in_array(
                    $user['employeeclassname'],
                    [
                        'Admin',
                        'Inventory Manager',
                        'Store Manager',
                        'Sales Person'
                    ],
                    true
                )
            ) {
                continue;
            }

            $entity = $this->importUsersService->fillUserEntity($user);
            $listEntities[] = $entity;
            $this->userManager->updateUser($entity);
        }

        return $listEntities;
    }
}