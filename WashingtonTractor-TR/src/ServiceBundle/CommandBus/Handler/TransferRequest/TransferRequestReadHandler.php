<?php

namespace ServiceBundle\CommandBus\Handler\TransferRequest;

use DomainBundle\Entity\TransferRequest\TransferRequest;
use DomainBundle\Model\RepositoryInterface;
use ServiceBundle\CommandBus\Command\TransferRequest\TransferRequestReadCommand;
use ServiceBundle\Exception\Handler\TransferRequest\TransferRequestNotFoundHandlerException;
use ServiceBundle\Model\Repository\TransferRequest\TransferRequestRepository;



class TransferRequestReadHandler
{
    /**
     * @var TransferRequestRepository
     */
    protected $transferRequestRepository;

    public function __construct(
        RepositoryInterface $transferRequestRepository
    )
    {
        $this->transferRequestRepository = $transferRequestRepository;
    }


    public function handle(TransferRequestReadCommand $command)
    {
        /** @var TransferRequest $entity */
        $entity = $this->transferRequestRepository->find($command->getId());
        if (!($entity instanceof TransferRequest)) {
            throw new TransferRequestNotFoundHandlerException();
        }

        return $entity;
    }
}