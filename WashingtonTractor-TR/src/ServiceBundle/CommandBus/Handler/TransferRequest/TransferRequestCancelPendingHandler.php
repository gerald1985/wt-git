<?php

namespace ServiceBundle\CommandBus\Handler\TransferRequest;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\ORMInvalidArgumentException;
use DomainBundle\Entity\TransferRequest\TransferRequest;
use ServiceBundle\CommandBus\Command\TransferRequest\TransferRequestCancelPendingCommand;
use ServiceBundle\Event\TransferRequest\TransferRequestEventEvent;
use ServiceBundle\Model\Repository\TransferRequest\TransferRequestRepository;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use DateTime;

class TransferRequestCancelPendingHandler
{
    /** @var TransferRequestRepository */
    protected $transferRequestRepository;
    /** @var EntityManager */
    protected $entityManager;
    /** @var EventDispatcherInterface */
    protected $eventDispatcher;

    /**
     * @param TransferRequestRepository $repository
     * @param EntityManager $entityManager
     * @param EventDispatcherInterface $eventDispatcher
     */
    public function __construct(
        TransferRequestRepository $repository,
        EntityManager $entityManager,
        EventDispatcherInterface $eventDispatcher
    )
    {
        $this->transferRequestRepository = $repository;
        $this->entityManager = $entityManager;
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * @param TransferRequestCancelPendingCommand $command
     *
     * @throws ORMInvalidArgumentException
     */
    public function handle(TransferRequestCancelPendingCommand $command)
    {
        $dateFrom = new DateTime('@' . $command->getTimestampFrom());
        $requests = $this->getRequests($dateFrom);

        /** @var TransferRequest $requestEntity */
        foreach ($requests as $requestEntity) {
            $this->cancelRequest($requestEntity);
        }
    }

    /**
     * @param DateTime $dateFrom
     *
     * @return array
     */
    protected function getRequests(DateTime $dateFrom)
    {
        $q = $this->transferRequestRepository->createQueryBuilder('o');
        $q->andWhere('o.status = :status');
        $q->andWhere('o.requestDatetime <= :date_from');
        $q->setParameters([
            ':date_from' => $dateFrom->format('Y-m-d H:i:s'),
            ':status' => TransferRequest::STATUS_PENDING
        ]);

        return $q->getQuery()->getResult();
    }

    /**
     * @param TransferRequest $entity
     *
     * @throws ORMInvalidArgumentException
     */
    protected function cancelRequest(TransferRequest $entity)
    {
        $entityOld = clone $entity;
        $entity->setStatus(TransferRequest::STATUS_CANCELLED);
        $this->entityManager->persist($entity);
        if ($entityOld->getStatus() !== $entity->getStatus()) {
            $event = new TransferRequestEventEvent($entity, $entityOld);
            $this->eventDispatcher->dispatch('tr_event.canceled_pending', $event);
        }
    }
}
