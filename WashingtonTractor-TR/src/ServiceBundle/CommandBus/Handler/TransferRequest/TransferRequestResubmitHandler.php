<?php

namespace ServiceBundle\CommandBus\Handler\TransferRequest;


use DomainBundle\Entity\Store\Store;
use DomainBundle\Entity\Store\StoreItem;
use DomainBundle\Entity\TransferRequest\TransferRequest;
use DomainBundle\Entity\User;
use Doctrine\ORM\EntityManager;
use DomainBundle\Model\RepositoryInterface;
use ServiceBundle\CommandBus\Command\TransferRequest\TransferRequestResubmitCommand;
use ServiceBundle\Event\TransferRequest\TransferRequestEventEvent;
use ServiceBundle\Exception\Handler\TransferRequest\TransferRequestIsNotDeclinedHandlerException;
use ServiceBundle\Exception\Handler\TransferRequest\TransferRequestIsNotRequestedByUserHandlerException;
use ServiceBundle\Exception\Handler\TransferRequest\TransferRequestNotFoundHandlerException;
use ServiceBundle\Exception\Validator\ValidationException;
use ServiceBundle\Model\Repository\Store\StoreItemRepository;
use ServiceBundle\Model\Repository\TransferRequest\TransferRequestRepository;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;


class TransferRequestResubmitHandler
{
    /**
     * @var User
     */
    protected $tokenUser;

    /**
     * @var EntityManager
     */
    protected $em;
    /**
     * @var TransferRequestRepository
     */
    protected $transferRequestRepository;

    /**
     * @var StoreItemRepository
     */
    protected $storeItemRepository;
    /** @var EventDispatcherInterface */
    protected $eventDispatcher;

    public function __construct(
        User $tokenUser,
        EntityManager $em,
        RepositoryInterface $transferRequestRepository,
        RepositoryInterface $storeItemRepository,
        EventDispatcherInterface $eventDispatcher
    )
    {
        $this->em = $em;
        $this->tokenUser = $tokenUser;
        $this->transferRequestRepository = $transferRequestRepository;
        $this->storeItemRepository = $storeItemRepository;
        $this->eventDispatcher = $eventDispatcher;
    }


    public function handle(TransferRequestResubmitCommand $command)
    {
        /** @var TransferRequest $entity */
        $entity = $this->transferRequestRepository->find($command->getId());
        if (!($entity instanceof TransferRequest)) {
            throw new TransferRequestNotFoundHandlerException();
        }

        $entityOld = clone $entity;

        if (
            !($entity->getStatus() === TransferRequest::STATUS_PENDING
                || $entity->getStatus() === TransferRequest::STATUS_DECLINED)
        ) {
            throw new TransferRequestIsNotDeclinedHandlerException();
        }
        if ($this->tokenUser->getId() !== $entity->getRequestedBy()->getId()) {
            throw new TransferRequestIsNotRequestedByUserHandlerException();
        }

        $this->setItem($command, $entity);

        $entity->setStatus(TransferRequest::STATUS_PENDING);

        $this->em->persist($entity);

        if ($entityOld->getStatus() !== $entity->getStatus()) {
            $event = new TransferRequestEventEvent($entity, $entityOld, $command->getLastNote());
            $this->eventDispatcher->dispatch('tr_event.resubmit', $event);
        }

        return $entity;
    }


    /**
     * @param TransferRequestResubmitCommand $command
     * @param TransferRequest $entity
     * @throws StoreItemIsUsedByOtherUserHandlerException
     * @throws ValidationException
     */
    public function setItem(TransferRequestResubmitCommand $command, TransferRequest $entity)
    {
        /** @var StoreItem $storeItem */
        $storeItem = $this->storeItemRepository->find($command->getStoreItemId());
        if (
            !($storeItem instanceof StoreItem)
            || $storeItem->getStore()->getStatus() !== Store::STATUS_ACTIVE
        ) {
            throw new ValidationException(
                [
                    'storeItemId' => ['not.exist.storeItem']
                ]
            );
        }

        foreach ($storeItem->getTransferRequests() as $transferRequest) {
            /** @var TransferRequest $transferRequest */
            if (
                $transferRequest->getId() !== $command->getId()
                && (
                    $transferRequest->getStatus() === TransferRequest::STATUS_APPROVED
                    || $transferRequest->getStatus() === TransferRequest::STATUS_PENDING
                )
            ) {
                throw new ValidationException(
                    [
                        'storeItemId' => ['is.used.store_item']
                    ]
                );
            }
        }

        $entity->setStoreItem($storeItem);
    }
}