<?php

namespace ServiceBundle\CommandBus\Handler\TransferRequest;

use DomainBundle\Entity\TransferRequest\TransferRequest;
use DomainBundle\Entity\TransferRequest\TransferRequestEvent;
use DomainBundle\Entity\User;
use Doctrine\ORM\EntityManager;
use DomainBundle\Model\RepositoryInterface;
use ServiceBundle\CommandBus\Command\TransferRequest\TransferRequestNoteCreateCommand;
use ServiceBundle\Event\TransferRequest\TransferRequestEventEvent;
use ServiceBundle\Exception\Handler\TransferRequest\TransferRequestNotFoundHandlerException;
use ServiceBundle\Model\Repository\TransferRequest\TransferRequestRepository;
use ServiceBundle\Security\OrderVoter;
use ServiceBundle\Services\TransferRequestEventService;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;


class TransferRequestNoteCreateHandler
{
    /**
     * @var User
     */
    protected $tokenUser;

    /**
     * @var EntityManager
     */
    protected $em;
    /**
     * @var TransferRequestRepository
     */
    protected $transferRequestRepository;
    /** @var TransferRequestEventService */
    protected $transferRequestEventService;
    /** @var EventDispatcherInterface */
    protected $eventDispatcher;

    public function __construct(
        User $tokenUser,
        EntityManager $em,
        RepositoryInterface $transferRequestRepository,
        TransferRequestEventService $transferRequestEventService,
        EventDispatcherInterface $eventDispatcher
    )
    {
        $this->em = $em;
        $this->tokenUser = $tokenUser;
        $this->transferRequestRepository = $transferRequestRepository;
        $this->transferRequestEventService = $transferRequestEventService;
        $this->eventDispatcher = $eventDispatcher;
    }


    public function handle(TransferRequestNoteCreateCommand $command)
    {
        $tr = $this->getTransferRequest($command);

        $event = new TransferRequestEventEvent($tr, $tr, $command->getLastNote());
        $event->setEventType(TransferRequestEvent::EVENT_NOTE);
        $entity = $this->transferRequestEventService->add($event);

        $this->eventDispatcher->dispatch('tr_event.note_notifications', $event);

        return $entity;
    }


    /**
     * @param TransferRequestNoteCreateCommand $command
     * @return object
     * @throws TransferRequestNotFoundHandlerException
     */
    public function getTransferRequest(TransferRequestNoteCreateCommand $command)
    {
        /** @var TransferRequest $storeItem */
        $tr = $this->transferRequestRepository->find($command->getTransferRequestId());
        if (!($tr instanceof TransferRequest)) {
            throw new TransferRequestNotFoundHandlerException();
        }

        return $tr;
    }
}