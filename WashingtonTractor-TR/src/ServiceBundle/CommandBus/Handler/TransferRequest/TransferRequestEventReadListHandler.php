<?php

namespace ServiceBundle\CommandBus\Handler\TransferRequest;

use Doctrine\ORM\QueryBuilder;
use DomainBundle\Entity\TransferRequest\TransferRequest;
use DomainBundle\Model\RepositoryInterface;
use ServiceBundle\CommandBus\Command\TransferRequest\TransferRequestEventReadListCommand;
use ServiceBundle\Exception\Handler\TransferRequest\TransferRequestNotFoundHandlerException;
use ServiceBundle\Model\Repository\TransferRequest\TransferRequestEventRepository;
use ServiceBundle\Model\Repository\TransferRequest\TransferRequestRepository;
use ServiceBundle\Security\OrderVoter;


class TransferRequestEventReadListHandler
{
    /**
     * @var TransferRequestEventRepository
     */
    protected $transferRequestEventRepository;
    /**
     * @var TransferRequestRepository
     */
    protected $transferRequestRepository;

    public function __construct(
        RepositoryInterface $transferRequestEventRepository,
        RepositoryInterface $transferRequestRepository
    ) {
        $this->transferRequestEventRepository = $transferRequestEventRepository;
        $this->transferRequestRepository = $transferRequestRepository;
    }


    public function handle(TransferRequestEventReadListCommand $command)
    {
        $this->checkTransferRequest($command);

        /** @var QueryBuilder $q */
        $q = $this->transferRequestEventRepository->createQueryBuilder('o');
        $q->leftJoin('o.transferRequest', 'otr');
        $q->andWhere($q->expr()->in('otr.id', $command->getTransferRequestId()));

        return $q->getQuery();
    }


    /**
     * @param TransferRequestEventReadListCommand $command
     * @throws TransferRequestNotFoundHandlerException
     */
    public function checkTransferRequest(TransferRequestEventReadListCommand $command)
    {
        /** @var TransferRequest $storeItem */
        $tr = $this->transferRequestRepository->find($command->getTransferRequestId());
        if (!($tr instanceof TransferRequest)) {
            throw new TransferRequestNotFoundHandlerException();
        }
    }
}