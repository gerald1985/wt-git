<?php

namespace ServiceBundle\CommandBus\Handler\TransferRequest;

use DomainBundle\Entity\TransferRequest\TransferRequest;
use DomainBundle\Entity\User;
use Doctrine\ORM\EntityManager;
use DomainBundle\Model\RepositoryInterface;
use ServiceBundle\CommandBus\Command\TransferRequest\TransferRequestDeclineCommand;
use ServiceBundle\Event\TransferRequest\TransferRequestEventEvent;
use ServiceBundle\Exception\Handler\Manager\ManagerIsNotItemStoreManagerHandlerException;
use ServiceBundle\Exception\Handler\TransferRequest\TransferRequestIsNotPendingHandlerException;
use ServiceBundle\Exception\Handler\TransferRequest\TransferRequestNotFoundHandlerException;
use ServiceBundle\Model\Repository\TransferRequest\TransferRequestRepository;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class TransferRequestDeclineHandler
{
    /**
     * @var User
     */
    protected $tokenUser;

    /**
     * @var EntityManager
     */
    protected $em;
    /**
     * @var TransferRequestRepository
     */
    protected $transferRequestRepository;
    /** @var EventDispatcherInterface */
    protected $eventDispatcher;

    public function __construct(
        User $tokenUser,
        EntityManager $em,
        RepositoryInterface $transferRequestRepository,
        EventDispatcherInterface $eventDispatcher
    )
    {
        $this->em = $em;
        $this->tokenUser = $tokenUser;
        $this->transferRequestRepository = $transferRequestRepository;
        $this->eventDispatcher = $eventDispatcher;
    }


    public function handle(TransferRequestDeclineCommand $command)
    {
        /** @var TransferRequest $entity */
        $entity = $this->transferRequestRepository->find($command->getId());
        if (!($entity instanceof TransferRequest)) {
            throw new TransferRequestNotFoundHandlerException();
        }

        $entityOld = clone $entity;

        if (
            !($entity->getStatus() === TransferRequest::STATUS_PENDING
                || $entity->getStatus() === TransferRequest::STATUS_DECLINED)
        ) {
            throw new TransferRequestIsNotPendingHandlerException();
        }
        if (!$this->tokenUser->hasStore($entity->getStoreItem()->getStore())) {
            throw new ManagerIsNotItemStoreManagerHandlerException();
        }

        $entity->setStatus(TransferRequest::STATUS_DECLINED);

        $this->em->persist($entity);

        if ($entityOld->getStatus() !== $entity->getStatus()) {
            $event = new TransferRequestEventEvent($entity, $entityOld, $command->getLastNote());
            $this->eventDispatcher->dispatch('tr_event.status_changed', $event);
        }

        return $entity;
    }
}