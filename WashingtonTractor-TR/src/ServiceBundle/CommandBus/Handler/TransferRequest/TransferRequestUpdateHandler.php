<?php

namespace ServiceBundle\CommandBus\Handler\TransferRequest;

use DomainBundle\Entity\Store\Store;
use DomainBundle\Entity\Store\StoreItem;
use DomainBundle\Entity\TransferRequest\TransferRequest;
use DomainBundle\Entity\User;
use Doctrine\ORM\EntityManager;
use DomainBundle\Model\RepositoryInterface;
use ServiceBundle\CommandBus\Command\TransferRequest\TransferRequestUpdateCommand;
use ServiceBundle\Event\TransferRequest\TransferRequestEventEvent;
use ServiceBundle\Exception\Handler\TransferRequest\TransferRequestIsCanceledHandlerException;
use ServiceBundle\Exception\Handler\TransferRequest\TransferRequestNotFoundHandlerException;
use ServiceBundle\Exception\Handler\Store\StoreItemIsUsedByOtherUserHandlerException;
use ServiceBundle\Exception\Validator\ValidationException;
use ServiceBundle\Model\Repository\Store\StoreItemRepository;
use ServiceBundle\Model\Repository\Store\StoreRepository;
use ServiceBundle\Model\Repository\TransferRequest\TransferRequestRepository;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;


class TransferRequestUpdateHandler
{
    /**
     * @var User
     */
    protected $tokenUser;

    /**
     * @var EntityManager
     */
    protected $em;
    /**
     * @var TransferRequestRepository
     */
    protected $transferRequestRepository;

    /**
     * @var StoreItemRepository
     */
    protected $storeItemRepository;

    /**
     * @var StoreRepository
     */
    protected $storeRepository;
    /** @var EventDispatcherInterface */
    protected $eventDispatcher;

    public function __construct(
        User $tokenUser,
        EntityManager $em,
        RepositoryInterface $transferRequestRepository,
        RepositoryInterface $storeItemRepository,
        RepositoryInterface $storeRepository,
        EventDispatcherInterface $eventDispatcher
    )
    {
        $this->em = $em;
        $this->tokenUser = $tokenUser;
        $this->transferRequestRepository = $transferRequestRepository;
        $this->storeItemRepository = $storeItemRepository;
        $this->storeRepository = $storeRepository;
        $this->eventDispatcher = $eventDispatcher;
    }


    public function handle(TransferRequestUpdateCommand $command)
    {
        /** @var TransferRequest $entity */
        $entity = $this->transferRequestRepository->find($command->getId());
        if (!($entity instanceof TransferRequest)) {
            throw new TransferRequestNotFoundHandlerException();
        }
        if ($entity->getStatus() === TransferRequest::STATUS_CANCELLED) {
            throw new TransferRequestIsCanceledHandlerException();
        }
        $entityOld = clone $entity;

        $this->setItem($command, $entity);
        $this->setReceiverStore($command, $entity);

        $shipmentDatetime = $command->getShipmentTimestamp() ? (new \DateTime('@' . $command->getShipmentTimestamp())) : null;
        $entity->setShipmentDate($shipmentDatetime);
        $entity->setStatus(TransferRequest::STATUS_PENDING);
        $entity->setPurpose($command->getPurpose());
        $entity->setReceiverUserName($command->getReceiverUserName());

        $this->em->persist($entity);

        $event = new TransferRequestEventEvent($entity, $entityOld, $command->getLastNote());
        $this->eventDispatcher->dispatch('tr_event.update', $event);

        return $entity;
    }


    /**
     * @param TransferRequestUpdateCommand $command
     * @param TransferRequest $entity
     * @throws StoreItemIsUsedByOtherUserHandlerException
     * @throws ValidationException
     */
    public function setItem(TransferRequestUpdateCommand $command, TransferRequest $entity)
    {
        /** @var StoreItem $storeItem */
        $storeItem = $this->storeItemRepository->find($command->getStoreItemId());
        if (
            !($storeItem instanceof StoreItem)
            || $storeItem->getStore()->getStatus() !== Store::STATUS_ACTIVE
        ) {
            throw new ValidationException(
                [
                    'storeItemId' => ['not.exist.storeItem']
                ]
            );
        }

        foreach ($storeItem->getTransferRequests() as $transferRequest) {
            /** @var TransferRequest $transferRequest */
            if (
                $transferRequest->getId() !== $command->getId()
                && (
                    $transferRequest->getStatus() === TransferRequest::STATUS_APPROVED
                    || $transferRequest->getStatus() === TransferRequest::STATUS_PENDING
                )
            ) {
                throw new ValidationException(
                    [
                        'storeItemId' => ['is.used.store_item']
                    ]
                );
            }
        }

        $entity->setStoreItem($storeItem);
    }


    /**
     * @param TransferRequestUpdateCommand $command
     * @param TransferRequest $entity
     * @throws ValidationException
     */
    public function setReceiverStore(TransferRequestUpdateCommand $command, TransferRequest $entity)
    {
        /** @var Store $store */
        $store = $this->storeRepository->softFind($command->getStoreReceiverId());
        if (
            !($store instanceof Store)
            || $store->getStatus() !== Store::STATUS_ACTIVE
        ) {
            throw new ValidationException(
                [
                    'storeReceiverId' => ['not.exist.store']
                ]
            );
        }

        $entity->setStoreReceiver($store);
    }
}