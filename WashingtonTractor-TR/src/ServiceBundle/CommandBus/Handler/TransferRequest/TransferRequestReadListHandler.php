<?php

namespace ServiceBundle\CommandBus\Handler\TransferRequest;

use Doctrine\ORM\QueryBuilder;
use DomainBundle\Model\RepositoryInterface;
use ServiceBundle\CommandBus\Command\TransferRequest\TransferRequestReadListCommand;
use ServiceBundle\Model\Repository\TransferRequest\TransferRequestRepository;
use ServiceBundle\Security\OrderVoter;


class TransferRequestReadListHandler
{
    /**
     * @var TransferRequestRepository
     */
    protected $transferRequestRepository;

    public function __construct(
        RepositoryInterface $transferRequestRepository
    ) {
        $this->transferRequestRepository = $transferRequestRepository;
    }


    public function handle(TransferRequestReadListCommand $command)
    {
        /** @var QueryBuilder $q */
        $q = $this->transferRequestRepository->createQueryBuilder('o');
        $q->addSelect("(CASE WHEN(o.status = 'PENDING') THEN '0' else '1' END) AS HIDDEN sortField");


        if (
            $command->getItemStockNumber()
            || $command->getItemModelNames()
            || $command->getItemStoreIds()
        ) {
            $q->leftJoin('o.storeItem', 'osi');
        }

        //search by stock number
        if ($command->getItemStockNumber()) {
            $q->andWhere($q->expr()->like('osi.stockNumber', ':stockNumber'));
            $q->setParameter('stockNumber', "%{$command->getItemStockNumber()}%");
        }

        //search by model name
        if ($command->getItemModelNames()) {
            $q->leftJoin('osi.item', 'osii');

            $q->andWhere($q->expr()->in('osii.modelName', $command->getItemModelNames()));
        }

        //search by item store id
        if ($command->getItemStoreIds()) {
            $q->leftJoin('osi.store', 'osi_s');

            $q->andWhere($q->expr()->in('osi_s.id', $command->getItemStoreIds()));
        }

        //search by item store id
        if ($command->getReceivingStoreIds()) {
            $q->leftJoin('o.storeReceiver', 'osr');

            $q->andWhere($q->expr()->in('osr.id', $command->getReceivingStoreIds()));
        }


        if ($command->getRequestedByIds()) {
            $q->leftJoin('o.requestedBy', 'orb');

            $q->andWhere($q->expr()->in('orb.id', $command->getRequestedByIds()));
        }

        //search by purpose
        if ($command->getPurposes()) {
            $q->andWhere($q->expr()->in('o.purpose', $command->getPurposes()));
        }

        //search by status
        if ($command->getStatuses()) {
            $q->andWhere($q->expr()->in('o.status', $command->getStatuses()));
        }

        //search by stock number
        if ($command->getRequestTimestampFrom()) {
            $q->andWhere($q->expr()->gte('o.requestDatetime', ':requestDatetimeFrom'));
            $date = (new \DateTime('@' . $command->getRequestTimestampFrom()))->format('Y-m-d H:i:s');
            $q->setParameter('requestDatetimeFrom', $date);
        }

        //search by stock number
        if ($command->getRequestTimestampTo()) {
            $q->andWhere($q->expr()->lte('o.requestDatetime', ':requestDatetimeTo'));
            $date = (new \DateTime('@' . $command->getRequestTimestampTo()))->format('Y-m-d H:i:s');
            $q->setParameter('requestDatetimeTo', $date);
        }

        $q->orderBy('sortField', 'ASC');
        $q->addOrderBy('o.requestDatetime', 'DESC');


        return $q->getQuery();
    }
}
