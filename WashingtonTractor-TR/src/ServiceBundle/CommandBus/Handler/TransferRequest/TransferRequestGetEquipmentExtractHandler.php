<?php

namespace ServiceBundle\CommandBus\Handler\TransferRequest;

use ServiceBundle\CommandBus\Command\TransferRequest\TransferRequestGetEquipmentExtractCommand;
use ServiceBundle\Services\ExtractEquipmentService;
use InvalidArgumentException;
use DateTime;

class TransferRequestGetEquipmentExtractHandler
{
    /** @var ExtractEquipmentService */
    protected $extractEquipmentService;

    /**
     * @param ExtractEquipmentService $equipmentService
     */
    public function __construct(ExtractEquipmentService $equipmentService)
    {
        $this->extractEquipmentService = $equipmentService;
    }

    /**
     * @param TransferRequestGetEquipmentExtractCommand $command
     *
     * @return \League\Csv\Writer
     *
     * @throws InvalidArgumentException
     */
    public function handle(TransferRequestGetEquipmentExtractCommand $command)
    {
        return $this->extractEquipmentService->getCsv(
            new DateTime('@' . $command->getShipmentTimestampFrom()),
            new DateTime('@' . $command->getShipmentTimestampTo())
        );
    }
}
