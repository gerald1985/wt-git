<?php

namespace ServiceBundle\CommandBus\Handler\TransferRequest;

use DomainBundle\Entity\TransferRequest\TransferRequest;
use ServiceBundle\CommandBus\Command\TransferRequest\TransferRequestCancelCommand;
use ServiceBundle\Exception\Handler\TransferRequest\TransferRequestNotFoundHandlerException;
use ServiceBundle\Model\Repository\TransferRequest\TransferRequestRepository;
use ServiceBundle\Services\CancelPendingTransferRequestService;

class TransferRequestCancelHandler
{
    /** @var TransferRequestRepository */
    protected $transferRequestRepository;
    /** @var CancelPendingTransferRequestService */
    protected $cancelPendingTransferRequestService;

    public function __construct(
        TransferRequestRepository $transferRequestRepository,
        CancelPendingTransferRequestService $cancelPendingTransferRequestService
    )
    {
        $this->transferRequestRepository = $transferRequestRepository;
        $this->cancelPendingTransferRequestService = $cancelPendingTransferRequestService;
    }


    public function handle(TransferRequestCancelCommand $command)
    {
        /** @var TransferRequest $entity */
        $entity = $this->transferRequestRepository->find($command->getId());
        if (!($entity instanceof TransferRequest)) {
            throw new TransferRequestNotFoundHandlerException();
        }

        $this->cancelPendingTransferRequestService->cancelRequest($entity);
        return $entity;
    }
}
