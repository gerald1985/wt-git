<?php

namespace ServiceBundle\CommandBus\Handler\TransferRequest;

use DomainBundle\Entity\Store\Store;
use DomainBundle\Entity\Store\StoreItem;
use DomainBundle\Entity\TransferRequest\TransferRequest;
use DomainBundle\Entity\User;
use Doctrine\ORM\EntityManager;
use DomainBundle\Model\RepositoryInterface;
use ServiceBundle\CommandBus\Command\TransferRequest\TransferRequestCreateCommand;
use ServiceBundle\Event\TransferRequest\TransferRequestEventEvent;
use ServiceBundle\Exception\Handler\Store\StoreItemIsUsedByOtherUserHandlerException;
use ServiceBundle\Exception\Validator\ValidationException;
use ServiceBundle\Model\Repository\Store\StoreItemRepository;
use ServiceBundle\Model\Repository\Store\StoreRepository;
use ServiceBundle\Model\Repository\TransferRequest\TransferRequestRepository;
use ServiceBundle\Security\OrderVoter;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;


class TransferRequestCreateHandler
{
    /**
     * @var User
     */
    protected $tokenUser;

    /**
     * @var EntityManager
     */
    protected $em;
    /**
     * @var TransferRequestRepository
     */
    protected $transferRequestRepository;

    /**
     * @var StoreItemRepository
     */
    protected $storeItemRepository;

    /**
     * @var StoreRepository
     */
    protected $storeRepository;
    /** @var EventDispatcherInterface */
    protected $eventDispatcher;

    public function __construct(
        User $tokenUser,
        EntityManager $em,
        RepositoryInterface $transferRequestRepository,
        RepositoryInterface $storeItemRepository,
        RepositoryInterface $storeRepository,
        EventDispatcherInterface $eventDispatcher
    )
    {
        $this->em = $em;
        $this->tokenUser = $tokenUser;
        $this->transferRequestRepository = $transferRequestRepository;
        $this->storeItemRepository = $storeItemRepository;
        $this->storeRepository = $storeRepository;
        $this->eventDispatcher = $eventDispatcher;
    }


    public function handle(TransferRequestCreateCommand $command)
    {
        $class = $this->transferRequestRepository->getClassName();
        /** @var TransferRequest $entity */
        $entity = new $class;

        $this->setItem($command, $entity);
        $this->setShipmentDate($command, $entity);
        $this->setReceiverStore($command, $entity);
        $this->checkFromTo($entity);

        $entity->setRequestedBy($this->tokenUser);
        $entity->setRequestDatetime(new \DateTime());
        $entity->setStatus(TransferRequest::STATUS_PENDING);
        $entity->setPurpose($command->getPurpose());
        $entity->setReceiverUserName($command->getReceiverUserName());
        $entity->setGetEmailUpdates($command->isGetEmailUpdates() ? 1 : 0);


        $this->em->persist($entity);

        $event = new TransferRequestEventEvent($entity, null, $command->getLastNote());
        $this->eventDispatcher->dispatch('tr_event.placement', $event);

        return $entity;
    }


    /**
     * @param TransferRequestCreateCommand $command
     * @param TransferRequest $entity
     * @throws StoreItemIsUsedByOtherUserHandlerException
     * @throws ValidationException
     */
    public function setItem(TransferRequestCreateCommand $command, TransferRequest $entity)
    {
        /** @var StoreItem $storeItem */
        $storeItem = $this->storeItemRepository->find($command->getStoreItemId());
        if (!($storeItem instanceof StoreItem)) {
            throw new ValidationException(
                [
                    'storeItemId' => ['not.exist.storeItem']
                ]
            );
        }
        if ($storeItem->getStore()->getStatus() !== Store::STATUS_ACTIVE) {
            throw new ValidationException(
                [
                    'storeItemId' => ['item.store.inactive']
                ]
            );
        }

        foreach ($storeItem->getTransferRequests() as $transferRequest) {
            /** @var TransferRequest $transferRequest */
            if (
                $transferRequest->getStatus() === TransferRequest::STATUS_APPROVED
                || $transferRequest->getStatus() === TransferRequest::STATUS_PENDING
            ) {
                throw new ValidationException(
                    [
                        'storeItemId' => ['is.used.store_item']
                    ]
                );
            }
        }

        $entity->setStoreItem($storeItem);
    }


    /**
     * @param TransferRequestCreateCommand $command
     * @param TransferRequest $entity
     * @throws ValidationException
     */
    public function setShipmentDate(TransferRequestCreateCommand $command, TransferRequest $entity)
    {
        if ($command->getShipmentTimestamp()) {
            if (
                !$this->tokenUser->hasRole(User::ROLE_ADMIN)
                && !$this->tokenUser->hasRole(User::ROLE_TRANSFER_COORDINATOR)
            ) {
                throw new ValidationException(
                    [
                        'shipmentTimestamp' => ['no.access.to.set.field']
                    ]
                );
            }

            $entity->setShipmentDate(new \DateTime('@' . $command->getShipmentTimestamp()));
        }
    }


    /**
     * @param TransferRequestCreateCommand $command
     * @param TransferRequest $entity
     * @throws ValidationException
     */
    public function setReceiverStore(TransferRequestCreateCommand $command, TransferRequest $entity)
    {
        if (
            $this->tokenUser->hasRole(User::ROLE_ADMIN)
            || $this->tokenUser->hasRole(User::ROLE_TRANSFER_COORDINATOR)
        ) {
            if (!$command->getStoreReceiverId()) {
                throw new ValidationException(
                    [
                        'storeReceiverId' => ['validation.not_null.choose.receive_store']
                    ]
                );
            }
            /** @var Store $store */
            $store = $this->storeRepository->softFind($command->getStoreReceiverId());
            if (
                !($store instanceof Store)
                || $store->getStatus() !== Store::STATUS_ACTIVE
            ) {
                throw new ValidationException(
                    [
                        'storeReceiverId' => ['not.exist.store']
                    ]
                );
            }
        }


        if (
            !$this->tokenUser->hasRole(User::ROLE_ADMIN)
            && !$this->tokenUser->hasRole(User::ROLE_TRANSFER_COORDINATOR)
        ) {
            $store = $this->tokenUser->getStores()->first();
            if (
                empty($this->tokenUser->getStores())
                || !($store instanceof Store)
                || $store->getStatus() !== Store::STATUS_ACTIVE
            ) {
                throw new ValidationException(
                    [
                        'storeReceiverId' => ['manager.not.bind.with.store']
                    ]
                );
            }
        }


        $entity->setStoreReceiver($store);
    }


    /**
     * @param TransferRequest $entity
     * @throws ValidationException
     */
    public function checkFromTo(TransferRequest $entity)
    {
        if ($entity->getStoreItem()->getStore()->getId() === $entity->getStoreReceiver()->getId()) {
            throw new ValidationException(
                [
                    'storeItemId'     => ['stores.from_and_to.is.match'],
                    'storeReceiverId' => ['stores.from_and_to.is.match']
                ]
            );
        }
    }
}