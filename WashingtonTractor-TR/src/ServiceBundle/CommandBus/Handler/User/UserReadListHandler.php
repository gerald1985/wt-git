<?php

namespace ServiceBundle\CommandBus\Handler\User;

use Doctrine\ORM\QueryBuilder;
use DomainBundle\Model\RepositoryInterface;
use ServiceBundle\CommandBus\Command\User\UserReadListCommand;
use ServiceBundle\Model\Repository\UserRepository;
use ServiceBundle\Security\OrderVoter;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;


class UserReadListHandler
{
    /**
     * @var AuthorizationCheckerInterface
     */
    protected $authorizationChecker;

    /**
     * @var UserRepository
     */
    protected $userRepository;

    public function __construct(
        AuthorizationCheckerInterface $authorizationChecker,
        RepositoryInterface $userRepository
    )
    {
        $this->userRepository = $userRepository;
        $this->authorizationChecker = $authorizationChecker;
    }


    /**
     * @param UserReadListCommand $command
     * @return \Doctrine\ORM\Query
     */
    public function handle(UserReadListCommand $command)
    {
        /** @var QueryBuilder $q */
        if (!empty($command->getStatuses()) && in_array('DELETED', $command->getStatuses())) {
            $q = $this->userRepository->createQueryBuilder('o');
        } else {
            $q = $this->userRepository->softCreateQueryBuilder('o');
        }


        //search by status
        if (!empty($command->getStatuses())) {
            if (in_array('DELETED', $command->getStatuses())) {
                $q->andWhere(
                    $q->expr()->orX(
                        $q->expr()->eq('o.isDeleted', ':isDeleted'),
                        $q->expr()->in('o.status', $command->getStatuses())
                    )
                );
                $q->setParameter(':isDeleted', 1);
            } else {
                $q->andWhere($q->expr()->in('o.status', $command->getStatuses()));
            }
        }
        //search by roles
        if ($command->getRoles()) {
            $args = [];
            foreach ($command->getRoles() as $key => $role) {
                $args[] = 'o.roles LIKE :role' . $key;
                $q->setParameter(':role' . $key, "%{$role}%");
            }
            $q->andWhere(implode(' OR ', $args));
        }

        $q->orderBy('o.firstName', 'asc');
        $q->orderBy('o.lastName', 'asc');

        return $q->getQuery();
    }
}