<?php

namespace ServiceBundle\CommandBus\Handler\User;

use DomainBundle\Entity\User;
use Doctrine\ORM\EntityManager;
use DomainBundle\Model\RepositoryInterface;
use FOS\UserBundle\Doctrine\UserManager;
use FOS\UserBundle\Model\UserManagerInterface;
use ServiceBundle\CommandBus\Command\User\UserChangePasswordWithTokenCommand;
use ServiceBundle\Exception\Handler\User\UserResetTokenNotFoundHandlerException;
use ServiceBundle\Model\Repository\UserRepository;
use ServiceBundle\Security\OrderVoter;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;


class UserChangePasswordWithTokenHandler
{
    /**
     * @var EntityManager
     */
    protected $em;

    /**
     * @var AuthorizationCheckerInterface
     */
    protected $authorizationChecker;

    /**
     * @var UserRepository
     */
    protected $userRepository;

    /**
     * @var UserManager
     */
    protected $userManager;
    /** @var EventDispatcherInterface */
    protected $eventDispatcher;

    public function __construct(
        EntityManager $em,
        UserManagerInterface $userManager,
        RepositoryInterface $userRepository,
        EventDispatcherInterface $eventDispatcher
    )
    {
        $this->em = $em;
        $this->userRepository = $userRepository;
        $this->userManager = $userManager;
        $this->eventDispatcher = $eventDispatcher;
    }


    /**
     * @param UserChangePasswordWithTokenCommand $command
     * @return User
     * @throws UserResetTokenNotFoundHandlerException
     */
    public function handle(UserChangePasswordWithTokenCommand $command)
    {
        /** @var User $entity */
        $entity = $this->userRepository->softFindOneBy(
            [
                $this->em->getExpressionBuilder()->eq('o.confirmationToken', ':confirmationToken')
            ],
            ['confirmationToken' => $command->getToken()]
        );
        if (!($entity instanceof User)) {
            throw new UserResetTokenNotFoundHandlerException();
        }

        $entity->setPlainPassword($command->getPassword());
        $entity->setConfirmationToken(null);

        $this->userManager->updateUser($entity);

        return $entity;
    }
}