<?php

namespace ServiceBundle\CommandBus\Handler\User;

use Doctrine\ORM\ORMInvalidArgumentException;
use DomainBundle\Entity\Store\Store;
use DomainBundle\Entity\User;
use Doctrine\ORM\EntityManager;
use DomainBundle\Model\RepositoryInterface;
use FOS\UserBundle\Doctrine\UserManager;
use FOS\UserBundle\Model\UserManagerInterface;
use ServiceBundle\CommandBus\Command\User\UserApproveCommand;
use ServiceBundle\Event\Email\SendEmailEvent;
use ServiceBundle\Exception\Handler\User\UserIsDeclinedException;
use ServiceBundle\Exception\Handler\User\UserIsNotExistHandlerException;
use ServiceBundle\Model\Repository\Store\StoreRepository;
use ServiceBundle\Model\Repository\UserRepository;
use ServiceBundle\Security\OrderVoter;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;


class UserApproveHandler
{
    /**
     * @var User
     */
    protected $tokenUser;

    /**
     * @var EntityManager
     */
    protected $em;

    /**
     * @var AuthorizationCheckerInterface
     */
    protected $authorizationChecker;

    /**
     * @var UserRepository
     */
    protected $userRepository;

    /**
     * @var UserManager
     */
    protected $userManager;

    /**
     * @var StoreRepository
     */
    protected $storeRepository;
    /** @var EventDispatcherInterface */
    protected $eventDispatcher;

    public function __construct(
        User $tokenUser,
        EntityManager $em,
        AuthorizationCheckerInterface $authorizationChecker,
        UserManagerInterface $userManager,
        RepositoryInterface $userRepository,
        RepositoryInterface $StoreRepository,
        EventDispatcherInterface $eventDispatcher
    )
    {
        $this->em = $em;
        $this->tokenUser = $tokenUser;
        $this->authorizationChecker = $authorizationChecker;
        $this->userRepository = $userRepository;
        $this->userManager = $userManager;
        $this->storeRepository = $StoreRepository;
        $this->eventDispatcher = $eventDispatcher;
    }


    /**
     * @param UserApproveCommand $command
     * @return User
     * @throws UserIsNotExistHandlerException
     * @throws ORMInvalidArgumentException
     * @throws UserIsDeclinedException
     */
    public function handle(UserApproveCommand $command)
    {
        /** @var User $entity */
        $entity = $this->userRepository->softFind($command->getId());
        if (!($entity instanceof User)) {
            throw new UserIsNotExistHandlerException();
        }
        if ($entity->getStatus() === User::STATUS_DECLINED) {
            throw new UserIsDeclinedException();
        }

        $entity->setEnabled(true);
        $entity->setStatus(User::STATUS_REGISTERED);

        //set new manager to store
        if (
            $entity->hasRole(User::ROLE_MANAGER_STORE)
            && $entity->getStores()->count()
        ) {
            /** @var Store $store */
            $store = $entity->getStores()->first();
            $managerOld = $store->getManager();
            if ($managerOld instanceof User) {
                $managerOld->removeStore($store);
                $this->em->persist($managerOld);
            }
            $store->setManager($entity);
            $store->setStatus(Store::STATUS_ACTIVE);
            $this->em->persist($store);
        }


//        if (!$this->authorizationChecker->isGranted(StoreVoter::CREATE_STORE, $entity)) {
//            throw new VoterException(StoreVoter::CREATE_STORE);
//        }

        $this->userManager->updateUser($entity);

        $event = new SendEmailEvent();
        $event->addTemplateData('user', $entity);
        $event->addTemplateData('admin', $this->tokenUser);
        $this->eventDispatcher->dispatch('send_email.user_approved', $event);

        return $entity;
    }
}