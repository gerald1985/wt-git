<?php

namespace ServiceBundle\CommandBus\Handler\User;

use DomainBundle\Entity\User;
use Doctrine\ORM\EntityManager;
use DomainBundle\Model\RepositoryInterface;
use FOS\UserBundle\Doctrine\UserManager;
use FOS\UserBundle\Model\UserManagerInterface;
use ServiceBundle\CommandBus\Command\User\UserCreateCommand;
use ServiceBundle\Event\Email\SendEmailEvent;
use ServiceBundle\Exception\Handler\Store\StoreIsNotExistHandlerException;
use ServiceBundle\Exception\Validator\UniqueException;
use ServiceBundle\Exception\Voter\VoterException;
use ServiceBundle\Model\Repository\Store\StoreRepository;
use ServiceBundle\Model\Repository\UserRepository;
use ServiceBundle\Security\OrderVoter;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;


class UserCreateHandler
{
    /**
     * @var User
     */
    protected $tokenUser;

    /**
     * @var EntityManager
     */
    protected $em;

    /**
     * @var UserRepository
     */
    protected $userRepository;

    /**
     * @var UserManager
     */
    protected $userManager;

    /**
     * @var StoreRepository
     */
    protected $storeRepository;
    /** @var EventDispatcherInterface */
    protected $eventDispatcher;

    public function __construct(
        EntityManager $em,
        UserManagerInterface $userManager,
        RepositoryInterface $userRepository,
        RepositoryInterface $StoreRepository,
        EventDispatcherInterface $eventDispatcher
    )
    {
        $this->em = $em;
        $this->userRepository = $userRepository;
        $this->userManager = $userManager;
        $this->storeRepository = $StoreRepository;
        $this->eventDispatcher = $eventDispatcher;
    }


    /**
     * @param UserCreateCommand $command
     * @return User
     * @throws StoreIsNotExistHandlerException
     * @throws VoterException
     */
    public function handle(UserCreateCommand $command)
    {
        $this->uniqueChecker($command);

        /** @var User $entity */
        $entity = $this->userRepository->findOneBy(
            [
                'email' => $command->getEmail()
            ]
        );
        if (!($entity instanceof User)) {
            $entity = $this->userManager->createUser();
        }

        $entity->setEnabled(false);
        $entity->setIsDeleted(0);
        $entity->setFirstName($command->getFirstName());
        $entity->setLastName($command->getLastName());
        $entity->setEmail($command->getEmail());
        $entity->setUsername($command->getEmail());
        $entity->setPlainPassword($command->getPassword());
        $entity->setStatus(User::STATUS_NEW);

        $this->setRole($entity, $command);
        $this->setStore($entity, $command);


//        if (!$this->authorizationChecker->isGranted(StoreVoter::CREATE_STORE, $entity)) {
//            throw new VoterException(StoreVoter::CREATE_STORE);
//        }

        $this->userManager->updateUser($entity);

        $event = new SendEmailEvent();
        $event->addTemplateData('user', $entity);
        $this->eventDispatcher->dispatch('send_email.user_registered', $event);

        return $entity;
    }


    /**
     * @param UserCreateCommand $command
     * @throws UniqueException
     */
    public function uniqueChecker(UserCreateCommand $command)
    {
        $errors = [];
        if ($command->getEmail()) {
//            $find = $this->userRepository->softFindOneBy(
//                [
//                    $this->em->getExpressionBuilder()->eq('o.email', ':email'),
//                    $this->em->getExpressionBuilder()->eq('o.status', ':status'),
//                ],
//                [
//                    ':email' => $command->getEmail(),
//                    ':status' => User::STATUS_REGISTERED
//                ]
//            );
            $find = $this->userRepository->findOneBy(
                [
                    'email'     => $command->getEmail(),
                    'isDeleted' => 0
                ]
            );
            if ($find instanceof User) {
                $errors['email'][] = 'validation.email.registered';
            }
        }

        if (!empty($errors)) {
            throw new UniqueException($errors);
        }
    }


    /**
     * @param User $entity
     * @param UserCreateCommand $command
     */
    public function setRole(User $entity, UserCreateCommand $command)
    {
        if (!$entity->hasRole($command->getRole())) {
            foreach ($entity->getRoles() as $role) {
                $entity->removeRole($role);
            }
            $entity->addRole($command->getRole());
        }
    }


    /**
     * @param User $entity
     * @param UserCreateCommand $command
     * @throws StoreIsNotExistHandlerException
     */
    public function setStore(User $entity, UserCreateCommand $command)
    {
        if (
            $command->getRole() !== User::ROLE_MANAGER_SALE
            && $command->getRole() !== User::ROLE_MANAGER_STORE
        ) {
            return;
        }
        if ($command->getStoreId() !== null) {
            $storeAdd = $this->storeRepository->softFind($command->getStoreId());
            if (empty($storeAdd)) {
                throw new StoreIsNotExistHandlerException();
            }
        } else {
            $storeAdd = null;
        }
        foreach ($entity->getStores() as $store) {
            $entity->removeStore($store);
        }
        if ($storeAdd) {
            $entity->addStore($storeAdd);
        }
    }
}