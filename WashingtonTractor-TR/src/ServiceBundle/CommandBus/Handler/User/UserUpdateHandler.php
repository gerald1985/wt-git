<?php

namespace ServiceBundle\CommandBus\Handler\User;

use Doctrine\ORM\QueryBuilder;
use DomainBundle\Entity\Store\Store;
use DomainBundle\Entity\TransferRequest\TransferRequest;
use DomainBundle\Entity\User;
use Doctrine\ORM\EntityManager;
use DomainBundle\Model\RepositoryInterface;
use FOS\UserBundle\Doctrine\UserManager;
use FOS\UserBundle\Model\UserManagerInterface;
use ServiceBundle\CommandBus\Command\User\UserUpdateCommand;
use ServiceBundle\Exception\Handler\Store\StoreIsNotExistHandlerException;
use ServiceBundle\Exception\Handler\User\UserIsNotExistHandlerException;
use ServiceBundle\Exception\Validator\UniqueException;
use ServiceBundle\Exception\Validator\ValidationException;
use ServiceBundle\Model\Repository\Store\StoreRepository;
use ServiceBundle\Model\Repository\TransferRequest\TransferRequestRepository;
use ServiceBundle\Model\Repository\UserRepository;
use ServiceBundle\Security\OrderVoter;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;


class UserUpdateHandler
{
    /**
     * @var User
     */
    protected $tokenUser;

    /**
     * @var EntityManager
     */
    protected $em;

    /**
     * @var AuthorizationCheckerInterface
     */
    protected $authorizationChecker;

    /**
     * @var UserRepository
     */
    protected $userRepository;

    /**
     * @var UserManager
     */
    protected $userManager;

    /**
     * @var StoreRepository
     */
    protected $storeRepository;
    /**
     * @var TransferRequestRepository
     */
    protected $transferRequestRepository;

    public function __construct(
        User $tokenUser,
        EntityManager $em,
        AuthorizationCheckerInterface $authorizationChecker,
        UserManagerInterface $userManager,
        RepositoryInterface $userRepository,
        RepositoryInterface $StoreRepository,
        RepositoryInterface $transferRequestRepository
    )
    {
        $this->em = $em;
        $this->tokenUser = $tokenUser;
        $this->authorizationChecker = $authorizationChecker;
        $this->userRepository = $userRepository;
        $this->userManager = $userManager;
        $this->storeRepository = $StoreRepository;
        $this->transferRequestRepository = $transferRequestRepository;
    }


    /**
     * @param UserUpdateCommand $command
     * @return User
     * @throws UserIsNotExistHandlerException
     */
    public function handle(UserUpdateCommand $command)
    {
        $this->uniqueChecker($command);

        /** @var User $entity */
        $entity = $this->userRepository->softFind($command->getId());
        if (!($entity instanceof User)) {
            throw new UserIsNotExistHandlerException();
        }

        $entity->setFirstName($command->getFirstName());
        $entity->setLastName($command->getLastName());
        $entity->setEmail($command->getEmail());
        $entity->setUsername($command->getEmail());
        $this->setRole($entity, $command);
        $this->setStore($entity, $command);


//        if (!$this->authorizationChecker->isGranted(StoreVoter::CREATE_STORE, $entity)) {
//            throw new VoterException(StoreVoter::CREATE_STORE);
//        }

        $this->userManager->updateUser($entity);

        return $entity;
    }


    /**
     * @param UserUpdateCommand $command
     * @throws UniqueException
     */
    public function uniqueChecker(UserUpdateCommand $command)
    {
        $errors = [];
        if ($command->getEmail()) {
//            $find = $this->userRepository->softFindOneBy(
//                [
//                    $this->em->getExpressionBuilder()->neq('o.id', ':id'),
//                    $this->em->getExpressionBuilder()->eq('o.email', ':email'),
//                    $this->em->getExpressionBuilder()->eq('o.status', ':status'),
//                ],
//                [
//                    ':id'     => $command->getId(),
//                    ':email'  => $command->getEmail(),
//                    ':status' => User::STATUS_REGISTERED
//                ]
//            );

            $find = $this->userRepository->createQueryBuilder('o');

            $find->andWhere($this->em->getExpressionBuilder()->neq('o.id', ':id'));
            $find->setParameter('id', $command->getId());
            $find->andWhere($this->em->getExpressionBuilder()->eq('o.email', ':email'));
            $find->setParameter('email', $command->getEmail());
            $find->andWhere($this->em->getExpressionBuilder()->eq('o.status', ':status'));
            $find->setParameter('status', User::STATUS_REGISTERED);
            $find = $find->getQuery()->getOneOrNullResult();

            if ($find instanceof User) {
                $errors['email'][] = 'validation.email.registered';
            }
        }

        if (!empty($errors)) {
            throw new UniqueException($errors);
        }
    }


    /**
     * @param User $entity
     * @param UserUpdateCommand $command
     */
    public function setRole(User $entity, UserUpdateCommand $command)
    {
        if (!$entity->hasRole($command->getRole())) {
            foreach ($entity->getRoles() as $role) {
                $entity->removeRole($role);
            }
            $entity->addRole($command->getRole());
        }
    }


    /**
     * @param User $entity
     * @param UserUpdateCommand $command
     * @throws StoreIsNotExistHandlerException
     * @throws ValidationException
     */
    public function setStore(User $entity, UserUpdateCommand $command)
    {
        if (
            $command->getRole() !== User::ROLE_MANAGER_SALE
            && $command->getRole() !== User::ROLE_MANAGER_STORE
        ) {
            return;
        }
        if ($command->getStoreId() !== null) {
            /** @var Store $storeAdd */
            $storeAdd = $this->storeRepository->softFind($command->getStoreId());
            if (empty($storeAdd)) {
                throw new StoreIsNotExistHandlerException();
            }

            //set new manager to store
            if (
                $entity->hasRole(User::ROLE_MANAGER_STORE)
                && $entity->getStores()->count()
            ) {
                /** @var Store $oldStore */
                $oldStore = $entity->getStores()->first();
                if ($command->getStoreId() !== $oldStore->getId()) {
                    //delete manager from old store
                    $this->checkPendingTransferRequests($entity->getId());
                    $oldStore->setManager(null);
                    $this->em->persist($oldStore);
                    $this->em->flush($oldStore);
                }
            }

            //delete store employee from new store
            $oldStoreManager = $storeAdd->getManager();
            if ($oldStoreManager instanceof User) {
                $oldStoreManager->removeStore($storeAdd);
                $this->em->persist($oldStoreManager);
            }

            //set manager from new store
            $storeAdd->setManager($entity);
            $storeAdd->setStatus(Store::STATUS_ACTIVE);
            $this->em->persist($storeAdd);

        } else {
            $storeAdd = null;
        }

        //delete store employee
        foreach ($entity->getStores() as $store) {
            $entity->removeStore($store);
        }
        //add new store employee
        if ($storeAdd) {
            $entity->addStore($storeAdd);
        }
    }


    /**
     * @param integer $managerId
     * @throws ValidationException
     */
    public function checkPendingTransferRequests($managerId)
    {
        /** @var QueryBuilder $q */
        $q = $this->transferRequestRepository->CreateQueryBuilder('o');
        $q->leftJoin('o.storeItem', 'osi');
        $q->leftJoin('osi.store', 'osis');
        $q->leftJoin('osis.manager', 'osism');

        $q->andWhere($q->expr()->in('o.status', [TransferRequest::STATUS_PENDING]));
        $q->andWhere($q->expr()->eq('osism.id', $managerId));

        $q->setMaxResults(1);

        $first = $q->getQuery()->getOneOrNullResult();
        if ($first instanceof TransferRequest) {
            throw new ValidationException(
                [
                    'managerId' => ['store.manager.has.pending.outgoing.tr']
                ]
            );
        }
    }
}