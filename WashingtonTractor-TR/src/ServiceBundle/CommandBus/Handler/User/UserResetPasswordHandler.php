<?php

namespace ServiceBundle\CommandBus\Handler\User;

use DomainBundle\Entity\User;
use Doctrine\ORM\EntityManager;
use DomainBundle\Model\RepositoryInterface;
use FOS\UserBundle\Doctrine\UserManager;
use FOS\UserBundle\Model\UserManagerInterface;
use ServiceBundle\CommandBus\Command\User\UserDeclineCommand;
use ServiceBundle\CommandBus\Command\User\UserResetPasswordCommand;
use ServiceBundle\Event\Email\SendEmailEvent;
use ServiceBundle\Exception\Handler\User\UserIsNotExistHandlerException;
use ServiceBundle\Model\Repository\Store\StoreRepository;
use ServiceBundle\Model\Repository\UserRepository;
use ServiceBundle\Security\OrderVoter;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;


class UserResetPasswordHandler
{
    /**
     * @var EntityManager
     */
    protected $em;

    /**
     * @var AuthorizationCheckerInterface
     */
    protected $authorizationChecker;

    /**
     * @var UserRepository
     */
    protected $userRepository;

    /**
     * @var UserManager
     */
    protected $userManager;
    /** @var EventDispatcherInterface */
    protected $eventDispatcher;

    public function __construct(
        EntityManager $em,
        UserManagerInterface $userManager,
        RepositoryInterface $userRepository,
        EventDispatcherInterface $eventDispatcher
    )
    {
        $this->em = $em;
        $this->userRepository = $userRepository;
        $this->userManager = $userManager;
        $this->eventDispatcher = $eventDispatcher;
    }


    /**
     * @param UserResetPasswordCommand $command
     * @return User
     * @throws UserIsNotExistHandlerException
     */
    public function handle(UserResetPasswordCommand $command)
    {
        /** @var User $entity */
        $entity = $this->userRepository->softFindOneBy(
            [
                $this->em->getExpressionBuilder()->eq('o.email', ':email')
            ],
            ['email' => $command->getEmail()]
        );
        if (!($entity instanceof User)) {
            throw new UserIsNotExistHandlerException();
        }

        $entity->setConfirmationToken(md5($command->getEmail()) . md5(time()));
        $entity->setPasswordRequestedAt(new \DateTime());

        $this->userManager->updateUser($entity);

        $event = new SendEmailEvent();
        $event->addTemplateData('user', $entity);
        $this->eventDispatcher->dispatch('send_email.user_reset_password', $event);

        return $entity;
    }
}