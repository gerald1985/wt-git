<?php

namespace ServiceBundle\CommandBus\Handler\User;

use DomainBundle\Entity\User;
use FOS\UserBundle\Doctrine\UserManager;
use FOS\UserBundle\Model\UserManagerInterface;
use ServiceBundle\CommandBus\Command\User\UserChangePasswordCommand;
use ServiceBundle\Exception\Handler\User\UserPasswordIsNotCorrectHandlerException;
use ServiceBundle\Exception\Handler\User\UserPasswordIsTheSameHandlerException;
use Symfony\Component\Security\Core\Encoder\EncoderFactory;
use Symfony\Component\Security\Core\Encoder\EncoderFactoryInterface;
use Symfony\Component\Security\Core\Encoder\PasswordEncoderInterface;


class UserChangePasswordHandler
{
    /**
     * @var User
     */
    protected $tokenUser;
    /**
     * @var EncoderFactory
     */
    protected $passwordEncoderFactory;
    /**
     * @var UserManager
     */
    protected $userManager;

    public function __construct(
        User $tokenUser,
        EncoderFactoryInterface $passwordEncoderFactory,
        UserManagerInterface $userManager
    )
    {
        $this->tokenUser = $tokenUser;
        $this->passwordEncoderFactory = $passwordEncoderFactory;
        $this->userManager = $userManager;
    }


    /**
     * @param UserChangePasswordCommand $command
     * @return User
     * @throws UserPasswordIsNotCorrectHandlerException
     * @throws UserPasswordIsTheSameHandlerException
     */
    public function handle(UserChangePasswordCommand $command)
    {
        /** @var PasswordEncoderInterface $encoder */
        $encoder = $this->passwordEncoderFactory->getEncoder($this->tokenUser);

        /** @var User $entity */
        if (!$encoder->isPasswordValid($this->tokenUser->getPassword(), $command->getCurrentPassword(), $this->tokenUser->getSalt())) {
            throw new UserPasswordIsNotCorrectHandlerException();
        }

        if ($command->getNewPassword() === $command->getCurrentPassword()) {
            throw new UserPasswordIsTheSameHandlerException();
        }

        $this->tokenUser->setPlainPassword($command->getNewPassword());
        $this->userManager->updateUser($this->tokenUser);

        return $entity;
    }
}