<?php

namespace ServiceBundle\CommandBus\Handler\User;


use DomainBundle\Entity\User;
use DomainBundle\Model\RepositoryInterface;
use ServiceBundle\CommandBus\Command\User\UserReadCommand;
use ServiceBundle\Exception\Handler\User\UserIsNotExistHandlerException;
use ServiceBundle\Model\Repository\UserRepository;
use ServiceBundle\Security\OrderVoter;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;


class UserReadHandler
{
    /**
     * @var AuthorizationCheckerInterface
     */
    protected $authorizationChecker;

    /**
     * @var UserRepository
     */
    protected $userRepository;

    public function __construct(
        AuthorizationCheckerInterface $authorizationChecker,
        RepositoryInterface $userRepository
    )
    {
        $this->userRepository = $userRepository;
        $this->authorizationChecker = $authorizationChecker;
    }


    /**
     * @param UserReadCommand $command
     * @return User
     * @throws UserIsNotExistHandlerException
     */
    public function handle(UserReadCommand $command)
    {
        /** @var User $entity */
        $entity = $this->userRepository->softFind($command->getId());
        if (!($entity instanceof User)) {
            throw new UserIsNotExistHandlerException();
        }

        return $entity;
    }
}