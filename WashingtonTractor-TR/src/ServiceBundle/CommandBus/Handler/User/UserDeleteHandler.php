<?php

namespace ServiceBundle\CommandBus\Handler\User;

use Doctrine\ORM\QueryBuilder;
use DomainBundle\Entity\Store\Store;
use DomainBundle\Entity\TransferRequest\TransferRequest;
use DomainBundle\Entity\User;
use Doctrine\ORM\EntityManager;
use DomainBundle\Model\RepositoryInterface;
use FOS\UserBundle\Doctrine\UserManager;
use FOS\UserBundle\Model\UserManagerInterface;
use ServiceBundle\CommandBus\Command\User\UserDeleteCommand;
use ServiceBundle\Exception\Handler\Manager\ManagerHasPendingTransferRequestsHandlerException;
use ServiceBundle\Exception\Handler\Store\StoreIsNotExistHandlerException;
use ServiceBundle\Model\Repository\Store\StoreRepository;
use ServiceBundle\Model\Repository\TransferRequest\TransferRequestRepository;
use ServiceBundle\Model\Repository\UserRepository;
use ServiceBundle\Security\OrderVoter;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;


class UserDeleteHandler
{
    /**
     * @var User
     */
    protected $tokenUser;

    /**
     * @var EntityManager
     */
    protected $em;

    /**
     * @var AuthorizationCheckerInterface
     */
    protected $authorizationChecker;

    /**
     * @var UserRepository
     */
    protected $userRepository;

    /**
     * @var UserManager
     */
    protected $userManager;

    /**
     * @var StoreRepository
     */
    protected $storeRepository;
    /**
     * @var TransferRequestRepository
     */
    protected $transferRequestRepository;

    public function __construct(
        User $tokenUser,
        EntityManager $em,
        AuthorizationCheckerInterface $authorizationChecker,
        UserManagerInterface $userManager,
        RepositoryInterface $userRepository,
        RepositoryInterface $StoreRepository,
        RepositoryInterface $transferRequestRepository
    )
    {
        $this->em = $em;
        $this->tokenUser = $tokenUser;
        $this->authorizationChecker = $authorizationChecker;
        $this->userRepository = $userRepository;
        $this->userManager = $userManager;
        $this->storeRepository = $StoreRepository;
        $this->transferRequestRepository = $transferRequestRepository;
    }


    /**
     * @param UserDeleteCommand $command
     */
    public function handle(UserDeleteCommand $command)
    {
        /** @var User $entity */
        $entity = $this->userRepository->softFind($command->getId());
        if ($entity instanceof User) {

            $this->checkPendingTransferRequests($entity, $command);
            $this->deleteFromStore($entity, $command);

            $entity->setEnabled(false);
            $entity->setIsDeleted(1);


//        if (!$this->authorizationChecker->isGranted(StoreVoter::CREATE_STORE, $entity)) {
//            throw new VoterException(StoreVoter::CREATE_STORE);
//        }

            $this->userManager->updateUser($entity);
        }
    }


    /**
     * @param User $entity
     * @param UserDeleteCommand $command
     * @throws ManagerHasPendingTransferRequestsHandlerException
     */
    public function checkPendingTransferRequests(User $entity, UserDeleteCommand $command)
    {
        if (!$entity->hasRole(User::ROLE_MANAGER_STORE)) {
            return;
        }
        /** @var QueryBuilder $q */
        $q = $this->transferRequestRepository->CreateQueryBuilder('o');
        $q->andWhere($q->expr()->eq('o.status', ':status'));
        $q->setParameter('status', TransferRequest::STATUS_PENDING);

        $q->leftJoin('o.storeItem', 'osi');
        $q->leftJoin('osi.store', 'osis');
        $q->leftJoin('osis.manager', 'osism');
        $q->andWhere($q->expr()->eq('osism.id', $entity->getId()));
        $q->setMaxResults(1);

        $first = $q->getQuery()->getOneOrNullResult();
        if ($first instanceof TransferRequest) {
            throw new ManagerHasPendingTransferRequestsHandlerException('delete.user.with.pending.tr');
        }
    }


    /**
     * @param User $entity
     * @param UserDeleteCommand $command
     * @throws StoreIsNotExistHandlerException
     */
    public function deleteFromStore(User $entity, UserDeleteCommand $command)
    {
        if (!$entity->hasRole(User::ROLE_MANAGER_STORE)) {
            return;
        }
        $store = $this->storeRepository->softFindOneBy(
            [
                $this->em->getExpressionBuilder()->eq('o.manager', ':manager')
            ],
            [
                ':manager'     => $entity
            ]
        );
        if ($store instanceof Store) {
            /** @var Store $store */
            $store->setManager(null);
            $store->setStatus(Store::STATUS_INACTIVE);
            $this->em->persist($store);
        }
    }
}