<?php

namespace ServiceBundle\CommandBus\Handler\User;

use DomainBundle\Entity\User;
use Doctrine\ORM\EntityManager;
use DomainBundle\Model\RepositoryInterface;
use FOS\UserBundle\Doctrine\UserManager;
use FOS\UserBundle\Model\UserManagerInterface;
use ServiceBundle\CommandBus\Command\User\UserDeclineCommand;
use ServiceBundle\Event\Email\SendEmailEvent;
use ServiceBundle\Exception\Handler\User\UserIsApprovedException;
use ServiceBundle\Exception\Handler\User\UserIsNotExistHandlerException;
use ServiceBundle\Model\Repository\Store\StoreRepository;
use ServiceBundle\Model\Repository\UserRepository;
use ServiceBundle\Security\OrderVoter;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;


class UserDeclineHandler
{
    /**
     * @var User
     */
    protected $tokenUser;

    /**
     * @var EntityManager
     */
    protected $em;

    /**
     * @var AuthorizationCheckerInterface
     */
    protected $authorizationChecker;

    /**
     * @var UserRepository
     */
    protected $userRepository;

    /**
     * @var UserManager
     */
    protected $userManager;

    /**
     * @var StoreRepository
     */
    protected $storeRepository;
    /** @var EventDispatcherInterface */
    protected $eventDispatcher;

    public function __construct(
        User $tokenUser,
        EntityManager $em,
        AuthorizationCheckerInterface $authorizationChecker,
        UserManagerInterface $userManager,
        RepositoryInterface $userRepository,
        RepositoryInterface $StoreRepository,
        EventDispatcherInterface $eventDispatcher
    )
    {
        $this->em = $em;
        $this->tokenUser = $tokenUser;
        $this->authorizationChecker = $authorizationChecker;
        $this->userRepository = $userRepository;
        $this->userManager = $userManager;
        $this->storeRepository = $StoreRepository;
        $this->eventDispatcher = $eventDispatcher;
    }


    /**
     * @param UserDeclineCommand $command
     * @return User
     * @throws UserIsNotExistHandlerException
     * @throws UserIsApprovedException
     */
    public function handle(UserDeclineCommand $command)
    {
        /** @var User $entity */
        $entity = $this->userRepository->softFind($command->getId());
        if (!($entity instanceof User)) {
            throw new UserIsNotExistHandlerException();
        }
        if ($entity->getStatus() === User::STATUS_REGISTERED) {
            throw new UserIsApprovedException();
        }

        $entity->setEnabled(false);
        $entity->setStatus(User::STATUS_DECLINED);
        $entity->setIsDeleted(1);


//        if (!$this->authorizationChecker->isGranted(StoreVoter::CREATE_STORE, $entity)) {
//            throw new VoterException(StoreVoter::CREATE_STORE);
//        }

        $this->userManager->updateUser($entity);

        $event = new SendEmailEvent();
        $event->addTemplateData('user', $entity);
        $event->addTemplateData('admin', $this->tokenUser);
        $event->addTemplateData('note', $command->getNote());
        $this->eventDispatcher->dispatch('send_email.user_declined', $event);

        return $entity;
    }
}