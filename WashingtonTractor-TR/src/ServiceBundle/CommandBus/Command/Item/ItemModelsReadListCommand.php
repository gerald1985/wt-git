<?php

namespace ServiceBundle\CommandBus\Command\Item;

use ServiceBundle\CommandBus\Command\BaseCommandAbstract;

class ItemModelsReadListCommand extends BaseCommandAbstract
{
    /** @var integer */
    protected $page = '1';
    /** @var integer */
    protected $perPage = '10';

    /**
     * @return int
     */
    public function getPage()
    {
        return $this->page;
    }

    /**
     * @return int
     */
    public function getPerPage()
    {
        return $this->perPage;
    }
}