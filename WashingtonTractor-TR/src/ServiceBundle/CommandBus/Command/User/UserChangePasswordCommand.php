<?php

namespace ServiceBundle\CommandBus\Command\User;

use ServiceBundle\CommandBus\Command\BaseCommandAbstract;

class UserChangePasswordCommand extends BaseCommandAbstract
{
    /** @var string */
    protected $currentPassword;

    /** @var string */
    protected $newPassword;

    /**
     * @return string
     */
    public function getCurrentPassword()
    {
        return $this->currentPassword;
    }

    /**
     * @return string
     */
    public function getNewPassword()
    {
        return $this->newPassword;
    }
}