<?php

namespace ServiceBundle\CommandBus\Command\User;

use ServiceBundle\CommandBus\Command\BaseCommandAbstract;

class UserReadCommand extends BaseCommandAbstract
{
    /** @var integer */
    protected $id;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
}