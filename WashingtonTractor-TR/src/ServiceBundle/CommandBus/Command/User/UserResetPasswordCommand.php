<?php

namespace ServiceBundle\CommandBus\Command\User;

use ServiceBundle\CommandBus\Command\BaseCommandAbstract;

class UserResetPasswordCommand extends BaseCommandAbstract
{
    /** @var string */
    protected $email;

    /**
     * @return int
     */
    public function getEmail()
    {
        return $this->email;
    }
}