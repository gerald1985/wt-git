<?php

namespace ServiceBundle\CommandBus\Command\User;

use ServiceBundle\CommandBus\Command\BaseCommandAbstract;

class UserChangePasswordWithTokenCommand extends BaseCommandAbstract
{
    /** @var string */
    protected $token;

    /** @var string */
    protected $password;

    /**
     * @return string
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }
}