<?php

namespace ServiceBundle\CommandBus\Command\User;

use ServiceBundle\CommandBus\Command\BaseCommandAbstract;

class UserReadListCommand extends BaseCommandAbstract
{
    /** @var string */
    protected $roles = [];
    /** @var string */
    protected $statuses = [];
    /** @var integer */
    protected $page = '1';
    /** @var integer */
    protected $perPage = '10';

    /**
     * @return string
     */
    public function getStatuses()
    {
        return $this->statuses;
    }

    /**
     * @return int
     */
    public function getPage()
    {
        return $this->page;
    }

    /**
     * @return int
     */
    public function getPerPage()
    {
        return $this->perPage;
    }

    /**
     * @return string
     */
    public function getRoles()
    {
        return $this->roles;
    }
}