<?php

namespace ServiceBundle\CommandBus\Command\TransferRequest;

use ServiceBundle\CommandBus\Command\BaseCommandAbstract;

class TransferRequestNoteCreateCommand extends BaseCommandAbstract
{
    /** @var integer */
    protected $transferRequestId;

    /** @var  string */
    protected $lastNote;

    /**
     * @return int
     */
    public function getTransferRequestId()
    {
        return $this->transferRequestId;
    }

    /**
     * @return string
     */
    public function getLastNote()
    {
        return $this->lastNote;
    }
}