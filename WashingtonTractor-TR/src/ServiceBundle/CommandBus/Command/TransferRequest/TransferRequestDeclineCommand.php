<?php

namespace ServiceBundle\CommandBus\Command\TransferRequest;

use ServiceBundle\CommandBus\Command\BaseCommandAbstract;

class TransferRequestDeclineCommand extends BaseCommandAbstract
{
    /** @var integer */
    protected $id;
    /** @var  string */
    protected $lastNote;

    /**
     * @return string
     */
    public function getLastNote()
    {
        return $this->lastNote;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
}