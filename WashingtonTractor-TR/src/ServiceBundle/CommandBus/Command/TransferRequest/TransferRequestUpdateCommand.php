<?php

namespace ServiceBundle\CommandBus\Command\TransferRequest;

use ServiceBundle\CommandBus\Command\BaseCommandAbstract;

class TransferRequestUpdateCommand extends BaseCommandAbstract
{
    /** @var integer */
    protected $id;
    /** @var integer */
    protected $storeItemId;

    /** @var  string */
    protected $purpose;

    /** @var  integer */
    protected $shipmentTimestamp;

    /** @var  integer */
    protected $storeReceiverId;

    /** @var  string */
    protected $receiverUserName;

    /** @var  string */
    protected $lastNote;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getStoreItemId()
    {
        return $this->storeItemId;
    }

    /**
     * @return string
     */
    public function getPurpose()
    {
        return $this->purpose;
    }

    /**
     * @return int
     */
    public function getShipmentTimestamp()
    {
        return $this->shipmentTimestamp;
    }

    /**
     * @return int
     */
    public function getStoreReceiverId()
    {
        return $this->storeReceiverId;
    }

    /**
     * @return string
     */
    public function getReceiverUserName()
    {
        return $this->receiverUserName;
    }

    /**
     * @return string
     */
    public function getLastNote()
    {
        return $this->lastNote;
    }
}