<?php

namespace ServiceBundle\CommandBus\Command\TransferRequest;

use ServiceBundle\CommandBus\Command\BaseCommandAbstract;

class TransferRequestCreateCommand extends BaseCommandAbstract
{
    /** @var integer */
    protected $storeItemId;

    /** @var  string */
    protected $purpose;

    /** @var  integer */
    protected $shipmentTimestamp;

    /** @var  integer */
    protected $storeReceiverId;

    /** @var  string */
    protected $receiverUserName;

    /** @var  string */
    protected $lastNote;
    /** @var  bool */
    protected $getEmailUpdates = true;

    /**
     * @return int
     */
    public function getStoreItemId()
    {
        return $this->storeItemId;
    }

    /**
     * @return string
     */
    public function getPurpose()
    {
        return $this->purpose;
    }

    /**
     * @return int
     */
    public function getShipmentTimestamp()
    {
        return $this->shipmentTimestamp;
    }

    /**
     * @return int
     */
    public function getStoreReceiverId()
    {
        return $this->storeReceiverId;
    }

    /**
     * @return string
     */
    public function getReceiverUserName()
    {
        return $this->receiverUserName;
    }

    /**
     * @return string
     */
    public function getLastNote()
    {
        return $this->lastNote;
    }

    /**
     * @return bool
     */
    public function isGetEmailUpdates()
    {
        return $this->getEmailUpdates;
    }
}