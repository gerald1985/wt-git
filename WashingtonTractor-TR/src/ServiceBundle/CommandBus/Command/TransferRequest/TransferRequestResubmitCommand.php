<?php

namespace ServiceBundle\CommandBus\Command\TransferRequest;

use ServiceBundle\CommandBus\Command\BaseCommandAbstract;

class TransferRequestResubmitCommand extends BaseCommandAbstract
{
    /** @var integer */
    protected $id;
    /** @var integer */
    protected $storeItemId;
    /** @var  string */
    protected $lastNote;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getStoreItemId()
    {
        return $this->storeItemId;
    }

    /**
     * @return string
     */
    public function getLastNote()
    {
        return $this->lastNote;
    }
}