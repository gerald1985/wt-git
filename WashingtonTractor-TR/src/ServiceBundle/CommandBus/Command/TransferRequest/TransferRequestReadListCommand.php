<?php

namespace ServiceBundle\CommandBus\Command\TransferRequest;

use ServiceBundle\CommandBus\Command\BaseCommandAbstract;

class TransferRequestReadListCommand extends BaseCommandAbstract
{
    /** @var integer */
    protected $page = '1';
    /** @var integer */
    protected $perPage = '10';
    /** @var integer */
    protected $itemStockNumber;
    /** @var array */
    protected $itemModelNames;
    /** @var array */
    protected $purposes;
    /** @var array */
    protected $statuses;
    /** @var array */
    protected $itemStoreIds;
    /** @var array */
    protected $receivingStoreIds;
    /** @var array */
    protected $requestedByIds;
    /** @var integer */
    protected $requestTimestampFrom;
    /** @var integer */
    protected $requestTimestampTo;

    /**
     * @return int
     */
    public function getRequestTimestampFrom()
    {
        return $this->requestTimestampFrom;
    }

    /**
     * @return int
     */
    public function getRequestTimestampTo()
    {
        return $this->requestTimestampTo;
    }

    /**
     * @return array
     */
    public function getRequestedByIds()
    {
        return $this->requestedByIds;
    }

    /**
     * @return array
     */
    public function getReceivingStoreIds()
    {
        return $this->receivingStoreIds;
    }


    /**
     * @return array
     */
    public function getItemStoreIds()
    {
        return $this->itemStoreIds;
    }

    /**
     * @return array
     */
    public function getStatuses()
    {
        return $this->statuses;
    }

    /**
     * @return array
     */
    public function getItemModelNames()
    {
        return $this->itemModelNames;
    }

    /**
     * @return array
     */
    public function getPurposes()
    {
        return $this->purposes;
    }

    /**
     * @return int
     */
    public function getItemStockNumber()
    {
        return $this->itemStockNumber;
    }

    /**
     * @return int
     */
    public function getPage()
    {
        return $this->page;
    }

    /**
     * @return int
     */
    public function getPerPage()
    {
        return $this->perPage;
    }
}