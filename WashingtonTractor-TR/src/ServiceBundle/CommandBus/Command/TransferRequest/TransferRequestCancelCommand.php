<?php

namespace ServiceBundle\CommandBus\Command\TransferRequest;

use ServiceBundle\CommandBus\Command\BaseCommandAbstract;

class TransferRequestCancelCommand extends BaseCommandAbstract
{
    /** @var integer */
    protected $id;
    /** @var string */
    protected $note;

    /**
     * @return string
     */
    public function getNote()
    {
        return $this->note;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
}