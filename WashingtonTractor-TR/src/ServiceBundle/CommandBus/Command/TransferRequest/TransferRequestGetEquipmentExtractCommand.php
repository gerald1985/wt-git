<?php

namespace ServiceBundle\CommandBus\Command\TransferRequest;

use ServiceBundle\CommandBus\Command\BaseCommandAbstract;

class TransferRequestGetEquipmentExtractCommand extends BaseCommandAbstract
{
    /** @var string */
    protected $shipmentTimestampFrom;

    /** @var string */
    protected $shipmentTimestampTo;

    /**
     * @return string
     */
    public function getShipmentTimestampFrom()
    {
        return $this->shipmentTimestampFrom;
    }

    /**
     * @return string
     */
    public function getShipmentTimestampTo()
    {
        return $this->shipmentTimestampTo;
    }
}
