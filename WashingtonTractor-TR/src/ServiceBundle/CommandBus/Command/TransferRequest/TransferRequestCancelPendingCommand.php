<?php

namespace ServiceBundle\CommandBus\Command\TransferRequest;

use ServiceBundle\CommandBus\Command\BaseCommandAbstract;

class TransferRequestCancelPendingCommand extends BaseCommandAbstract
{
    /** @var integer */
    protected $timestampFrom;

    /**
     * @return integer
     */
    public function getTimestampFrom()
    {
        return $this->timestampFrom;
    }
}
