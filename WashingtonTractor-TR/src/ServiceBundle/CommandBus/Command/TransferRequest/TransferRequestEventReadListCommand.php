<?php

namespace ServiceBundle\CommandBus\Command\TransferRequest;

use ServiceBundle\CommandBus\Command\BaseCommandAbstract;

class TransferRequestEventReadListCommand extends BaseCommandAbstract
{
    /** @var integer */
    protected $transferRequestId;

    /**
     * @return int
     */
    public function getTransferRequestId()
    {
        return $this->transferRequestId;
    }
}