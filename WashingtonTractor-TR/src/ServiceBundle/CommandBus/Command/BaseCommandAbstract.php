<?php

namespace ServiceBundle\CommandBus\Command;

abstract class BaseCommandAbstract
{
    /**
     * AutoFillDataCommandTrait constructor.
     * @param array $values
     */
    public function __construct(array $values = [])
    {
        foreach ($values as $property => $value) {
            if (property_exists($this, $property)) {
                $this->$property = $value;
            }
        }
    }
}