<?php

namespace ServiceBundle\CommandBus\Command\Store;

use ServiceBundle\CommandBus\Command\BaseCommandAbstract;

class StoreItemReadListCommand extends BaseCommandAbstract
{
    /** @var string */
    protected $modelName;
    /** @var string */
    protected $stockNumber;
    /** @var array */
    protected $excludeInTRWithStatuses;
    /** @var array */
    protected $excludeStores;
    /** @var integer */
    protected $page = '1';
    /** @var integer */
    protected $perPage = '10';

    /**
     * @return array
     */
    public function getExcludeStores()
    {
        return $this->excludeStores;
    }

    /**
     * @return array
     */
    public function getExcludeInTRWithStatuses()
    {
        return $this->excludeInTRWithStatuses;
    }

    /**
     * @return string
     */
    public function getModelName()
    {
        return $this->modelName;
    }

    /**
     * @return string
     */
    public function getStockNumber()
    {
        return $this->stockNumber;
    }


    /**
     * @return int
     */
    public function getPage()
    {
        return $this->page;
    }

    /**
     * @return int
     */
    public function getPerPage()
    {
        return $this->perPage;
    }
}