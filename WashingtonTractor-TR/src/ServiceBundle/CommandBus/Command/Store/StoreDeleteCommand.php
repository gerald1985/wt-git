<?php

namespace ServiceBundle\CommandBus\Command\Store;

use ServiceBundle\CommandBus\Command\BaseCommandAbstract;

class StoreDeleteCommand extends BaseCommandAbstract
{
    /** @var  integer */
    protected $id;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
}