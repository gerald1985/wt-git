<?php

namespace ServiceBundle\CommandBus\Command\Store;

use ServiceBundle\CommandBus\Command\BaseCommandAbstract;

class StoreCreateCommand extends BaseCommandAbstract
{
    /** @var string */
    protected $name;

    /** @var  integer */
    protected $number;

    /** @var  string */
    protected $address;

    /** @var  integer */
    protected $managerId;

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return int
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @return int
     */
    public function getManagerId()
    {
        return $this->managerId;
    }
}