<?php

namespace ServiceBundle\Security;

use DomainBundle\Entity\Store\Store;
use DomainBundle\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class StoreVoter extends Voter
{
    const CREATE_STORE                       = 'createStore';


    /**
     * @var array
     */
    public $accessByRole = [
        User::ROLE_MANAGER_STORE  => [
            'owner' => [
            ],
            'all'   => [
            ]
        ],
        User::ROLE_ADMIN    => [
            'owner' => [
            ],
            'all'   => [
                //status => access to fields
                Store::STATUS_ACTIVE   => [
                    self::CREATE_STORE
                ],
                Store::STATUS_INACTIVE => [
                    self::CREATE_STORE
                ]
            ]
        ],
    ];


    protected function supports($access, $entity)
    {
        $list = [
            self::CREATE_STORE
        ];
        if (!in_array($access, $list)) {
            return false;
        }

        if (!empty($entity) && !($entity instanceof Store)) {
            return false;
        }

        return true;
    }

    protected function voteOnAttribute($access, $entity, TokenInterface $token)
    {
        $user = $token->getUser();
        if (!$user instanceof User) {
            // the user must be logged in; if not, deny access
            return false;
        }


        /** @var Store|null $entity */

        return $this->canDo($access, $token, $entity);
    }

    private function canDo($field, TokenInterface $token, Store $entity)
    {
        /** @var User $user */
        $user = $token->getUser();
        if ($user->hasRole(User::ROLE_ADMIN)) {
            $role = 'ROLE_ADMIN';
            $owner = '';
        } elseif ($user->hasRole(User::ROLE_MANAGER_STORE)) {
            $role = 'ROLE_MANAGER';
            $owner = $entity->getManager();
        } else {
            return false;
        }


        /**
         * get status
         *
         * @var Store $currentStatus
         */
        $currentStatus = $entity->getStatus();
        //checks owner if for this field update necessary owner
        if (
            !empty($this->accessByRole[$role]['owner'][$currentStatus])
            && in_array($field, $this->accessByRole[$role]['owner'][$currentStatus])
            && $owner != $user
        ) {
            return false;
        }

        //checks access for status
        $accessList = array_merge_recursive($this->accessByRole[$role]['owner'], $this->accessByRole[$role]['all']);
        $isHaveAccess = !empty($accessList[$currentStatus]) && in_array($field, $accessList[$currentStatus]);

        return $isHaveAccess;
    }
}

