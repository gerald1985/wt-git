<?php

namespace ServiceBundle\EventListener\TransferRequest;

use Doctrine\ORM\QueryBuilder;
use DomainBundle\Entity\TransferRequest\TransferRequest;
use DomainBundle\Entity\TransferRequest\TransferRequestEvent;
use DomainBundle\Entity\User;
use DomainBundle\Model\RepositoryInterface;
use ServiceBundle\Event\Email\SendEmailEvent;
use ServiceBundle\Event\TransferRequest\TransferRequestEventEvent;
use ServiceBundle\Model\Repository\UserRepository;
use ServiceBundle\Services\EmailSendService;
use ServiceBundle\Services\TransferRequestEventService;

class TransferRequestListener
{
    /** @var TransferRequestEventService */
    protected $transferRequestEventService;
    /** @var EmailSendService */
    protected $emailNotificationService;

    /**
     * @var UserRepository
     */
    protected $userRepository;

    /**
     * TransferRequestListener constructor.
     * @param TransferRequestEventService $transferRequestEventService
     * @param EmailSendService $emailNotificationService
     * @param RepositoryInterface $userRepository
     */
    public function __construct(
        TransferRequestEventService $transferRequestEventService,
        EmailSendService $emailNotificationService,
        RepositoryInterface $userRepository
    )
    {
        $this->transferRequestEventService = $transferRequestEventService;
        $this->emailNotificationService = $emailNotificationService;
        $this->userRepository = $userRepository;
    }

    /**
     * @param TransferRequestEventEvent $event
     */
    public function onNote(TransferRequestEventEvent $event)
    {
        $this->saveEvent(TransferRequestEvent::EVENT_NOTE, $event);
        $this->onNoteNotifications($event);
    }

    /**
     * @param TransferRequestEventEvent $event
     */
    public function onNoteNotifications(TransferRequestEventEvent $event)
    {
        $this->sendEmail('tr_note', $event, [
            'tr'   => $event->getTransferRequest(),
            'note' => $event->getNote(),
            'dateTime' => new \DateTime(),
        ]);
    }

    /**
     * @param TransferRequestEventEvent $event
     */
    public function onPlacement(TransferRequestEventEvent $event)
    {
        $this->saveEvent(TransferRequestEvent::EVENT_PLACEMENT, $event);
        $this->sendEmail('tr_created', $event, [
            'tr'   => $event->getTransferRequest(),
            'note' => $event->getNote(),
            'dateTime' => new \DateTime(),
        ]);
    }

    /**
     * @param TransferRequestEventEvent $event
     */
    public function onUpdate(TransferRequestEventEvent $event)
    {
        $this->saveEvent(TransferRequestEvent::EVENT_UPDATED, $event);
        $this->sendEmail('tr_updated', $event, [
            'tr'   => $event->getTransferRequest(),
            'old'  => $event->getTransferRequestOld(),
            'note' => $event->getNote(),
            'dateTime' => new \DateTime(),
        ]);
    }

    /**
     * @param TransferRequestEventEvent $event
     */
    public function onStatusChanged(TransferRequestEventEvent $event)
    {
        $this->saveEvent(TransferRequestEvent::EVENT_STATUS_CHANGED, $event);
        $this->sendEmail('tr_status_changed', $event, [
            'tr'   => $event->getTransferRequest(),
            'old'  => $event->getTransferRequestOld(),
            'note' => $event->getNote(),
            'dateTime' => new \DateTime(),
        ]);
    }

    /**
     * @param TransferRequestEventEvent $event
     */
    public function onCanceledPending(TransferRequestEventEvent $event)
    {
        $this->saveEvent(TransferRequestEvent::EVENT_EXPIRED, $event);
        $this->sendEmail('tr_cancel_pending', $event, [
            'tr' => $event->getTransferRequest(),
        ]);
    }

    /**
     * @param TransferRequestEventEvent $event
     */
    public function onResubmit(TransferRequestEventEvent $event)
    {
        $this->saveEvent(TransferRequestEvent::EVENT_RESUBMITTED, $event);
        $this->sendEmail('tr_resubmitted', $event, [
            'tr'   => $event->getTransferRequest(),
            'old'  => $event->getTransferRequestOld(),
            'note' => $event->getNote(),
            'dateTime' => new \DateTime(),
        ]);
    }

    /**
     * @param string $type
     * @param TransferRequestEventEvent $event
     */
    protected function saveEvent($type, TransferRequestEventEvent $event)
    {
        $event->setEventType($type);
        $this->transferRequestEventService->add($event);
    }


    /**
     * @param $type
     * @param TransferRequestEventEvent $event
     * @param array $templateData
     */
    protected function sendEmail($type, TransferRequestEventEvent $event, array $templateData)
    {
        $eventEmail = new SendEmailEvent();
        $eventEmail->setType($type);
        $eventEmail->setTemplateData($templateData);

        $this->addParticipantsToEmail($event->getTransferRequest(), $eventEmail);
        $this->emailNotificationService->send($eventEmail);
    }

    /**
     * @param TransferRequest $entity
     * @param SendEmailEvent $event
     */
    protected function addParticipantsToEmail(TransferRequest $entity, SendEmailEvent $event)
    {
        //Requester
        if ($entity->getGetEmailUpdates()) {
            $event->addTo($entity->getRequestedBy()->getEmail());
        }
        //Hosting SM
        if ($entity->getStoreItem()->getStore()->getManager() instanceof User) {
            $event->addTo($entity->getStoreItem()->getStore()->getManager()->getEmail());
        }
        //Receiving SM
        if ($entity->getStoreReceiver()->getManager() instanceof User) {
            $event->addTo($entity->getStoreReceiver()->getManager()->getEmail());
        }
        $this->addTransfersCoordinatorsToReceiver($event, $entity);
    }

    /**
     * @param SendEmailEvent $event
     */
    protected function addTransfersCoordinatorsToReceiver(SendEmailEvent $event, TransferRequest $entity)
    {
        /** @var QueryBuilder $q */
        $q = $this->userRepository->softCreateQueryBuilder('o');
        $q->andWhere($q->expr()->like('o.roles', ':admin'));
        $q->setParameter(':admin', "%" . User::ROLE_TRANSFER_COORDINATOR . "%");

        /** @var User $user */
        foreach ($q->getQuery()->getResult() as $user) {
            if ($entity->getRequestedBy()->getId() !== $user->getId()) {
                /** @var User $user */
                $event->addTo($user->getEmail());
            }
        }
    }
}
