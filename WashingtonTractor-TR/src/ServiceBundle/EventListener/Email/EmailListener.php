<?php

namespace ServiceBundle\EventListener\Email;

use DomainBundle\Entity\User;
use DomainBundle\Model\RepositoryInterface;
use ServiceBundle\Event\Email\SendEmailEvent;
use ServiceBundle\Model\Repository\UserRepository;
use ServiceBundle\Services\EmailSendService;

class EmailListener
{
    /** @var EmailSendService */
    protected $emailSendService;

    /**
     * @var UserRepository
     */
    protected $userRepository;

    /**
     * EmailListener constructor.
     * @param EmailSendService $emailSendService
     * @param RepositoryInterface $userRepository
     */
    public function __construct(
        EmailSendService $emailSendService,
        RepositoryInterface $userRepository
    )
    {
        $this->emailSendService = $emailSendService;
        $this->userRepository = $userRepository;
    }

    /**
     * @param SendEmailEvent $event
     */
    public function onUserRegistered(SendEmailEvent $event)
    {
        $entity = $event->getTemplateData('user');
        if ($entity->hasRole(User::ROLE_MANAGER_SALE)) {
            $userLevel = '1 - Sales Manager';
        } elseif ($entity->hasRole(User::ROLE_MANAGER_STORE)) {
            $userLevel = '2 - Store Manager';
        } elseif ($entity->hasRole(User::ROLE_DRIVER)) {
            $userLevel = '3 - Driver';
        } elseif ($entity->hasRole(User::ROLE_TRANSFER_COORDINATOR)) {
            $userLevel = '4 - Transfer Coordinator';
        } elseif ($entity->hasRole(User::ROLE_ADMIN)) {
            $userLevel = '5 - Admin';
        }
        $event->addTemplateData('userLevel', $userLevel);
        $event->addTemplateData('userLocation', $entity->getStores()->first());
        $event->setType('user_registered');

        $this->emailSendService->addAdminsToReceiver($event);

        $this->emailSendService->send($event);
    }

    /**
     * @param SendEmailEvent $event
     */
    public function onUserDeclined(SendEmailEvent $event)
    {
        //emails for admins
        $eventAdmin = clone $event;
        $eventAdmin->setType('user_declined.admin');
        $this->emailSendService->addAdminsToReceiver($eventAdmin, $eventAdmin->getTemplateData('admin'));
        $this->emailSendService->send($eventAdmin);

        //email for user
        $eventUser = clone $event;
        $eventUser->setType('user_declined.user');
        $eventUser->addTo($eventUser->getTemplateData('user')->getEmail());
        $this->emailSendService->send($eventUser);
    }

    /**
     * @param SendEmailEvent $event
     */
    public function onUserApproved(SendEmailEvent $event)
    {
        //emails for admins
        $eventAdmin = clone $event;
        $eventAdmin->setType('user_approved.admin');
        $this->emailSendService->addAdminsToReceiver($eventAdmin, $eventAdmin->getTemplateData('admin'));
        $this->emailSendService->send($eventAdmin);

        //email for user
        $eventUser = clone $event;
        $eventUser->setType('user_approved.user');
        $eventUser->addTo($eventUser->getTemplateData('user')->getEmail());
        $this->emailSendService->send($eventUser);
    }

    /**
     * @param SendEmailEvent $event
     */
    public function onUserResetPassword(SendEmailEvent $event)
    {
        //email for user
        $eventUser = clone $event;
        $eventUser->setType('user_reset_password');
        $eventUser->addTo($eventUser->getTemplateData('user')->getEmail());
        $this->emailSendService->send($eventUser);
    }
}
