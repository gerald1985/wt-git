<?php

namespace ServiceBundle\Command\TransferRequest;

use ServiceBundle\Command\BaseCommand;
use ServiceBundle\CommandBus\Command\TransferRequest\TransferRequestCancelPendingCommand;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use DateTime;
use DateInterval;

class CancelPendingTransferRequestCommand extends BaseCommand
{
    protected function configure()
    {
        $this
            ->setName('service:cancel:pending:transfer-request')
            ->setDescription('Cancels transfer requests with PENDING status starting from timestamp-from (default 96 hours)')
            ->addOption(
                'timestamp-from',
                null,
                InputOption::VALUE_OPTIONAL,
                'Sets starting shipment timestamp of Transfer Requests for extract',
                (new DateTime())->sub(new DateInterval('PT96H'))->getTimestamp()
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->setSystemUser();
        $command = new TransferRequestCancelPendingCommand(
            [
                'timestampFrom' => $input->getOption('timestamp-from'),
            ]
        );

        $this->getContainer()->get('tactician.commandbus')->handle($command);
    }
}