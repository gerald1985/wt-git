<?php

namespace ServiceBundle\Command\TransferRequest;

use ServiceBundle\Command\BaseCommand;
use ServiceBundle\CommandBus\Command\TransferRequest\TransferRequestGetEquipmentExtractCommand;
use ServiceBundle\Event\Email\SendEmailEvent;
use ServiceBundle\Services\EmailSendService;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use DateTime;
use Swift_Attachment;

class SendDailyTransferRequestExtractCommand extends BaseCommand
{
    protected function configure()
    {
        $this
            ->setName('service:mail:send:daily-transfer-request')
            ->setDescription('Sends csv with transfer requests for today shipment date or between provided shipment dates (if defined)')
            ->addOption(
                'shipment-timestamp-from',
                null,
                InputOption::VALUE_OPTIONAL,
                'Sets starting shipment timestamp of Transfer Requests for extract',
                (new DateTime('today'))->getTimestamp()
            )
            ->addOption(
                'shipment-timestamp-to',
                null,
                InputOption::VALUE_OPTIONAL,
                'Sets starting shipment timestamp of Transfer Requests for extract',
                (new DateTime('now'))->getTimestamp()
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->setSystemUser();
        $command = new TransferRequestGetEquipmentExtractCommand(
            [
                'shipmentTimestampFrom' => $input->getOption('shipment-timestamp-from'),
                'shipmentTimestampTo'   => $input->getOption('shipment-timestamp-to'),
            ]
        );

        $attachment = Swift_Attachment::newInstance()
            ->setFilename('extract.csv')
            ->setContentType('text/csv')
            ->setBody(
                (string) $this->getContainer()->get('tactician.commandbus')->handle($command)
            );

        $email = new SendEmailEvent();
        $email->setType('tr_extract_equipment');
        $email->addAttachment($attachment);
        $email->setTemplateData([
            'date' => (new DateTime())->format('m/d/Y'),
            'time' => (new DateTime())->format('H:i')
        ]);

        /** @var EmailSendService $sendEmailService */
        $sendEmailService = $this->getContainer()->get('service_bundle.email_send');
        $sendEmailService->addAdminsToReceiver($email);
        $sendEmailService->send($email);
    }
}