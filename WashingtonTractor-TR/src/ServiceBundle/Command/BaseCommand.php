<?php

namespace ServiceBundle\Command;

use DomainBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Security\Core\Authentication\Token\AnonymousToken;

abstract class BaseCommand extends ContainerAwareCommand
{
    protected function setSystemUser()
    {
        $token = new AnonymousToken('System', 'System', [User::ROLE_ADMIN]);
        $token->setUser(new User());
        $this->getContainer()->get('security.token_storage')->setToken($token);
    }
}
