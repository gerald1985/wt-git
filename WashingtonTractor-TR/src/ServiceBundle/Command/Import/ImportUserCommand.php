<?php

namespace ServiceBundle\Command\Import;

use ServiceBundle\CommandBus\Command\Import\ImportUsersCommand;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * @author Matthieu Bontemps <matthieu@knplabs.com>
 * @author Thibault Duplessis <thibault.duplessis@gmail.com>
 * @author Luis Cordova <cordoval@gmail.com>
 */
class ImportUserCommand extends ContainerAwareCommand
{

    protected function configure()
    {
        $this
            ->setName('service:import:users')
            ->setDescription('Import users to app.');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $command = new ImportUsersCommand(
            [
            ]
        );

        $list = $this->getContainer()->get('tactician.commandbus')->handle($command);

        $output->writeln(sprintf('Imported <comment>%s</comment> users', count($list)));
    }
}
