<?php

namespace ServiceBundle\Exception\Validator;

use ServiceBundle\Exception\ExceptionAbstract;
use Exception;

class UniqueException extends ExceptionAbstract
{
    protected $statusCode = 400;
    protected $code = 7100;
    protected $message = [];
    protected $errors = [];

    /**
     * UniqueException constructor.
     * @param array $errors
     * @param int $code
     * @param Exception|null $previous
     */
    public function __construct(array $errors, $code = 0, Exception $previous = null)
    {
        parent::__construct('Unique exceptions', $code, $previous);

        $this->errors = $errors;
    }

    public function getErrors()
    {
        return $this->errors;
    }
}
