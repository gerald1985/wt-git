<?php

namespace ServiceBundle\Exception\Voter;

use ServiceBundle\Exception\ExceptionInterface;
use Exception;

class VoterException extends Exception implements ExceptionInterface
{
    protected $statusCode = 403;

    public function __construct($message, Exception $previous = null)
    {
        $this->message = 'voter.' . $message;
        parent::__construct($this->message, 5100, $previous);
    }

    /**
     * Returns the status code.
     *
     * @return int An HTTP response status code
     */
    public function getStatusCode()
    {
        return $this->statusCode;
    }
}
