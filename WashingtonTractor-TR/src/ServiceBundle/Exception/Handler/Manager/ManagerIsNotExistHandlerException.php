<?php


namespace ServiceBundle\Exception\Handler\Manager;

use ServiceBundle\Exception\Handler\HandlerException;

class ManagerIsNotExistHandlerException extends HandlerException
{
    protected $statusCode = 400;
    protected $code = 7003;
    protected $message = 'Manager is not exist';
}
