<?php


namespace ServiceBundle\Exception\Handler\Manager;

use ServiceBundle\Exception\Handler\HandlerException;

class ManagerHasPendingTransferRequestsHandlerException extends HandlerException
{
    protected $statusCode = 400;
    protected $code = 7005;
    protected $message = 'The user to be disabled is a Hosting Store Manager and has "pending" TRs';
}
