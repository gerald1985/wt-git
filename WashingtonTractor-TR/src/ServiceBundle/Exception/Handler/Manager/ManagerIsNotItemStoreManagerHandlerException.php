<?php


namespace ServiceBundle\Exception\Handler\Manager;

use ServiceBundle\Exception\Handler\HandlerException;

class ManagerIsNotItemStoreManagerHandlerException extends HandlerException
{
    protected $statusCode = 400;
    protected $code = 7004;
    protected $message = 'Manager is not item location manager';
}
