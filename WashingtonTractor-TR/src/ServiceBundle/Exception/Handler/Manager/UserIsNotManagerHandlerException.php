<?php


namespace ServiceBundle\Exception\Handler\Manager;

use ServiceBundle\Exception\Handler\HandlerException;

class UserIsNotManagerHandlerException extends HandlerException
{
    protected $statusCode = 400;
    protected $code = 7001;
    protected $message = 'User is not manager';
}
