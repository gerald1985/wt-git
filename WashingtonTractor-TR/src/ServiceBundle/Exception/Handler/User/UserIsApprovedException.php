<?php


namespace ServiceBundle\Exception\Handler\User;

use ServiceBundle\Exception\Handler\HandlerException;

class UserIsApprovedException extends HandlerException
{
    protected $statusCode = 400;
    protected $code = 7010;
    protected $message = 'User was approved before';
}
