<?php


namespace ServiceBundle\Exception\Handler\User;

use ServiceBundle\Exception\Handler\HandlerException;

class UserPasswordIsTheSameHandlerException extends HandlerException
{
    protected $statusCode = 400;
    protected $code = 7008;
    protected $message = 'Your new password must differ from your current password';
}
