<?php


namespace ServiceBundle\Exception\Handler\User;

use ServiceBundle\Exception\Handler\HandlerException;

class UserResetTokenNotFoundHandlerException extends HandlerException
{
    protected $statusCode = 400;
    protected $code = 7006;
    protected $message = 'User token is not found';
}
