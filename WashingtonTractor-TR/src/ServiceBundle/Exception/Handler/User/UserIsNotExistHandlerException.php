<?php


namespace ServiceBundle\Exception\Handler\User;

use ServiceBundle\Exception\Handler\HandlerException;

class UserIsNotExistHandlerException extends HandlerException
{
    protected $statusCode = 400;
    protected $code = 7002;
    protected $message = 'User is not exist';
}
