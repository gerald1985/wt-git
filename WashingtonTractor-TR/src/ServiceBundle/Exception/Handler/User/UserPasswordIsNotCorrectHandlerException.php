<?php


namespace ServiceBundle\Exception\Handler\User;

use ServiceBundle\Exception\Handler\HandlerException;

class UserPasswordIsNotCorrectHandlerException extends HandlerException
{
    protected $statusCode = 400;
    protected $code = 7007;
    protected $message = 'Not your current password';
}
