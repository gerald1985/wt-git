<?php


namespace ServiceBundle\Exception\Handler\User;

use ServiceBundle\Exception\Handler\HandlerException;

class UserIsDeclinedException extends HandlerException
{
    protected $statusCode = 400;
    protected $code = 7009;
    protected $message = 'User was declined before';
}
