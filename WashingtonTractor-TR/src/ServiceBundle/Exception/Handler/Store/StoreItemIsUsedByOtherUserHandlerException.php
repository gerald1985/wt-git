<?php


namespace ServiceBundle\Exception\Handler\Store;

use ServiceBundle\Exception\Handler\HandlerException;

class StoreItemIsUsedByOtherUserHandlerException extends HandlerException
{
    protected $statusCode = 400;
    protected $code = 7201;
    protected $message = 'The chosen Item was requested by another User';
}
