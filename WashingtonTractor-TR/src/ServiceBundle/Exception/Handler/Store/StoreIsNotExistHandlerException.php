<?php


namespace ServiceBundle\Exception\Handler\Store;

use ServiceBundle\Exception\Handler\HandlerException;

class StoreIsNotExistHandlerException extends HandlerException
{
    protected $statusCode = 400;
    protected $code = 7200;
    protected $message = 'Store is not exist';
}
