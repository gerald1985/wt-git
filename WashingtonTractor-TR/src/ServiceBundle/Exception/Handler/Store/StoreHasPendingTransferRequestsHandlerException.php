<?php


namespace ServiceBundle\Exception\Handler\Store;

use ServiceBundle\Exception\Handler\HandlerException;

class StoreHasPendingTransferRequestsHandlerException extends HandlerException
{
    protected $statusCode = 400;
    protected $code = 7202;
    protected $message = 'This Location cannot be deleted as it has transfer requests from or to it.';
}
