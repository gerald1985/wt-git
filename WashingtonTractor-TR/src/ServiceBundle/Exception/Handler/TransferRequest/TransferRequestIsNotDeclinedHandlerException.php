<?php


namespace ServiceBundle\Exception\Handler\TransferRequest;

use ServiceBundle\Exception\Handler\HandlerException;

class TransferRequestIsNotDeclinedHandlerException extends HandlerException
{
    protected $statusCode = 400;
    protected $code = 7303;
    protected $message = 'Transfer request has not declined status';
}
