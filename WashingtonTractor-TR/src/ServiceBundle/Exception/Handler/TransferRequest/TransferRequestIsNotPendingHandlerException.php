<?php


namespace ServiceBundle\Exception\Handler\TransferRequest;

use ServiceBundle\Exception\Handler\HandlerException;

class TransferRequestIsNotPendingHandlerException extends HandlerException
{
    protected $statusCode = 400;
    protected $code = 7302;
    protected $message = 'Transfer request has not pending status';
}
