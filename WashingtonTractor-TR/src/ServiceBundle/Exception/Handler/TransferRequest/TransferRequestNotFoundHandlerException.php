<?php


namespace ServiceBundle\Exception\Handler\TransferRequest;

use ServiceBundle\Exception\Handler\HandlerException;

class TransferRequestNotFoundHandlerException extends HandlerException
{
    protected $statusCode = 404;
    protected $code = 7300;
    protected $message = 'Transfer request is not found';
}
