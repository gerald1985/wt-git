<?php


namespace ServiceBundle\Exception\Handler\TransferRequest;

use ServiceBundle\Exception\Handler\HandlerException;

class TransferRequestIsCanceledHandlerException extends HandlerException
{
    protected $statusCode = 400;
    protected $code = 7301;
    protected $message = 'Transfer request is canceled';
}
