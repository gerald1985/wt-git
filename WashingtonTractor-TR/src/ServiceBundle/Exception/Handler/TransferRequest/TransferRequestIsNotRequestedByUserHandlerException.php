<?php


namespace ServiceBundle\Exception\Handler\TransferRequest;

use ServiceBundle\Exception\Handler\HandlerException;

class TransferRequestIsNotRequestedByUserHandlerException extends HandlerException
{
    protected $statusCode = 400;
    protected $code = 7304;
    protected $message = 'Transfer request were requested not by you';
}
