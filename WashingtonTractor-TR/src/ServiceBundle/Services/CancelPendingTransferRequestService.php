<?php

namespace ServiceBundle\Services;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\ORMInvalidArgumentException;
use DomainBundle\Entity\TransferRequest\TransferRequest;
use ServiceBundle\Event\TransferRequest\TransferRequestEventEvent;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class CancelPendingTransferRequestService
{
    /** @var EntityManager */
    protected $em;
    /** @var EventDispatcherInterface */
    protected $eventDispatcher;

    /**
     * @param EntityManager $entityManager
     * @param EventDispatcherInterface $dispatcher
     */
    public function __construct(
        EntityManager $entityManager,
        EventDispatcherInterface $dispatcher
    )
    {
        $this->em = $entityManager;
        $this->eventDispatcher = $dispatcher;
    }

    /**
     * @param TransferRequest $entity
     *
     * @throws ORMInvalidArgumentException
     */
    public function cancelRequest(TransferRequest $entity)
    {
        $entityOld = clone $entity;

        $entity->setStatus(TransferRequest::STATUS_CANCELLED);

        $this->em->persist($entity);

        if ($entityOld->getStatus() !== $entity->getStatus()) {
            $event = new TransferRequestEventEvent($entity, $entityOld);
            $this->eventDispatcher->dispatch('tr_event.status_changed', $event);
        }
    }
}
