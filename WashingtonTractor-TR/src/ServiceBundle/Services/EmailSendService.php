<?php

namespace ServiceBundle\Services;

use Doctrine\ORM\QueryBuilder;
use DomainBundle\Entity\User;
use ServiceBundle\Event\Email\SendEmailEvent;
use Symfony\Component\Templating\EngineInterface;
use ServiceBundle\Model\Repository\UserRepository;
use Swift_Mime_Attachment;
use Swift_Message;
use UnexpectedValueException;

class EmailSendService
{
    /** @var array */
    protected $messageTemplates = [
        'tr_created' => [
            'subject' => 'ServiceBundle:Emails:subject.tr_created.html.twig',
            'body'    => 'ServiceBundle:Emails:body.tr_created.html.twig',
        ],
        'tr_updated' => [
            'subject' => 'ServiceBundle:Emails:subject.tr_updated.html.twig',
            'body'    => 'ServiceBundle:Emails:body.tr_updated.html.twig',
        ],
        'tr_status_changed' => [
            'subject' => 'ServiceBundle:Emails:subject.tr_updated.html.twig',
            'body'    => 'ServiceBundle:Emails:body.tr_status_changed.html.twig',
        ],
        'tr_note' => [
            'subject' => 'ServiceBundle:Emails:subject.tr_updated.html.twig',
            'body'    => 'ServiceBundle:Emails:body.tr_note.html.twig',
        ],
        'tr_resubmitted' => [
            'subject' => 'ServiceBundle:Emails:subject.tr_updated.html.twig',
            'body'    => 'ServiceBundle:Emails:body.tr_resubmitted.html.twig',
        ],
        'tr_daily_extract' => [
            'subject' => 'ServiceBundle:Emails:subject.tr_daily_extract.html.twig',
            'body'    => 'ServiceBundle:Emails:body.tr_daily_extract.html.twig',
        ],
        'tr_extract_equipment' => [
            'subject' => 'ServiceBundle:Emails:subject.tr_extract.html.twig',
            'body' => 'ServiceBundle:Emails:body.tr_extract.html.twig',
        ],
        'tr_cancel_pending' => [
            'subject' => 'ServiceBundle:Emails:subject.tr_expired.html.twig',
            'body' => 'ServiceBundle:Emails:body.tr_expired.html.twig',
        ],
        'user_registered' => [
            'subject' => 'ServiceBundle:Emails:subject.user_registered.html.twig',
            'body'    => 'ServiceBundle:Emails:body.user_registered.html.twig',
        ],
        'user_declined.admin' => [
            'subject' => 'ServiceBundle:Emails:subject.user_declined.html.twig',
            'body'    => 'ServiceBundle:Emails:body.user_declined.admin.html.twig',
        ],
        'user_declined.user' => [
            'subject' => 'ServiceBundle:Emails:subject.user_declined.html.twig',
            'body'    => 'ServiceBundle:Emails:body.user_declined.user.html.twig',
        ],
        'user_approved.admin' => [
            'subject' => 'ServiceBundle:Emails:subject.user_approved.html.twig',
            'body'    => 'ServiceBundle:Emails:body.user_approved.admin.html.twig',
        ],
        'user_approved.user' => [
            'subject' => 'ServiceBundle:Emails:subject.user_approved.html.twig',
            'body'    => 'ServiceBundle:Emails:body.user_approved.user.html.twig',
        ],
        'user_reset_password' => [
            'subject' => 'ServiceBundle:Emails:subject.user_reset_password.html.twig',
            'body'    => 'ServiceBundle:Emails:body.user_reset_password.html.twig'
        ],
    ];

    /** @var EngineInterface  */
    protected $templater;
    /** @var \Swift_Mailer */
    protected $mailer;
    /** @var string */
    protected $from;
    /** @var UserRepository */
    protected $userRepository;

    public function __construct(
        $from,
        EngineInterface $templater,
        \Swift_Mailer $mailer,
        UserRepository $userRepository
    )
    {
        $this->from = $from;
        $this->templater = $templater;
        $this->mailer = $mailer;
        $this->userRepository = $userRepository;
    }

    /**
     * @param SendEmailEvent $event
     */
    public function send(SendEmailEvent $event)
    {
        $subject = $this->generateEmailSubject($event);
        $body = $this->generateEmailBody($event);
        foreach ($event->getTo() as $email) {
            $message = Swift_Message::newInstance()
                ->setSubject($subject)
                ->setFrom($this->from)
                ->setTo($email)
                ->setBody($body, 'text/html');

            /** @var Swift_Mime_Attachment $attachment */
            foreach ($event->getAttachments() as $attachment) {
                $message->attach($attachment);
            }

            $this->mailer->send($message);
        }
    }

    /**
     * @param SendEmailEvent $event
     * @param null|User $except
     */
    public function addAdminsToReceiver(SendEmailEvent $event, $except = null)
    {
        /** @var QueryBuilder $q */
        $q = $this->userRepository->softCreateQueryBuilder('o');
        $q->andWhere($q->expr()->like('o.roles', ':admin'));
        $q->setParameter(':admin', "%" . User::ROLE_ADMIN . "%");

        /** @var User $user */
        foreach ($q->getQuery()->getResult() as $user) {
            if ($except && $except->getId() === $user->getId()) {
                continue;
            }
            $event->addTo($user->getEmail());
        }
    }

    /**
     * @param SendEmailEvent $event
     * @return string
     * @throws UnexpectedValueException
     */
    private function generateEmailBody(SendEmailEvent $event)
    {
        if (empty($this->messageTemplates[$event->getType()])) {
            throw new UnexpectedValueException('Email template not found');
        }
        $templateBody = $this->messageTemplates[$event->getType()]['body'];

        return $this->templater->render($templateBody, $event->getTemplateData());
    }

    /**
     * @param SendEmailEvent $event
     * @return string
     * @throws UnexpectedValueException
     */
    private function generateEmailSubject(SendEmailEvent $event)
    {
        if (empty($this->messageTemplates[$event->getType()])) {
            throw new UnexpectedValueException('Email template not found');
        }
        $templateSubject = $this->messageTemplates[$event->getType()]['subject'];

        return $this->templater->render($templateSubject, $event->getTemplateData());
    }
}