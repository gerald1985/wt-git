<?php

namespace ServiceBundle\Services;

use Doctrine\ORM\EntityManager;
use Doctrine\DBAL\Query\QueryBuilder;
use DomainBundle\Entity\User;
use DomainBundle\Services\PasswordEncoder;
use Symfony\Component\Security\Core\Encoder\BasePasswordEncoder;
use Symfony\Component\Security\Core\Exception\BadCredentialsException;

class ImportUsersService
{
    /**
     * @var EntityManager
     */
    protected $importEM;

    /**
     * @var PasswordEncoder
     */
    protected $passwordEncoder;

    public function __construct(
        EntityManager $importEM,
        BasePasswordEncoder $passwordEncoder
    )
    {
        $this->importEM = $importEM;
        $this->passwordEncoder = $passwordEncoder;
    }


    /**
     * @return array
     *
     * @throws \Doctrine\DBAL\DBALException
     */
    public function getUsersDataFromImportDb()
    {
        /** @var QueryBuilder $qb */
        $importConnection = $this->importEM->getConnection();
        $qb = $importConnection->createQueryBuilder();
        $qb->select(
            [
                'e.email',
                'e.firstname',
                'e.lastname',
                'e.password',
                'ec.employeeclassname',
            ]
        );
        $qb->from('employee', 'e');
        $qb->leftJoin('e','employee_class', 'ec', 'ec.employeeclassid = e.employeeclassid');
        $qb->where($qb->expr()->eq('e.status', $qb->expr()->literal('1')));
        $qb->andWhere($qb->expr()->eq('ec.status', $qb->expr()->literal('1')));

        $stmt = $importConnection->executeQuery($qb->getSQL());

        return $stmt->fetchAll(\PDO::FETCH_ASSOC);
    }


    /**
     * @param array $data
     * @return User
     * @throws BadCredentialsException
     */
    public function fillUserEntity(array $data)
    {
        $entity = new User();
        $entity->setUsername($data['email']);
        $entity->setEmail($data['email']);
        $entity->setFirstName($data['firstname']);
        $entity->setLastName($data['lastname']);
        $entity->setPassword($this->passwordEncoder->encodePasswordFromMD5($data['password'], null));
        $entity->setEnabled(1);
        $entity->setIsDeleted(0);
        $entity->setStatus(User::STATUS_REGISTERED);
        if ($data['employeeclassname'] === 'Admin') {
            $entity->addRole(User::ROLE_ADMIN);
        } elseif ($data['employeeclassname'] === 'Inventory Manager') {
            $entity->addRole(User::ROLE_TRANSFER_COORDINATOR);
        } elseif ($data['employeeclassname'] === 'Store Manager') {
            $entity->addRole(User::ROLE_MANAGER_STORE);
        } elseif ($data['employeeclassname'] === 'Sales Person') {
            $entity->addRole(User::ROLE_MANAGER_SALE);
        }

        return $entity;
    }
}