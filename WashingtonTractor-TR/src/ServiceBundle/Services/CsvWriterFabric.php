<?php

namespace ServiceBundle\Services;

use League\Csv\Writer;
use SplTempFileObject;

class CsvWriterFabric
{
    /**
     * @return Writer
     */
    public function createWriterInstance()
    {
        return Writer::createFromFileObject(new SplTempFileObject());
    }
}
