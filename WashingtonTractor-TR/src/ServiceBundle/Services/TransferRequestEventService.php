<?php

namespace ServiceBundle\Services;

use Doctrine\ORM\ORMInvalidArgumentException;
use DomainBundle\Entity\TransferRequest\TransferRequest;
use DomainBundle\Entity\TransferRequest\TransferRequestEvent;
use DomainBundle\Entity\User;
use Doctrine\ORM\EntityManager;
use DomainBundle\Model\RepositoryInterface;
use ServiceBundle\Event\TransferRequest\TransferRequestEventEvent;
use ServiceBundle\Exception\Validator\ValidationException;
use ServiceBundle\Model\Repository\TransferRequest\TransferRequestEventRepository;
use ServiceBundle\Model\Repository\TransferRequest\TransferRequestRepository;
use Symfony\Bundle\TwigBundle\TwigEngine;
use Symfony\Component\Templating\EngineInterface;

class TransferRequestEventService
{
    /**
     * @var array
     */
    protected $messageTemplates = [
        TransferRequestEvent::EVENT_NOTE      => [
            'event' => 'ServiceBundle:Events:message.note.event.html.twig'
        ],
        TransferRequestEvent::EVENT_PLACEMENT => [
            'event'           => 'ServiceBundle:Events:message.placement.event.html.twig',
            'event_with_note' => 'ServiceBundle:Events:message.placement.event.with_note.html.twig'
        ],
        TransferRequestEvent::EVENT_UPDATED   => [
            'event'           => 'ServiceBundle:Events:message.update.event.html.twig',
            'event_with_note' => 'ServiceBundle:Events:message.update.event.with_note.html.twig'
        ],
        TransferRequestEvent::EVENT_STATUS_CHANGED   => [
            'event'           => 'ServiceBundle:Events:message.status_changed.event.html.twig',
            'event_with_note' => 'ServiceBundle:Events:message.status_changed.event.with_note.html.twig'
        ],
        TransferRequestEvent::EVENT_RESUBMITTED   => [
            'event'           => 'ServiceBundle:Events:message.resubmit.event.html.twig',
            'event_with_note' => 'ServiceBundle:Events:message.resubmit.event.with_note.html.twig'
        ],
        TransferRequestEvent::EVENT_EXPIRED => [
            'event' => 'ServiceBundle:Events:message.expired.event.html.twig',
        ]
    ];
    /**
     * @var User
     */
    protected $tokenUser;

    /**
     * @var EntityManager
     */
    protected $em;
    /**
     * @var TransferRequestRepository
     */
    protected $transferRequestRepository;

    /**
     * @var TransferRequestEventRepository
     */
    protected $transferRequestEventRepository;

    /**
     * @var TwigEngine
     */
    protected $templater;

    public function __construct(
        User $tokenUser,
        EntityManager $em,
        RepositoryInterface $transferRequestRepository,
        RepositoryInterface $transferRequestEventRepository,
        EngineInterface $templater
    )
    {
        $this->em = $em;
        $this->tokenUser = $tokenUser;
        $this->transferRequestRepository = $transferRequestRepository;
        $this->transferRequestEventRepository = $transferRequestEventRepository;
        $this->templater = $templater;
    }


    /**
     * @param TransferRequestEventEvent $event
     * @return TransferRequestEvent
     *
     * @throws ORMInvalidArgumentException
     * @throws ValidationException
     */
    public function add(TransferRequestEventEvent $event)
    {
        $class = $this->transferRequestEventRepository->getClassName();
        /** @var TransferRequestEvent $entity */
        $entity = new $class;
        /** @var TransferRequest $tr */
        $tr = $event->getTransferRequest();
        $tr->addEvent($entity);

        $this->setHistoryJson($event, $entity);
        $entity->setMessage($this->generateMessageText($event));
        $entity->setTransferRequest($tr);
        $entity->setEventDatetime(new \DateTime());
        $entity->setEventType($event->getEventType());
        $entity->setNote($event->getNote());

        if ($this->tokenUser->getId() !== null) {
            $entity->setAddedBy($this->tokenUser);
        }

        $this->em->persist($entity);

        return $entity;
    }


    /**
     * @param TransferRequestEventEvent $event
     * @param TransferRequestEvent $entity
     * @throws ValidationException
     */
    public function setHistoryJson(TransferRequestEventEvent $event, TransferRequestEvent $entity)
    {
        $data = [
            'new' => $event->getTransferRequest(),
            'old' => $event->getTransferRequestOld()
        ];
        foreach ($data as $k => $d) {
            $tmp = [];
            if ($d instanceof TransferRequest) {
                $tmp = [
                    'store_item_id'      => $d->getStoreItem()->getId(),
                    'store_receiver_id'  => $d->getStoreReceiver()->getId(),
                    'status'             => $d->getStatus(),
                    'shipment_date'      => $d->getShipmentDate(),
                    'receiver_user_name' => $d->getReceiverUserName()
                ];
            }
            $data[$k] = $tmp;
        }
        $data['madeByUserId'] = $this->tokenUser->getId();
        $data['note'] = $event->getNote();

        $entity->setHistoryJson(json_encode($data));
    }


    /**
     * @param TransferRequestEventEvent $event
     * @return string
     */
    public function generateMessageText(TransferRequestEventEvent $event)
    {
        $template = '';
        if (!empty($this->messageTemplates[$event->getEventType()])) {
            $k = 'event';
            if (
                $event->getEventType() !== TransferRequestEvent::EVENT_NOTE
                && !is_null($event->getNote())
            ) {
                $k = 'event_with_note';
            }
            $template = $this->messageTemplates[$event->getEventType()][$k];
        }

        return $this->templater->render($template, [
            'madeByUser' => $this->tokenUser,
            'note'       => $event->getNote(),
            'new'        => $event->getTransferRequest(),
            'old'        => $event->getTransferRequestOld(),
            'dateTime'   => new \DateTime()
        ]);
    }
}