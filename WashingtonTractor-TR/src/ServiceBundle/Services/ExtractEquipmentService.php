<?php

namespace ServiceBundle\Services;

use League\Csv\Writer;
use ServiceBundle\Model\Repository\TransferRequest\TransferRequestRepository;
use Symfony\Component\Translation\TranslatorInterface;
use InvalidArgumentException;
use DateTime;

class ExtractEquipmentService
{
    /** @var CsvWriterFabric */
    private $_csvWriterFabric;
    /** @var TransferRequestRepository */
    private $_transferRequestRepository;
    /** @var TranslatorInterface */
    private $_translator;

    public function __construct(
        CsvWriterFabric $writerFabric,
        TransferRequestRepository $transferRequestRepository,
        TranslatorInterface $translator
    )
    {
        $this->_csvWriterFabric = $writerFabric;
        $this->_transferRequestRepository = $transferRequestRepository;
        $this->_translator = $translator;
    }

    /**
     * @param DateTime $shipmentDateFrom
     * @param DateTime $shipmentDateTo
     *
     * @return Writer
     *
     * @throws InvalidArgumentException
     */
    public function getCsv(DateTime $shipmentDateFrom, DateTime $shipmentDateTo)
    {
        $this->checkDateInterval($shipmentDateFrom, $shipmentDateTo);

        $csv = $this->_csvWriterFabric->createWriterInstance();

        $csv->insertOne($this->getCsvHeader());
        $csv->insertAll(
            $this->getCsvData($shipmentDateFrom, $shipmentDateTo)
        );

        return $csv;
    }

    /**
     * @param DateTime $shipmentDateFrom
     * @param DateTime $shipmentDateTo
     *
     * @throws InvalidArgumentException
     */
    private function checkDateInterval(DateTime $shipmentDateFrom, DateTime $shipmentDateTo)
    {
        if ($shipmentDateTo < $shipmentDateFrom) {
            throw new InvalidArgumentException('Start date should less or equal of the end date.');
        }
    }

    /**
     * @param DateTime $shipmentDateFrom
     * @param DateTime $shipmentDateTo
     *
     * @return array
     */
    private function getCsvData(DateTime $shipmentDateFrom, DateTime $shipmentDateTo)
    {
        $q = $this->_transferRequestRepository->createQueryBuilder('o');
        $q->select(
            'osi.stockNumber, o.status, ost.name as hostingLocationName, osr.name as receiverLocationName'
        );
        $q->leftJoin('o.storeItem', 'osi');
        $q->leftJoin('osi.store', 'ost');
        $q->leftJoin('o.storeReceiver', 'osr');

        $q->andWhere('o.shipmentDate >= :start_date');
        $q->andWhere('o.shipmentDate <= :end_date');
        $q->andWhere('ost.id != osr.id');

        $q->setParameters([
            ':start_date' => $shipmentDateFrom->format('Y-m-d 00:00:00'),
            ':end_date' => $shipmentDateTo->format('Y-m-d 23:59:59'),
        ]);

        return $q->getQuery()->getArrayResult();
    }

    /**
     * @return array
     *
     * @throws \Symfony\Component\Translation\Exception\InvalidArgumentException
     */
    private function getCsvHeader()
    {
        return [
            $this->_translator->trans('equipment.header.stockNumber', [], 'csv'),
            $this->_translator->trans('equipment.header.status', [], 'csv'),
            $this->_translator->trans('equipment.header.hostingLocation', [], 'csv'),
            $this->_translator->trans('equipment.header.receivingLocation', [], 'csv'),
        ];
    }
}
