<?php

namespace ServiceBundle\Transformers;

use DomainBundle\Entity\Item\Item;
use DomainBundle\Entity\Store\Store;
use League\Fractal;

class ItemModelsReadTransformer extends Fractal\TransformerAbstract
{
    /**
     * @param Item|null $entity
     * @return array
     */
    public function transform(Item $entity = null)
    {
        /** @var Item $entity */
        if ($entity === null) {
            return [];
        }

        /** @var Store $store */
//        $store = $entity->getStoresItems();

        $data = $entity->getModelName();


        return $data;
    }
}