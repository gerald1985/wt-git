<?php

namespace ServiceBundle\Transformers;

use DomainBundle\Entity\Item\Item;
use DomainBundle\Entity\Store\Store;
use League\Fractal;

class ItemReadTransformer extends Fractal\TransformerAbstract
{
    /**
     * @param Item|null $entity
     * @return array
     */
    public function transform(Item $entity = null)
    {
        /** @var Item $entity */
        if ($entity === null) {
            return [];
        }

        /** @var Store $store */
//        $store = $entity->getStoresItems();

        $data = [
            'id'                => (int)$entity->getId(),
            'pin'               => $entity->getPin(),
            'tagNumber'         => $entity->getPin(),
            'attachedTo'        => $entity->getAttachedTo(),
            'attachmentNumber'  => $entity->getAttachmentNumber(),
            'make'              => $entity->getMake(),
            'category'          => $entity->getCategory(),
            'modelName'         => $entity->getModelName(),
            'yearMfrd'          => $entity->getYearMfrd()
        ];


        return $data;
    }
}