<?php

namespace ServiceBundle\Transformers;

use DomainBundle\Entity\TransferRequest\TransferRequest;
use DomainBundle\Entity\TransferRequest\TransferRequestEvent;
use League\Fractal;

class TransferRequestReadTransformer extends Fractal\TransformerAbstract
{
    /**
     * @param TransferRequest|null $entity
     * @return array
     */
    public function transform(TransferRequest $entity = null)
    {
        /** @var TransferRequest $entity */
        if ($entity === null) {
            return [];
        }

        $data = [
            'id'                => $entity->getId(),
            'requestTimestamp'  => $entity->getRequestDatetime()->getTimestamp(),
            'purpose'           => $entity->getPurpose(),
            'status'            => $entity->getStatus(),
            'getEmailUpdates'   => $entity->getGetEmailUpdates(),
            'shipmentTimestamp' => $entity->getShipmentDate() ? $entity->getShipmentDate()->getTimestamp() : null,
            'receiverUserName'  => $entity->getReceiverUserName()
        ];

        $data['storeItem'] = StoreItemReadTransformer::transform($entity->getStoreItem());
        $data['requestedBy'] = UserReadTransformer::transform($entity->getRequestedBy());
        $data['storeReceiver'] = StoreReadTransformer::transform($entity->getStoreReceiver());
        $data['events'] = [];

        $activitiesCounter = 0;
        foreach ($entity->getEvents() as $event) {
            /** @var TransferRequestEvent $event */
            $activitiesCounter++;
            $data['events'][] = TransferRequestEventReadTransformer::transform($event);
            if (!empty($event->getNote())) {
                $activitiesCounter++;
            }
        }
        $data['activitiesCounter'] = $activitiesCounter;

        return $data;
    }
}