<?php
namespace ServiceBundle\Transformers;

use DomainBundle\Entity\User;
use League\Fractal;

class ManagerReadTransformer extends Fractal\TransformerAbstract
{
    /**
     * @param User|null $entity
     * @return array
     */
    public static function transform(User $entity = null)
    {
        /** @var User $entity */
        if ($entity === null) {
            return [];
        }

        $roles = $entity->getRoles();
        if ($key = array_search(User::ROLE_DEFAULT, $roles)) {
            unset($roles[$key]);
        }

        $data = [
            'id'    => (int)$entity->getId(),
            'email' => $entity->getEmail(),
            'role'  => array_shift($roles)
        ];

        $data['firstName'] = $entity->getFirstName();
        $data['lastName'] = $entity->getLastName();

        return $data;
    }
}