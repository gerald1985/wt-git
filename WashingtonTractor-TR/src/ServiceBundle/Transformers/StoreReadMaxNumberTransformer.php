<?php

namespace ServiceBundle\Transformers;

use DomainBundle\Entity\Store\Store;
use League\Fractal;

class StoreReadMaxNumberTransformer extends Fractal\TransformerAbstract
{
    /**
     * @param Store $entity
     * @return array
     */
    public static function transform(Store $entity = null)
    {
        if ($entity === null) {
            return [];
        }

        $data = [
            'maxNumber' => 0
        ];
        if ($entity instanceof Store) {
            $data['maxNumber'] = $entity->getNumber();
        }

        return $data;
    }
}