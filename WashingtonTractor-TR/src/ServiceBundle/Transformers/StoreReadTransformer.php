<?php
namespace ServiceBundle\Transformers;

use DomainBundle\Entity\Store\Store;
use League\Fractal;

class StoreReadTransformer extends Fractal\TransformerAbstract
{
    /**
     * @param Store $entity
     * @return array
     */
    public static function transform(Store $entity = null)
    {
        if ($entity === null) {
            return [];
        }

        $data = [
            'id' => (int)$entity->getId()
        ];

        $data['status'] = $entity->getStatus();
        $data['name'] = $entity->getName();
        $data['number'] = $entity->getNumber();
        $data['address'] = $entity->getAddress();
        $data['manager'] = ManagerReadTransformer::transform($entity->getManager());

        return $data;
    }
}