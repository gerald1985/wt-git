<?php
namespace ServiceBundle\Transformers;

use DomainBundle\Entity\Store\Store;
use DomainBundle\Entity\User;
use League\Fractal;

class UserReadTransformer extends Fractal\TransformerAbstract
{
    /**
     * @param User|null $entity
     * @return array
     */
    public static function transform(User $entity = null)
    {
        /** @var User $entity */
        if ($entity === null) {
            return [];
        }

        $roles = $entity->getRoles();
        if ($key = array_search(User::ROLE_DEFAULT, $roles)) {
            unset($roles[$key]);
        }

        $data = [
            'id'    => (int)$entity->getId(),
            'status' => $entity->getStatus(),
            'email' => $entity->getEmail(),
            'role'  => array_shift($roles)
        ];

        $data['firstName'] = $entity->getFirstName();
        $data['lastName'] = $entity->getLastName();

        $data['stores'] = self::getStores($entity);
        return $data;
    }

    /**
     * @param User $entity
     * @return array
     */
    private static function getStores(User $entity)
    {
        $stores = [];
        if (
            $entity->hasRole(User::ROLE_MANAGER_STORE)
            || $entity->hasRole(User::ROLE_MANAGER_SALE)
        ) {
            foreach ($entity->getStores() as $store) {
                /** @var Store $store */
                $stores[] = [
                    'id' => $store->getId(),
                    'name' => $store->getName(),
                ];
            }
        }

        return $stores;
    }
}