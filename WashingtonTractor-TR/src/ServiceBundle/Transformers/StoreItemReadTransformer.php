<?php
namespace ServiceBundle\Transformers;

use DomainBundle\Entity\Item\Item;
use DomainBundle\Entity\Store\Store;
use DomainBundle\Entity\Store\StoreItem;
use League\Fractal;

class StoreItemReadTransformer extends Fractal\TransformerAbstract
{
    /**
     * @param StoreItem|null $entity
     * @return array
     */
    public function transform(StoreItem $entity = null)
    {
        /** @var StoreItem $entity */
        if ($entity === null) {
            return [];
        }

        /** @var Item $store */
        $item = $entity->getItem();
        /** @var Store $store */
        $store = $entity->getStore();

        $data = ItemReadTransformer::transform($item);

        $data['id'] = $entity->getId();
        $data['stockNumber'] = $entity->getStockNumber();
        $data['meter'] = $entity->getMeter();
        $data['daysInStock'] = $entity->getDaysInStock();
        $data['salesPerson'] = $entity->getSalesPerson();
        $data['usage'] = $entity->getUsage();

        $data['store'] = StoreReadTransformer::transform($store);

        return $data;
    }
}