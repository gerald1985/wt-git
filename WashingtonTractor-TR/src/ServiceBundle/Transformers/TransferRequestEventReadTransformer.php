<?php

namespace ServiceBundle\Transformers;

use DomainBundle\Entity\TransferRequest\TransferRequestEvent;
use League\Fractal;

class TransferRequestEventReadTransformer extends Fractal\TransformerAbstract
{
    /**
     * @param TransferRequestEvent|null $entity
     * @return array
     */
    public static function transform(TransferRequestEvent $entity = null)
    {
        /** @var TransferRequestEvent $entity */
        if ($entity === null) {
            return [];
        }

        $data = [
            'id'                => $entity->getId(),
            'eventTimestamp'    => $entity->getEventDatetime()->getTimestamp(),
            'eventType'         => $entity->getEventType(),
            'note'              => $entity->getNote(),
            'message'           => $entity->getMessage(),
            'transferRequestId' => $entity->getTransferRequest()->getId(),
        ];

        $data['addedBy'] = UserReadTransformer::transform($entity->getAddedBy());

        return $data;
    }
}