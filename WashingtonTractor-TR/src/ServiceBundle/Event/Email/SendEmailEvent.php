<?php

namespace ServiceBundle\Event\Email;

use Symfony\Component\EventDispatcher\Event;
use Swift_Mime_Attachment;

class SendEmailEvent extends Event
{
    /** @var array */
    protected $to = [];
    /** @var string */
    protected $type;
    /** @var array */
    protected $templateData = [];
    /** @var Swift_Mime_Attachment[] */
    protected $attachments = [];

    /**
     * @return array
     */
    public function getTo()
    {
        return $this->to;
    }

    /**
     * @param string $to
     */
    public function addTo($to)
    {
        $this->to[] = $to;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @param null $key
     * @return array|mixed
     */
    public function getTemplateData($key = null)
    {
        return $key ? $this->templateData[$key] : $this->templateData;
    }

    /**
     * @param string $key
     * @param mixed $value
     */
    public function addTemplateData($key, $value)
    {
        $this->templateData[$key] = $value;
    }

    /**
     * @param array $templateData
     */
    public function setTemplateData(array $templateData)
    {
        $this->templateData = $templateData;
    }

    /**
     * @return Swift_Mime_Attachment[]
     */
    public function getAttachments()
    {
        return $this->attachments;
    }

    /**
     * @param Swift_Mime_Attachment $attachment
     */
    public function addAttachment(Swift_Mime_Attachment $attachment)
    {
        $this->attachments[] = $attachment;
    }
}
