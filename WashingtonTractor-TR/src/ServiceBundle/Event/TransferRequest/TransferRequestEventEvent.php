<?php

namespace ServiceBundle\Event\TransferRequest;

use DomainBundle\Entity\TransferRequest\TransferRequest;
use Symfony\Component\EventDispatcher\Event;

class TransferRequestEventEvent extends Event
{
    /** @var TransferRequest */
    protected $transferRequest;
    /** @var TransferRequest */
    protected $transferRequestOld;
    /** @var string */
    protected $eventType;
    /** @var string|null */
    protected $note;

    public function __construct(TransferRequest $transferRequest, TransferRequest $transferRequestOld = null, $note = null)
    {
        $this->transferRequest = $transferRequest;
        $this->transferRequestOld = $transferRequestOld;
        $this->note = $note;
    }

    /**
     * @return TransferRequest
     */
    public function getTransferRequestOld()
    {
        return $this->transferRequestOld;
    }

    /**
     * @return TransferRequest[
     */
    public function getTransferRequest()
    {
        return $this->transferRequest;
    }

    /**
     * @return string
     */
    public function getEventType()
    {
        return $this->eventType;
    }

    /**
     * @param string $eventType
     */
    public function setEventType($eventType)
    {
        $this->eventType = $eventType;
    }

    /**
     * @return string
     */
    public function getNote()
    {
        return $this->note;
    }

}
