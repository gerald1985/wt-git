import { Injectable, OnInit } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { TransferRequestApi } from '../generated/api/TransferRequestApi';
import { ReadTransferRequest } from '../generated/model/ReadTransferRequest';
import { ListOfTransferRequestsApi } from '../generated/api/ListOfTransferRequestsApi';
import { Observable } from 'rxjs/Observable';
import * as apiRequests from '../generated/model/ReadTransferRequest';
import { Subscription } from 'rxjs/Subscription';
import { AuthService } from './auth.service';
import { CommonError } from '../generated/model/CommonError';
import { CommonErrorAttribute } from '../generated/model/CommonErrorAttribute';
import { MessageService } from './message.service';
import { ResubmitTransferRequest } from '../generated/model/ResubmitTransferRequest';
import { RequestsGetParams } from '../components/requests/requset-get-params.interface';
import { UpdateTransferRequest } from '../generated/model/UpdateTransferRequest';
import { DeleteSubscriptionStore } from '../classes/delete-subscription-store';
import { ReadStore } from '../generated/model/ReadStore';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import {Router} from '@angular/router';
import {WriteRequestNote} from '../generated/model/WriteRequestNote';
import {WriteTransferRequest} from '../generated/model/WriteTransferRequest';

@Injectable()
export class RequestService {
	// private pendingCount$ = new Subject<number>();
	private pendingCount$: Observable<number>;
	requests$ = new BehaviorSubject<Array<ReadTransferRequest>>([]);
	private transferRequestsToDelete = new DeleteSubscriptionStore<ReadTransferRequest>();

	params$ = new BehaviorSubject<RequestsGetParams>({
		perPage: 30,
		page: 1
	});

	private clearPrevItems = true;

	constructor(
		private requestApi: TransferRequestApi,
		private listRequestApi: ListOfTransferRequestsApi,
		private messageService: MessageService,
		private router: Router,
		private auth: AuthService
	) {
		this.updatePendingCounts();
		this.transferRequestsToDelete.onDelete().subscribe(request => {
			this.requestApi.transferRequestIdCancelPut(request.id).subscribe(newRequest => {
				// do not reload request, only update one item
				// this.getRequests();
				this.changeTransferRequestStatus(newRequest['data']);
			}, errorResponse => {
				try {
					let error = <CommonError>errorResponse.json();
					this.messageService.showMessage(error.errorMessage.message);
				} catch (e) {
					this.messageService.showMessage(`Unable to cancel TR # ${request.id}`);
				}
			});
		});
		this.params$.subscribe(() => {
			this.getRequests();
		});
	}

	updatePendingCounts() {

		this.pendingCount$ = Observable.merge(Observable.interval(15000), Observable.from([1]).delay(3000))
			.switchMap(_ => this.getPedingRequests(this.auth.getUserStoreIds()))
			.map(response => response.data.length ? response.data.length : null ).distinct().share();
	}

	isRequester(requesterId) {
		return this.auth.getUserId() === requesterId;
	}

	private getPedingRequests = (storeIds: number[]) => this.listRequestApi.transferRequestsGet(
		undefined, undefined, undefined, undefined, undefined,
		apiRequests.ReadTransferRequest.StatusEnum.PENDING.toString(),
		undefined, undefined, undefined, 100, 1
	);

	decline(item: ReadTransferRequest, lastNote: string) {
		let decline$ = this.requestApi.transferRequestIdDeclinePut(item.id, {lastNote}).share();
		decline$.subscribe(response => {
			this.replaceWithUpdatedItem(response);
		}, response => {
			this.processErrors(response.json());
		});
		return decline$;
	}

	closeSidebar() {
		this.router.navigate(['/requests']);
	}

	openSidebar(transferId) {
		this.router.navigate(['/requests', {id: transferId}]);
	}

	getRequests() {
		let params = this.params$.getValue();
		this.listRequestApi.transferRequestsGet(
			params.itemModelName,
			params.itemStockNumber,
			params.itemStoreIds,
			params.receivingStoreIds,
			params.purposes,
			params.statuses,
			params.requestedByIds,
			params.requestTimestampFrom,
			params.requestTimestampTo,
			params.perPage,
			params.page
		).subscribe(response => {
			if (this.clearPrevItems) this.requests$.next([]);
			this.requests$.next([...this.requests$.getValue(), ...response.data.map(item => {
				Object.assign(item, {
					shipmentDate: item.shipmentTimestamp ? new Date(item.shipmentTimestamp * 1000) : null,
					requestDate: item.requestTimestamp ? new Date(item.requestTimestamp * 1000) : null,
					removed: this.transferRequestsToDelete.isSubscribed(item)
				});
				return item;
			})]
			);
		});
	}

	resubmit(id: number, item: ResubmitTransferRequest) {
		let request$ = this.requestApi.transferRequestIdResubmitPut(id, item).share();
		request$.subscribe(_ => this.getRequests(), error => {});
		return request$;
	}


	sendNote(transferRequestId: number, note: WriteRequestNote) {
		let sendNote$ = this.requestApi.transferRequestIdNotePost(transferRequestId, note).share();
		sendNote$.subscribe(_ => this.getRequests(), error => {});
		return sendNote$;
	}

	addTransferRequest(request: WriteTransferRequest) {
		let request$ = this.requestApi.transferRequestPost(request).share();
		request$.subscribe(_ => this.getRequests(), error => {});
		return request$;
	}

	updateTransferRequest(request: UpdateTransferRequest) {
		let request$ = this.requestApi.transferRequestPut(request);
		request$.subscribe(_ => this.getRequests(), error => {});
		return request$;
	}

	changeTransferRequestStatus(transferRequest: ReadTransferRequest) {
		let requests = this.requests$.getValue();
		this.requests$.next(requests.map(request => {
			if (request.id !== transferRequest.id) return request;
			return this.transferRequestsToDelete.assingRemovedStatus(transferRequest);
		}));
	}

	handleCancel(transferRequest: ReadTransferRequest) {
		this.transferRequestsToDelete.subscribe(transferRequest);
		this.changeTransferRequestStatus(transferRequest);
	}

	handleRestore(transferRequest: ReadTransferRequest) {
		this.transferRequestsToDelete.unsubscribe(transferRequest);
		this.changeTransferRequestStatus(transferRequest);
	}

	cancelAllToDelete() {
		this.transferRequestsToDelete.cancelAll(false);
	}

	approve(item: ReadTransferRequest) {
		let approve$ = this.requestApi.transferRequestIdApprovePut(item.id).share();
		approve$.subscribe(response => {
			this.replaceWithUpdatedItem(response);
		}, response => {
			this.processErrors(response.json());
		});
		return approve$;
	}

	replaceWithUpdatedItem(response) {
		if (!response || !response['data']) {
			return;
		}
		let requestItem = response['data'];

		let requests = this.requests$.getValue();
		let index = requests.map(t => t.id).indexOf(requestItem.id);
		if (index > -1) {
			Object.assign(requestItem, {
				shipmentDate: requestItem.shipmentTimestamp ? new Date(requestItem.shipmentTimestamp * 1000) : null,
				requestDate: requestItem.requestTimestamp ? new Date(requestItem.requestTimestamp * 1000) : null,
				removed: this.transferRequestsToDelete.isSubscribed(requestItem)
			});
			let value = [...requests.slice(0, index), requestItem, ...requests.slice(index + 1)];
			this.requests$.next(value);
		}
	}

	complete(item: ReadTransferRequest) {
		let complete$ = this.requestApi.transferRequestIdCompletePut(item.id).share();
		complete$.subscribe(response => {
			this.replaceWithUpdatedItem(response);
		}, response => {
			this.processErrors(response.json());
		});
		return complete$;
	}

	private processErrors(error: CommonError) {
		if (!error.errorMessage) {
			this.messageService.showMessage('Unknown error');
			return;
		}
		this.messageService.showMessage(error.errorMessage.message);
		if (!error.errorMessage.attributes) return;
		error.errorMessage.attributes.forEach((attr: CommonErrorAttribute) => {
			attr.errors.forEach(errorText => {
				this.messageService.showMessage(errorText);
			});
		});
	}

	loadNextPage() {
		let params = this.params$.getValue();
		let nextPage = params.page + 1;
		this.updateGetParams(Object.assign({}, params, {page: nextPage}), false);
	}

	updateGetParams(newParams: RequestsGetParams = {}, clear = true) {
		this.clearPrevItems = clear;
		this.params$.next(Object.assign({}, this.params$.getValue(), newParams));
	}

	onPendingCountChange = () => this.pendingCount$;
}
