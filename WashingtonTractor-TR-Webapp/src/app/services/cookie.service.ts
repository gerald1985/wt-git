import { Injectable } from '@angular/core';

@Injectable()
export class CookieService {

	constructor() { }

	private loadCookies = function () {
		let cookies: Object = {};
		document.cookie.split(';').forEach(function (cookieNameValue) {
			if (!cookieNameValue) return;
			let cookiePair = cookieNameValue.trim().split('=');
			cookies[cookiePair[0]] = cookiePair[1];
		});
		return cookies;
	};

	getCookie = function(name) {
		let cookies = this.loadCookies();
		return cookies[name];
	};

	setCookie = function(name, value) {
		document.cookie = `${name}=${value}; expires=Fri, 31 Dec 9999 23:59:59 GMT`;
	};

}
