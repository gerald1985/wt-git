import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

@Injectable()
export class MessageService {

	messages = new BehaviorSubject<Array<string>>([]);

	constructor() { }

	showMessage(message: string) {
		this.messages.next([...this.messages.getValue(), message]);
		setTimeout(() => {
			let messages = this.messages.getValue();
			let index = messages.indexOf(message);
			this.messages.next([...messages.slice(0, index), ...messages.slice(index + 1)]);
		}, 8000);
	}

}
