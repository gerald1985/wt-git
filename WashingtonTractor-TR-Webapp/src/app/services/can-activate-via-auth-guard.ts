import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { AuthService } from './auth.service';

@Injectable()
export class CanActivateViaAuthGuard implements CanActivate {
	canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
		const path = route.url[0].path;
		const authenticated = this.authService.isAuthenticated();
		if (!authenticated && path !== 'login') {
			this.authService.linkPath = `/${route.url.toString()}`;
			this.router.navigateByUrl('/login');
			return false;
		}
		if (path === 'login' && authenticated) {
			this.router.navigateByUrl('/');
			return false;
		}
		return true;
	}

	constructor(private authService: AuthService, private router: Router) {
	}
}
