import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import { Http, RequestOptionsArgs, Response, Request, RequestOptions, Headers } from '@angular/http';
import { CommonError } from '../generated/model/CommonError';
import { AuthService, AuthToken } from './auth.service';
import { Subscription } from 'rxjs/Subscription';
import { Observer } from 'rxjs/Observer';
import { Subject } from 'rxjs/Subject';
import { ActivatedRoute } from '@angular/router';
import { MessageService } from './message.service';

@Injectable()
export class AppHttpService {

	private defaultOpts: RequestOptions;
	private activeRequests: Array<ActiveRequest> = [];

	constructor(private http: Http, private auth: AuthService, private messages: MessageService) {
		this.defaultOpts = new RequestOptions({
			headers: new Headers({'Content-Type': 'application/json', 'Accept': 'application/json'})
		});
		this.auth.onTokenChange().subscribe((token: AuthToken) => {
			if (!token) {
				this.defaultOpts.headers.delete('Authorization');
				return;
			}
			this.defaultOpts.headers.set('Authorization', `Bearer ${token.access_token}`);

			// update active requests if token is updated
			this.activeRequests.forEach(activeRequest => {
				this.subscribe(activeRequest);
			});
		});
	}

	private mergeOptions(defaultOpts: RequestOptions, providedOpts?: RequestOptionsArgs, method?: string, url?: string): RequestOptions {
		let newOptions = defaultOpts;
		if (providedOpts) {
			return newOptions.merge(providedOpts);
		}
		return newOptions.merge(new RequestOptions({ method: method, url: url }));
	}

	setDefaultOptions(options: RequestOptionsArgs) {
		this.defaultOpts = new RequestOptions(options);
	}
	getDefaultOptions(): RequestOptions {
		return new RequestOptions(this.defaultOpts);
	}

	setDefaultHeaders(headers: Headers) {
		this.defaultOpts.headers = new Headers(headers.toJSON());
	}
	getDefaultHeaders(): Headers {
		return new Headers(this.defaultOpts.headers.toJSON());
	}

	private removeRequest(activeRequest: ActiveRequest) {
		let requests = this.activeRequests;
		let index = requests.indexOf(activeRequest);
		if (index === -1) return;
		this.activeRequests = [...requests.slice(0, index), ...requests.slice(index + 1)];
	}

	private addRequest(activeRequest: ActiveRequest) {
		let index = this.activeRequests.indexOf(activeRequest);
		if (index !== -1) return;
		this.activeRequests.push(activeRequest);
	}

	request(request: string | Request, options?: RequestOptionsArgs): Observable<Response> {
		let activeRequest = <ActiveRequest>{
			request,
			options,
			response$: new Subject<Response>()
		};
		this.subscribe(activeRequest);
		return activeRequest.response$;
	}

	private subscribe(activeRequest: ActiveRequest) {
		this.addRequest(activeRequest);
		if (activeRequest.subscription) {
			activeRequest.subscription.unsubscribe();
		}
		const token = this.defaultOpts.headers.get('Authorization');
		if (activeRequest.options && token) {
			activeRequest.options.headers.set('Authorization', token);
		}
		activeRequest.subscription = this.http.request(activeRequest.request, this.mergeOptions(this.defaultOpts, activeRequest.options))
			.subscribe(response => {
				activeRequest.response$.next(response);
				activeRequest.response$.complete();
				this.removeRequest(activeRequest);
			}, errorResponse => {
				let refreshingToken = this.handleError(errorResponse);
				if (refreshingToken) return;
				this.removeRequest(activeRequest);
				activeRequest.response$.error(errorResponse);
				activeRequest.response$.complete();
			});
	}

	private handleError(errorResponse) {
		let error = errorResponse.json();
		if (error.error_code !== 5000) return;
		switch (error.error_message) {
			case 'The access token provided has expired.':
				this.refreshToken();
				return true;
			case 'User account is disabled.':
				this.messages.showMessage('User account is disabled.');
				this.auth.logout();
		}
	}

	private refreshToken() {
		this.activeRequests.forEach(request => {
			request.subscription.unsubscribe();
		});
		this.auth.refreshToken();
	}

}

interface ActiveRequest {
	request: string | Request;
	subscription?: Subscription;
	options?: RequestOptionsArgs;
	response$: Subject<Response>;
}
