import { Injectable, Inject } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { CookieService } from './cookie.service';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Configuration } from '../generated/configuration';
import { BASE_PATH } from '../generated/variables';
import { Router } from '@angular/router';
import { ReadUser } from '../generated/model/ReadUser';
import * as apiUsers from '../generated/model/ReadUser';
import { MessageService } from './message.service';
import { OAuthConfig } from '../oauth-config';

@Injectable()
export class AuthService {

	private COOKIE_MARKER = 'washington-tractor-token-user';

	private secret = OAuthConfig.secret;
	private clientId = OAuthConfig.clientId;
	private url = OAuthConfig.url;

	private token$: BehaviorSubject<AuthToken>;

	linkPath: string;

	private oAuthUrl = `/oauth/v2/token?grant_type=password&client_id=${this.clientId}&client_secret=${this.secret}`;
	private refreshTokenUrl = `/oauth/v2/token?grant_type=refresh_token&client_id=${this.clientId}&client_secret=${this.secret}`;

	constructor(
		private http: Http,
		private cookie: CookieService,
		private router: Router,
		private message: MessageService,
		private config: Configuration
	) {
		this.token$ = new BehaviorSubject<AuthToken>(this.retrieveToken());
		this.onTokenChange().subscribe((token: AuthToken) => {
			this.storeToken(token);
			if (!token) {
				this.config.accessToken = null;
				return;
			}
			this.config.accessToken = token.access_token;
		});
	}

	private storeToken(token: AuthToken) {
		try {
			if (!token) {
				this.cookie.setCookie(this.COOKIE_MARKER, 'null');
				window.localStorage.setItem(this.COOKIE_MARKER, 'null');
				return;
			}
			this.cookie.setCookie(this.COOKIE_MARKER, JSON.stringify({
				access_token: token.access_token,
				expires_in: token.expires_in,
				refresh_token: token.refresh_token,
				scope: token.scope,
				token_type: token.token_type
			}));
			window.localStorage.setItem(this.COOKIE_MARKER, JSON.stringify(token.user));
		} catch (e) {
			this.cookie.setCookie(this.COOKIE_MARKER, 'null');
			console.error('Error saving token in cookies', e);
		}
	}

	private retrieveToken(): AuthToken {
		let token: AuthToken = null;
		try {
			token = JSON.parse(this.cookie.getCookie(this.COOKIE_MARKER) || 'null');
			if (token) {
				token.user = JSON.parse(window.localStorage.getItem(this.COOKIE_MARKER));
				if (!token.user) token = null;
			}
		} catch (e) {
			token = null;
			console.error('Error getting token from cookies', e);
		} finally {
			return token;
		}
	}

	private hasRole(role: apiUsers.ReadUser.RoleEnum) {
		if (!this.isAuthenticated()) return false;
		return this.token$.getValue().user.role === role;
	}

	isAdmin = () => this.hasRole(apiUsers.ReadUser.RoleEnum.ADMIN);
	isDriver = () => this.hasRole(apiUsers.ReadUser.RoleEnum.DRIVER);
	isManagerSale = () => this.hasRole(apiUsers.ReadUser.RoleEnum.MANAGERSALE);
	isManagerStore = () => this.hasRole(apiUsers.ReadUser.RoleEnum.MANAGERSTORE);
	isTransferCoordinator = () => this.hasRole(apiUsers.ReadUser.RoleEnum.TRANSFERCOORDINATOR);

	isAuthenticated(): boolean {
		return !!this.token$.getValue();
	}

	getUserId(): Number {
		if (this.isAuthenticated()) {
			return this.token$.getValue().user.id;
		}
		return;
	}

	getUserStoreIds(): number[] {
		if (!this.isManagerSale() && !this.isManagerStore()) return [];
		return this.token$.getValue().user.stores.map(store => store.id);
	}

	getAccessToken(): string {
		if (this.isAuthenticated()) {
			return this.token$.getValue().access_token;
		}
		return;
	}

	getUserName(): string {
		if (this.isAuthenticated()) {
			let user = this.token$.getValue().user;
			return user.firstName + ' ' + user.lastName;
		}
		return;
	}

	setUser(user: ReadUser) {
		let auth = this.token$.getValue();
		auth.user = user;
		this.token$.next(auth);
	}

	logout() {
		this.token$.next(null);
		this.router.navigateByUrl('/login');
	}

	refreshToken() {
		let url = `${this.url}${this.refreshTokenUrl}&refresh_token=${this.token$.getValue().refresh_token}`;
		let options = { headers: new Headers({'Content-Type': 'application/json', 'Accept': 'application/json'}) };
		let refresh$ = this.http.get(url, options).map(e => e.json()).share();
		refresh$.subscribe((token: AuthToken) => {
			this.token$.next(token);
		}, error => {
			this.message.showMessage('Time of your session is up. Please, log in again.');
			this.logout();
		});
		return refresh$;
	}

	login(username, password) {
		let url = `${this.url}${this.oAuthUrl}&password=${password}&username=${username}`;
		let login$ = this.http.get(url).map(e => e.json()).share();
		login$.subscribe((token: AuthToken) => {
			this.token$.next(token);
			this.router.navigateByUrl(this.linkPath || '/requests');
		}, error => {
		});
		return login$;
	}

	onTokenChange() {
		return this.token$.share();
	}
}

export interface AuthToken {
	access_token: string;
	expires_in: number;
	refresh_token: string;
	scope: any;
	token_type: string;
	user: ReadUser;
}
