import * as transferApi from '../generated/model/ReadTransferRequest';

export const PURPOSE_TITLES = {};
PURPOSE_TITLES[transferApi.ReadTransferRequest.PurposeEnum.EQUIPMENTSALE] = 'Equipment Sale';
PURPOSE_TITLES[transferApi.ReadTransferRequest.PurposeEnum.RENTAL] = 'Rental';
PURPOSE_TITLES[transferApi.ReadTransferRequest.PurposeEnum.DEMO] = 'Demo';
PURPOSE_TITLES[transferApi.ReadTransferRequest.PurposeEnum.STOCKLEVELING] = 'Stock Leveling';

export const PURPOSES = Object.keys(PURPOSE_TITLES).map(purpose => ({value: purpose, viewValue: PURPOSE_TITLES[purpose]}));
