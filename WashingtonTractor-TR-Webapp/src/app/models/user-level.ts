import * as apiUsers from '../generated/model/ReadUser';

export const USER_LEVEL = {};
USER_LEVEL[apiUsers.ReadUser.RoleEnum.MANAGERSALE] = 1;
USER_LEVEL[apiUsers.ReadUser.RoleEnum.MANAGERSTORE] = 2;
USER_LEVEL[apiUsers.ReadUser.RoleEnum.DRIVER] = 3;
USER_LEVEL[apiUsers.ReadUser.RoleEnum.TRANSFERCOORDINATOR] = 4;
USER_LEVEL[apiUsers.ReadUser.RoleEnum.ADMIN] = 5;

export const USER_ROLE = {};
USER_ROLE[apiUsers.ReadUser.RoleEnum.MANAGERSALE] = 'Sales Person';
USER_ROLE[apiUsers.ReadUser.RoleEnum.MANAGERSTORE] = 'Store Manager';
USER_ROLE[apiUsers.ReadUser.RoleEnum.DRIVER] = 'Driver';
USER_ROLE[apiUsers.ReadUser.RoleEnum.TRANSFERCOORDINATOR] = 'Transfer Coordinator';
USER_ROLE[apiUsers.ReadUser.RoleEnum.ADMIN] = 'Admin';

export const USER_ROLES = Object.keys(USER_ROLE).map(role => ({value: role, viewValue: `${USER_LEVEL[role]} ${USER_ROLE[role]}`}));
