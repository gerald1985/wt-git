import * as transferApi from '../generated/model/ReadTransferRequest';

export const STATUS_TITLES = {};
STATUS_TITLES[transferApi.ReadTransferRequest.StatusEnum.APPROVED] = 'Approved';
STATUS_TITLES[transferApi.ReadTransferRequest.StatusEnum.CANCELLED] = 'Cancelled';
STATUS_TITLES[transferApi.ReadTransferRequest.StatusEnum.DECLINED] = 'Declined';
STATUS_TITLES[transferApi.ReadTransferRequest.StatusEnum.PENDING] = 'Pending';
STATUS_TITLES[transferApi.ReadTransferRequest.StatusEnum.COMPLETED] = 'Completed';
STATUS_TITLES[transferApi.ReadTransferRequest.StatusEnum.INTRANSIT] = 'In transit';
STATUS_TITLES[transferApi.ReadTransferRequest.StatusEnum.DELIVERED] = 'Delivered';

export const STATUSES = Object.keys(STATUS_TITLES).map(purpose => ({value: purpose, viewValue: STATUS_TITLES[purpose]}));
