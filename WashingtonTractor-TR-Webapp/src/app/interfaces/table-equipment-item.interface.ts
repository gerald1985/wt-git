import {ReadStoreItem} from '../generated/model/ReadStoreItem';
export interface TableEquipmentItem extends ReadStoreItem {
	locationName?: string;
}
