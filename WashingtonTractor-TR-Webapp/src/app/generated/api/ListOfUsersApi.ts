/**
 * Tractor CRM API
 * Backend API for Tractor CRM. Serves web and native mobile applications.
 *
 * OpenAPI spec version: 1.1.0
 *
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */

import { Inject, Injectable, Optional }                      from '@angular/core';
import { Http, Headers, URLSearchParams }                    from '@angular/http';
import { RequestMethod, RequestOptions, RequestOptionsArgs } from '@angular/http';
import { Response, ResponseContentType }                     from '@angular/http';

import { Observable }                                        from 'rxjs/Observable';
import 'rxjs/add/operator/map';

import * as models                                           from '../model/models';
import { BASE_PATH, COLLECTION_FORMATS }                     from '../variables';
import { Configuration }                                     from '../configuration';
import {AppHttpService} from "../../services/app-http.service";

/* tslint:disable:no-unused-variable member-ordering */


@Injectable()
export class ListOfUsersApi {
    protected basePath = 'https://localhost/api';
    public defaultHeaders: Headers = new Headers();
    public configuration: Configuration = new Configuration();

    constructor(protected http: AppHttpService, @Optional()@Inject(BASE_PATH) basePath: string, @Optional() configuration: Configuration) {
        if (basePath) {
            this.basePath = basePath;
        }
        if (configuration) {
            this.configuration = configuration;
        }
    }

    /**
     * Get a list of Users
     * Provides a list of Users.
     * @param perPage Apply limit on users list. Applied by default.
     * @param page Apply offset on users list (for pagination purposes). Applied by default.
     * @param statuses Filter users by status. Example: NEW|DECLINED
     * @param roles Filter users by role. Example: ROLE_DRIVER|ROLE_ADMIN
     */
    public usersGet(perPage?: number, page?: number, statuses?: Array<string>, roles?: Array<string>, extraHttpRequestParams?: any): Observable<models.InlineResponse20010> {
        return this.usersGetWithHttpInfo(perPage, page, statuses, roles, extraHttpRequestParams)
            .map((response: Response) => {
                if (response.status === 204) {
                    return undefined;
                } else {
                    return response.json();
                }
            });
    }


    /**
     * Get a list of Users
     * Provides a list of Users.
     * @param perPage Apply limit on users list. Applied by default.
     * @param page Apply offset on users list (for pagination purposes). Applied by default.
     * @param statuses Filter users by status. Example: NEW|DECLINED
     * @param roles Filter users by role. Example: ROLE_DRIVER|ROLE_ADMIN
     */
    public usersGetWithHttpInfo(perPage?: number, page?: number, statuses?: Array<string>, roles?: Array<string>, extraHttpRequestParams?: any): Observable<Response> {
        const path = this.basePath + `/users`;

        let queryParameters = new URLSearchParams();
        let headers = new Headers(this.defaultHeaders.toJSON()); // https://github.com/angular/angular/issues/6845
        if (perPage !== undefined) {
                queryParameters.set('perPage', <any>perPage);
        }

        if (page !== undefined) {
                queryParameters.set('page', <any>page);
        }

        if (statuses) {
            queryParameters.set('statuses', statuses.join(COLLECTION_FORMATS['pipes']));
        }

        if (roles) {
            queryParameters.set('roles', roles.join(COLLECTION_FORMATS['pipes']));
        }

        // to determine the Content-Type header
        let consumes: string[] = [
            'application/json'
        ];

        // to determine the Accept header
        let produces: string[] = [
            'application/json'
        ];

        // authentication (OauthSecurity) required
        // oauth required
        if (this.configuration.accessToken) {
            let accessToken = typeof this.configuration.accessToken === 'function'
                ? this.configuration.accessToken()
                : this.configuration.accessToken;
            headers.set('Authorization', 'Bearer ' + accessToken);
        }

        let requestOptions: RequestOptionsArgs = new RequestOptions({
            method: RequestMethod.Get,
            headers: headers,
            search: queryParameters
        });

        // https://github.com/swagger-api/swagger-codegen/issues/4037
        if (extraHttpRequestParams) {
            requestOptions = (<any>Object).assign(requestOptions, extraHttpRequestParams);
        }

        return this.http.request(path, requestOptions);
    }

}
