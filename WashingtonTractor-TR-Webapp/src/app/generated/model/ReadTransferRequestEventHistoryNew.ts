/**
 * Tractor CRM API
 * Backend API for Tractor CRM. Serves web and native mobile applications.
 *
 * OpenAPI spec version: 1.1.0
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */

import * as models from './models';

export interface ReadTransferRequestEventHistoryNew {
    storeItem?: models.ReadStoreItem;

    storeReceiver?: models.ReadStore;

    /**
     * Status of Transfer request.
     */
    status?: ReadTransferRequestEventHistoryNew.StatusEnum;

    /**
     * -> Equipment Sale – Store A sells a piece of equipment that is located at Store B. Rental – Store A rents a machine that is located at Store B. Demo – A perspective customer from Store A wants to try out a machine located at Store B. Stock Leveling – Store A is out of a particular model while Store B has 6 of that model.
     */
    purpose?: ReadTransferRequestEventHistoryNew.PurposeEnum;

    shipmentTimestamp?: number;

    receiverUserName?: string;

}
export namespace ReadTransferRequestEventHistoryNew {
    export enum StatusEnum {
        PENDING = <any> 'PENDING',
        APPROVED = <any> 'APPROVED',
        DECLINED = <any> 'DECLINED',
        CANCELLED = <any> 'CANCELLED',
        INTRANSIT = <any> 'IN_TRANSIT',
        DELIVERED = <any> 'DELIVERED',
        COMPLETED = <any> 'COMPLETED'
    }
    export enum PurposeEnum {
        EQUIPMENTSALE = <any> 'EQUIPMENT_SALE',
        RENTAL = <any> 'RENTAL',
        DEMO = <any> 'DEMO',
        STOCKLEVELING = <any> 'STOCK_LEVELING'
    }
}
