/**
 * Tractor CRM API
 * Backend API for Tractor CRM. Serves web and native mobile applications.
 *
 * OpenAPI spec version: 1.1.0
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */

import * as models from './models';

export interface ReadUser {
    /**
     * The unique id of user.
     */
    id?: number;

    /**
     * User email address.
     */
    email?: string;

    /**
     * The first name of user.
     */
    firstName?: string;

    /**
     * The last name of user.
     */
    lastName?: string;

    /**
     * User role.
     */
    role?: ReadUser.RoleEnum;

    /**
     * Stores.
     */
    stores?: Array<models.ReadStore>;

    /**
     * The status of user. When user created - set as NEW.
     */
    status?: ReadUser.StatusEnum;

}
export namespace ReadUser {
    export enum RoleEnum {
        ADMIN = <any> 'ROLE_ADMIN',
        MANAGERSALE = <any> 'ROLE_MANAGER_SALE',
        MANAGERSTORE = <any> 'ROLE_MANAGER_STORE',
        DRIVER = <any> 'ROLE_DRIVER',
        TRANSFERCOORDINATOR = <any> 'ROLE_TRANSFER_COORDINATOR'
    }
    export enum StatusEnum {
        NEW = <any> 'NEW',
        REGISTERED = <any> 'REGISTERED',
        DECLINED = <any> 'DECLINED',
        DELETED = <any> 'DELETED'
    }
}
