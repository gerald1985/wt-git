import { OpaqueToken } from '@angular/core';
import {OAuthConfig} from "../oauth-config";

export const BASE_PATH = OAuthConfig.url;
export const COLLECTION_FORMATS = {
    'csv': ',',
    'tsv': '   ',
    'ssv': ' ',
    'pipes': '|'
}
