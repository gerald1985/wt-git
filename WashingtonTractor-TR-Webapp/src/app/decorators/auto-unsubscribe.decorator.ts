export function AutoUnsubscribe( params = {exclude: []} ) {

	return function (constructor) {
		const original = constructor.prototype.ngOnDestroy;

		constructor.prototype.ngOnDestroy = function () {
			Object.keys(this).forEach(propKey => {
				if (!this.hasOwnProperty(propKey)) return;
				const property = this[propKey];
				if (params.exclude.includes(propKey)) return;
				if (!property || typeof property.unsubscribe !== 'function') return;
				property.unsubscribe();
			});
			if (original && typeof original === 'function') {
				original.apply(this, arguments);
			}
		};
	};

}
