import { ReadStore } from '../generated/model/ReadStore';
import { ReadUser } from '../generated/model/ReadUser';
import { Subject } from 'rxjs/Subject';
import { Subscription } from 'rxjs/Subscription';
import { Observable } from 'rxjs/Observable';
import {ReadTransferRequest} from '../generated/model/ReadTransferRequest';

type DeleteSubscriptionItem = ReadStore | ReadUser | ReadTransferRequest;

export class DeleteSubscriptionStore<T extends DeleteSubscriptionItem> {
	private items: Array<{id: number; subscription: Subscription}> = [];
	private itemDeleted$ = new Subject<T>();

	onDelete() {
		return this.itemDeleted$;
	}

	isSubscribed(item: T) {
		for (let index = 0, len = this.items.length; index < len; index++) {
			if (this.items[index].id === item.id) return true;
		}
		return false;
	}

	subscribe(item: T) {
		this.items.push({
			id: item.id,
			subscription: Observable.of(item)
			.delay(8000)
			.subscribe(itemToDelete => {
				this.itemDeleted$.next(itemToDelete);
				this.unsubscribe(item);
			})
		});
	}

	unsubscribe(item: T) {
		let id = item.id;
		for (let index = 0, len = this.items.length; index < len; index++) {
			if (this.items[index].id !== item.id) continue;
			this.items[index].subscription.unsubscribe();
			this.items = [...this.items.slice(0, index), ...this.items.slice(index + 1)];
			break;
		}
	}

	cancelAll(complete = true) {
		this.items.forEach(item => {
			item.subscription.unsubscribe();
		});
		this.items = [];
		if (complete) this.itemDeleted$.complete();
	}

	assingRemovedStatus(item: T): T {
		return Object.assign({}, item, {removed: this.isSubscribed(item)});
	}
}
