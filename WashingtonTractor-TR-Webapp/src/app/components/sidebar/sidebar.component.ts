import { Component, OnInit, ViewEncapsulation, ChangeDetectionStrategy, ChangeDetectorRef, OnDestroy } from '@angular/core';
import { RequestService } from '../../services/request.service';
import { Observable } from 'rxjs/Observable';
import { AuthService } from '../../services/auth.service';
import { Subscription } from 'rxjs/Subscription';
import { AutoUnsubscribe } from '../../decorators/auto-unsubscribe.decorator';

@Component({
	selector: 'app-sidebar',
	templateUrl: './sidebar.component.html',
	styleUrls: ['./sidebar.component.scss'],
	encapsulation: ViewEncapsulation.None,
	changeDetection: ChangeDetectionStrategy.Default
})

@AutoUnsubscribe()
export class SidebarComponent implements OnInit {

	pendingCount: number;

	tokenSubscription: Subscription;
	pedingSubscription: Subscription;

	isAdmin = this.auth.isAdmin();

	constructor(private requests: RequestService, private cd: ChangeDetectorRef, private auth: AuthService) { }

	ngOnInit() {
		this.pedingSubscription = this.requests.onPendingCountChange().subscribe(count => {
			this.pendingCount = count;
		});
		this.tokenSubscription = this.auth.onTokenChange().subscribe(_ => {
			this.isAdmin = this.auth.isAdmin();
			if (this.auth.isManagerStore()) {
				this.pedingSubscription = this.requests.onPendingCountChange().subscribe(count => {
					this.pendingCount = count;
				});
			} else if (this.pedingSubscription) {
				this.pedingSubscription.unsubscribe();
			}
		});
	}
}
