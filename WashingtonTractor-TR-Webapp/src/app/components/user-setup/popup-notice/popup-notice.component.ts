import { Component, OnInit, ViewEncapsulation, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'app-popup-notice',
  templateUrl: './popup-notice.component.html',
  styleUrls: ['./popup-notice.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PopupNoticeComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
