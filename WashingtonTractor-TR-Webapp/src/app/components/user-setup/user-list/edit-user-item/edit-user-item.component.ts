import { Component, OnInit, ViewEncapsulation, ChangeDetectionStrategy, Inject } from '@angular/core';
import { MdDialogRef } from '@angular/material';
import { UpdateUser } from '../../../../generated/model/UpdateUser';
import { WriteUser } from '../../../../generated/model/WriteUser';
import { ReadUser } from '../../../../generated/model/ReadUser';
import { USER_ROLES } from '../../../../models/user-level';
import { ListOfStoresApi } from '../../../../generated/api/ListOfStoresApi';
import { ReadStore } from '../../../../generated/model/ReadStore';
import { UserApi } from '../../../../generated/api/UserApi';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { CommonError } from '../../../../generated/model/CommonError';
import { MessageService } from '../../../../services/message.service';
import * as apiUsers from '../../../../generated/model/ReadUser';

@Component({
	selector: 'app-edit-user-item',
	templateUrl: './edit-user-item.component.html',
	styleUrls: ['./edit-user-item.component.scss'],
	encapsulation: ViewEncapsulation.None,
	changeDetection: ChangeDetectionStrategy.Default
})

export class EditUserItemComponent implements OnInit {

	levels = USER_ROLES;
	locations: Array<ReadStore> = [];

	updateUserForm: FormGroup;

	errors: any = {};

	showLocation = false;

	constructor(
		private dialogRef: MdDialogRef<EditUserItemComponent>,
		@Inject('ListOfStoresApi') public listStoresApi: ListOfStoresApi,
		private message: MessageService,
		private userApi: UserApi
	) {}

	ngOnInit() {
		this.listStoresApi.storesGet().subscribe(response => {
			this.locations = response.data;
		});
	}

	setUser(readUser: ReadUser) {
		this.updateUserForm = new FormGroup({
			id: new FormControl(readUser.id, Validators.required),
			email: new FormControl(readUser.email || '', [Validators.required, Validators.email]),
			firstName: new FormControl(readUser.firstName || '', Validators.required),
			lastName: new FormControl(readUser.lastName || '', Validators.required),
			role: new FormControl(readUser.role),
			storeId: new FormControl(readUser.stores && readUser.stores[0] ? readUser.stores[0].id : null),
		});
		this.showLocation = readUser.role === apiUsers.ReadUser.RoleEnum.MANAGERSALE || readUser.role === apiUsers.ReadUser.RoleEnum.MANAGERSTORE;
		this.updateUserForm.get('role').valueChanges.subscribe(value => {
			if (value === apiUsers.ReadUser.RoleEnum.MANAGERSALE || value === apiUsers.ReadUser.RoleEnum.MANAGERSTORE) {
				this.showLocation = true;
			} else {
				this.showLocation = false;
			}
		});
	}

	deleteUser() {
			this.dialogRef.close('delete');
	}

	updateUser({value, valid}: {value: any, valid: boolean}) {
		Object.keys(this.updateUserForm.controls).forEach(key => {
			this.updateUserForm.get(key).markAsDirty();
		});
		if (!valid) return;
		this.errors = {};
		if (!this.showLocation) {
			value.storeId = null;
		}
		this.userApi.userPut(value).subscribe(response => {
			this.dialogRef.close(response.data);
		}, errorResponse => {
			let error = <CommonError>errorResponse.json();
			if (!error.errorMessage.attributes) {
				this.message.showMessage(error.errorMessage.message);
				return;
			};
			error.errorMessage.attributes.forEach(fieldError => {
				let formControl = this.updateUserForm.get(fieldError.field);
				if (!formControl) {
					fieldError.errors.forEach(message => {
						this.message.showMessage(message);
					});
					return;
				}
				this.errors[fieldError.field] = fieldError.errors;
				formControl.setErrors({'serverError': true});
			});
		});
	}

}
