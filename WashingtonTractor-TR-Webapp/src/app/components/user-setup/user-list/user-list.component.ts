import {
	Component, ViewEncapsulation, ChangeDetectionStrategy,
	OnInit, ViewChild, ElementRef, AfterViewInit, Input, Output, EventEmitter
} from '@angular/core';
import { MdDialog } from '@angular/material';
import { EditUserItemComponent } from './edit-user-item/edit-user-item.component';
import { UserApi } from '../../../generated/api/UserApi';
import { ListOfUsersApi } from '../../../generated/api/ListOfUsersApi';
import { InlineResponse2006 } from '../../../generated/model/InlineResponse2006';
import { ReadUser } from '../../../generated/model/ReadUser';
import * as apiUsers from '../../../generated/model/ReadUser';
import { USER_LEVEL, USER_ROLE } from '../../../models/user-level';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';
import { Subject } from 'rxjs/Subject';

@Component({
	selector: 'app-user-list',
	templateUrl: './user-list.component.html',
	styleUrls: ['./user-list.component.scss'],
	encapsulation: ViewEncapsulation.None,
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class UserListComponent implements AfterViewInit {

	position = 'below';
	USER_LEVEL = USER_LEVEL;
	USER_ROLE = USER_ROLE;
	editUser: ReadUser;

	@ViewChild('scrollTable') scrollTable: ElementRef;

	@Input() users: Array<ReadUser> = [];
	@Input() loadMore$: Subject<any>;
	@Output() onEdit = new EventEmitter<ReadUser>();
	@Output() onDelete = new EventEmitter<ReadUser>();
	@Output() onRestore = new EventEmitter<ReadUser>();

	constructor(public dialog: MdDialog, public userApi: UserApi, public userListApi: ListOfUsersApi) {}

	setEditUser(user: ReadUser) {
		this.editUser = user;
	}

	deleteUser() {
		if (!this.editUser) return;
		this.onDelete.emit(this.editUser);
		this.editUser = null;
	}

	restoreUser(user: ReadUser) {
		this.onRestore.emit(user);
	}

	editUserItem() {
		if (!this.editUser) return;
		let dialogRef = this.dialog.open(EditUserItemComponent, {
			width: '840px',
		});
		dialogRef.componentInstance.setUser(this.editUser);
		dialogRef.afterClosed().subscribe(result => {
			if (result === 'delete') {
				this.deleteUser();
				return;
			}
			this.editUser = null;
			this.onEdit.emit(result);
		});
	}

	ngAfterViewInit() {
		const table: HTMLElement = this.scrollTable.nativeElement;
		const offset = 50;
		const scrollSubscription = Observable.fromEvent(window, 'scroll').debounceTime(200);
		const resize$ = Observable.fromEvent(window, 'resize').debounceTime(200);
		const tableInitalHeight = table.clientHeight;
		let prevScrollTop = 0;
		Observable.merge(scrollSubscription, resize$).filter(() => {
			// if scroll to the top, do nothing
			if (prevScrollTop > window.scrollY) {
				return false;
			}
			prevScrollTop = window.scrollY;
			const windowBottomBorder = window.innerHeight + window.scrollY;
			const tableBottomBorder = table.clientHeight + table.offsetTop;
			return windowBottomBorder + offset > tableBottomBorder;
		}).subscribe(() => { this.loadMore$.next(); });
	}

}
