import {Component, OnInit, OnDestroy, ViewEncapsulation} from '@angular/core';
import { ReadUser } from '../../generated/model/ReadUser';
import { ListOfUsersApi } from '../../generated/api/ListOfUsersApi';
import { UserApi } from '../../generated/api/UserApi';
import { Subject } from 'rxjs/Subject';
import { Subscription } from 'rxjs/Subscription';
import { DeleteSubscriptionStore } from '../../classes/delete-subscription-store';
import { MessageService } from '../../services/message.service';
import { CommonError } from '../../generated/model/CommonError';
import {AuthService} from '../../services/auth.service';

@Component({
	selector: 'app-user-setup',
	templateUrl: './user-setup.component.html',
	styleUrls: ['./user-setup.component.scss'],
	encapsulation: ViewEncapsulation.None,
})
export class UserSetupComponent implements OnInit, OnDestroy {

	newUsers: Array<ReadUser> = [];
	registeredUsers: Array<ReadUser> = [];
	page = 1;
	itemsPerPage = 10;
	isLoading = false;
	scrollSubscription: Subscription;

	private usersToDelete = new DeleteSubscriptionStore<ReadUser>();

	loadMore$ = new Subject<any>();

	constructor(private userApi: UserApi, private userListApi: ListOfUsersApi, private message: MessageService, private auth: AuthService) { }

	changeUserStatus(user: ReadUser) {
		this.registeredUsers = this.registeredUsers.map(registeredUser => {
			if (registeredUser.id !== user.id ) return registeredUser;
			return this.usersToDelete.assingRemovedStatus(registeredUser);
		});
	}

	handleDelete(user: ReadUser) {
		this.usersToDelete.subscribe(user);
		this.changeUserStatus(user);
	}

	handleRestore(user: ReadUser) {
		this.usersToDelete.unsubscribe(user);
		this.changeUserStatus(user);
	}

	onUserEdit(user: ReadUser) {
		if (!user) return;
		let index = -1;
		for (let i = 0, len = this.registeredUsers.length; i < len; i++) {
			if (this.registeredUsers[i].id !== user.id) continue;
			index = i;
		}
		if (index === -1) return;
		this.registeredUsers = [...this.registeredUsers.slice(0, index), user, ...this.registeredUsers.slice(index + 1)];
		if (user.id === this.auth.getUserId()) {
			this.auth.setUser(user);
		}
	}

	loadRegisteredUsers(limit?: number) {
		if (this.isLoading) return;
		this.isLoading = true;
		let page = this.page;
		let itemsPerPage = this.itemsPerPage;
		if (limit) {
			this.subscribeToScroll();
			page = 1;
			itemsPerPage = Math.ceil((limit + 1) / this.itemsPerPage) * this.itemsPerPage;
		}
		this.userListApi.usersGet(itemsPerPage, page, ['REGISTERED']).subscribe(response => {
			this.page++;
			if (limit) {
				this.registeredUsers = response.data.map(user => this.usersToDelete.assingRemovedStatus(user));
			} else {
				this.registeredUsers = [...this.registeredUsers, ...response.data.map(user => this.usersToDelete.assingRemovedStatus(user))];
			}
			// If the last page, stop listening to scroll;
			if ((response.data.length === 0 || response.data.length < this.itemsPerPage) && this.scrollSubscription) {
				this.scrollSubscription.unsubscribe();
			}
			this.isLoading = false;
			if (page === 1) {
				this.loadRegisteredUsers();
			}
		}, error => {
			this.isLoading = false;
		});
	}

	loadNewUsers() {
		this.userListApi.usersGet(100, 1, ['NEW']).subscribe(response => {
			this.newUsers = response.data;
		}, error => {

		});
	}

	handleApprove(user: ReadUser) {
		this.userApi.userIdApprovePut(user.id).subscribe(response => {
			this.loadNewUsers();
			this.loadRegisteredUsers(this.registeredUsers.length);
		}, errorResponse  => {
			let error = <CommonError>errorResponse.json();
			this.message.showMessage(error.errorMessage.message);
		});
	}

	handleDecline(data: any) {
		this.userApi.userIdDeclinePut(data.user.id, {note: data.note}).subscribe(response => {
				this.loadNewUsers();
			}, errorResponse => {
			let error = <CommonError>errorResponse.json();
			this.message.showMessage(error.errorMessage.message);
			});
	}

	subscribeToScroll() {
		this.scrollSubscription = this.loadMore$.subscribe(() => {
			this.loadRegisteredUsers();
		});
	}

	ngOnInit() {
		this.subscribeToScroll();
		this.loadNewUsers();
		this.loadRegisteredUsers();
		this.usersToDelete.onDelete().subscribe(user => {
			this.userApi.userIdDelete(user.id).subscribe(response => {
				this.loadRegisteredUsers(this.registeredUsers.length);
			}, errorResponse => {
				try {
					let error = <CommonError>errorResponse.json();
					this.message.showMessage(error.errorMessage.message);
				} catch (e) {
					this.message.showMessage(`Unknown error while deleting user ${user.firstName} ${user.lastName}`);
				} finally {
					this.changeUserStatus(user);
				}
			});
		});
	}

	ngOnDestroy() {
		this.usersToDelete.cancelAll();
	}

}
