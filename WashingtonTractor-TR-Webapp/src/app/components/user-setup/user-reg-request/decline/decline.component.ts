import {MdDialogRef} from '@angular/material';
import {Component, ViewEncapsulation, ChangeDetectionStrategy} from '@angular/core';
import { ReadUser } from '../../../../generated/model/ReadUser';

@Component({
	selector: 'app-decline',
	templateUrl: './decline.component.html',
	styleUrls: ['./decline.component.scss'],
	encapsulation: ViewEncapsulation.None,
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class DeclineComponent {
	name = 'John';

	note = '';

	constructor(public dialogRef: MdDialogRef<DeclineComponent>) { }

	submit() {
		this.dialogRef.close(this.note);
	}
}
