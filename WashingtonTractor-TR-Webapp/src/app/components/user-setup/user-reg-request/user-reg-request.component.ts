import { Component, ViewEncapsulation, ChangeDetectionStrategy, Input, Output, EventEmitter } from '@angular/core';
import { MdDialog} from '@angular/material';
import { DeclineComponent } from './decline/decline.component';
import { ReadUser } from '../../../generated/model/ReadUser';
import { USER_LEVEL, USER_ROLE } from '../../../models/user-level';

@Component({
	selector: 'app-user-reg-request',
	templateUrl: './user-reg-request.component.html',
	styleUrls: ['./user-reg-request.component.scss'],
	encapsulation: ViewEncapsulation.None,
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class UserRegRequestComponent {
	@Input() users: Array<ReadUser> = [];
	@Output() onApprove = new EventEmitter<ReadUser>();
	@Output() onDecline = new EventEmitter<any>();
	USER_LEVEL = USER_LEVEL;
	USER_ROLE = USER_ROLE;

	constructor(public dialog: MdDialog) {}

	public approve(user: ReadUser) {
		this.onApprove.emit(user);
	}

	public showDecline(user: ReadUser) {
		let dialogRef = this.dialog.open(DeclineComponent, {
			width: '500px',
		});

		dialogRef.afterClosed().subscribe(result => {
			if (result === undefined) return;
			this.onDecline.emit({user: user, note: result});
		});
	}
}
