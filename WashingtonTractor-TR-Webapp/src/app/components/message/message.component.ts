import { Component, OnInit, ViewEncapsulation, ChangeDetectionStrategy, AfterViewInit } from '@angular/core';
import { MessageService } from '../../services/message.service';

@Component({
	selector: 'app-message',
	templateUrl: './message.component.html',
	styleUrls: ['./message.component.scss'],
	encapsulation: ViewEncapsulation.None,
	changeDetection: ChangeDetectionStrategy.Default
})
export class MessageComponent implements OnInit, AfterViewInit {

	messages: Array<string> = [];

	constructor(private messageService: MessageService) { }

	ngOnInit() {

	}

	ngAfterViewInit() {
		this.messageService.messages.subscribe(messages => {
			this.messages = messages;
		});
	}

}
