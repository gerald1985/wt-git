import { Observable } from 'rxjs/Observable';
import { Component, ViewEncapsulation, Input, EventEmitter, Output, ElementRef, ViewChild, AfterViewInit } from '@angular/core';
import { MdDialog } from '@angular/material';
import { DeclineRequestComponent } from '../decline-request/decline-request.component';
import { ReadTransferRequest } from '../../../generated/model/ReadTransferRequest';
import {RequestService} from '../../../services/request.service';
import {AuthService} from '../../../services/auth.service';
import * as requestApiAll from '../../../generated/model/ReadTransferRequest';


@Component({
	selector: 'app-request-list',
	templateUrl: './request-list.component.html',
	styleUrls: ['./request-list.component.scss'],
	encapsulation: ViewEncapsulation.None,
})
export class RequestListComponent implements AfterViewInit {

	@Input() requests: Array<ReadTransferRequest> = [];
	@Output() onEditClick = new EventEmitter<ReadTransferRequest>();

	@ViewChild('scrollTable') scrollTable: ElementRef;

	currentItem: ReadTransferRequest;

	isAdmin = this.authService.isAdmin();
	isTransferCoordinator = this.authService.isTransferCoordinator();
	cancelled = requestApiAll.ReadTransferRequest.StatusEnum.CANCELLED;

	setCurrentItem(item) {
		this.currentItem = item;
	}

	requestSidebar( passed ) {
		this.requestsService.openSidebar(passed.id);
	}

	editItem() {
		if (!this.currentItem) return;
		this.onEditClick.emit(this.currentItem);
		this.currentItem = null;
	}

	cancelItem() {
		if (!this.currentItem) return;
		this.requestsService.handleCancel(this.currentItem);
	}

	constructor(private dialog: MdDialog,
							private authService: AuthService,
							private requestsService: RequestService) {
	}

	private declineTransferRequest() {
		let dialogRef = this.dialog.open(DeclineRequestComponent, {
			width: '560px',
		});
	}

	ngAfterViewInit() {
		const table: HTMLElement = this.scrollTable.nativeElement;
		const offset = 50;
		const scrollSubscription = Observable.fromEvent(window, 'scroll').debounceTime(200);
		const resize$ = Observable.fromEvent(window, 'resize').debounceTime(200);
		const tableInitalHeight = table.clientHeight;
		let prevScrollTop = 0;
		let prevItemsCount = 0;
		let allLoaded = false;
		Observable.merge(scrollSubscription, resize$).filter(() => {
			// if scroll to the top, do nothing
			if (prevScrollTop > window.scrollY || allLoaded) {
				return false;
			}
			prevScrollTop = window.scrollY;
			const windowBottomBorder = window.innerHeight + window.scrollY;
			const tableBottomBorder = table.clientHeight + table.offsetTop;
			return windowBottomBorder + offset > tableBottomBorder;
		}).subscribe(() => {
			this.requestsService.loadNextPage();
		});

		this.requestsService.requests$.subscribe(items => {
			if (items.length === 0) {
				prevScrollTop = 0;
				prevItemsCount = 0;
				allLoaded = false;
				return;
			}
			allLoaded = items.length <= prevItemsCount;
			prevItemsCount = items.length;
		});
	}
}
