import {Component, OnInit, ViewEncapsulation, ChangeDetectionStrategy, Input} from '@angular/core';
import { ReadUser } from '../../../generated/model/ReadUser';

@Component({
	selector: 'app-username',
	templateUrl: 'username.component.html',
	styleUrls: ['username.component.scss'],
	encapsulation: ViewEncapsulation.None,
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class UsernameComponent implements OnInit {

	@Input() user: ReadUser;

	initials: string;

	constructor() {
	}

	private updateInitials() {
		let fullName = this.user.firstName + ' ' + this.user.lastName;
		this.initials = fullName.split(' ').map(e => e.substr(0, 1).toUpperCase()).join('');
	}

	ngOnInit() {
		this.updateInitials();
	}

}
