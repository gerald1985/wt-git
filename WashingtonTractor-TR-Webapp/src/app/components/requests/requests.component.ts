import { Component, OnInit, OnDestroy } from '@angular/core';
import { MdDialog } from '@angular/material';
import { NewRequestComponent } from './new-request/new-request.component';
import { ListOfTransferRequestsApi } from '../../generated/api/ListOfTransferRequestsApi';
import { ReadTransferRequest } from '../../generated/model/ReadTransferRequest';
import { TransferRequestApi } from '../../generated/api/TransferRequestApi';
import { RequestService } from '../../services/request.service';
import { Subscription } from 'rxjs/Subscription';
import { RequestsGetParams } from './requset-get-params.interface';
import { ReadTransferRequestTemplate } from './read-transfer-requset-template.interface';
import { Observable } from 'rxjs/Observable';
import { FormControl } from '@angular/forms';
import { AutoUnsubscribe } from '../../decorators/auto-unsubscribe.decorator';
import {Subject} from "rxjs/Subject";

@Component({
	selector: 'app-requests',
	templateUrl: './requests.component.html',
	styleUrls: ['./requests.component.scss']
})

export class RequestsComponent implements OnDestroy {

	requests$ = this.requestService.requests$;
	params$ = this.requestService.params$;
	clearSearchText$ = new Subject();
	constructor(
		private dialog: MdDialog,
		private listRequestApi: ListOfTransferRequestsApi,
		private requestApi: TransferRequestApi,
		private requestService: RequestService
	) {}

	updateGetParams(searchObject) {
		this.requestService.updateGetParams({
			page: 1,
			itemStockNumber: searchObject.type === 'stock' ? searchObject.value : undefined,
			itemModelName: searchObject.type === 'model' ? searchObject.value : undefined
		});
	}

	ngOnDestroy() {
		this.requestService.cancelAllToDelete();
	}

	handleNewRequest(item?: ReadTransferRequest) {
		let dialogRef = this.dialog.open(NewRequestComponent, {
			width: '550px',
			disableClose: true
		});
		dialogRef.componentInstance.setCurrentRequest(item);

		dialogRef.afterClosed().subscribe(result => {
			// console.log(result);
		});
	}
}
