import { ReadTransferRequest } from '../../generated/model/ReadTransferRequest';
export interface ReadTransferRequestTemplate extends ReadTransferRequest {
	shipmentDate?: Date;
	requestDate?: Date;
	removed?: boolean;
}
