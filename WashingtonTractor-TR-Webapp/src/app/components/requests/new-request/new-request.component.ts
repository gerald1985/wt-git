import { Component, ViewEncapsulation, ChangeDetectionStrategy, OnInit, Inject } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import 'rxjs/add/operator/startWith';
import { WriteTransferRequest } from '../../../generated/model/WriteTransferRequest';
import { ReadStoreItem } from '../../../generated/model/ReadStoreItem';
import { PURPOSES } from '../../../models/transfer-purpose';
import { ReadTransferRequest } from '../../../generated/model/ReadTransferRequest';
import { ReadStore } from '../../../generated/model/ReadStore';
import { ListOfStoresApi } from '../../../generated/api/ListOfStoresApi';
import { TransferRequestApi } from '../../../generated/api/TransferRequestApi';
import { CommonError } from '../../../generated/model/CommonError';
import { MdDialogRef } from '@angular/material';
import { AuthService } from '../../../services/auth.service';
import { UpdateTransferRequest } from '../../../generated/model/UpdateTransferRequest';
import { Observable } from 'rxjs/Observable';
import { RequestService } from '../../../services/request.service';
import * as apiStores from '../../../generated/model/ReadStore';
import * as apiRequests from '../../../generated/model/ReadTransferRequest';
import { ListOfItemsApi } from '../../../generated/api/ListOfItemsApi';
import { MessageService } from '../../../services/message.service';

@Component({
	selector: 'app-new-request',
	templateUrl: './new-request.component.html',
	styleUrls: ['./new-request.component.scss'],
	encapsulation: ViewEncapsulation.None,
	changeDetection: ChangeDetectionStrategy.Default
})
export class NewRequestComponent implements OnInit {

	locations: Array<ReadStore>;

	purposes = PURPOSES;

	items: ReadStoreItem = [];
	selectedItem: ReadStoreItem;

	requestForm: FormGroup;
	inputType: FormControl;

	dates: Array<Date> = [];

	isAdmin = this.auth.isAdmin();
	isTransferCoordinator = this.auth.isTransferCoordinator();
        notSalesPerson = !this.auth.isManagerSale();

	private prevSearchText: string;
	errors: any = {};

	title = 'Add a transfer request';
	resubmit: boolean;

	constructor(
		private dialog: MdDialogRef<NewRequestComponent>,
		private itemsApi: ListOfItemsApi,
		@Inject('ListOfStoresApi') private listStoresApi: ListOfStoresApi,
		private requestApi: TransferRequestApi,
		private auth: AuthService,
		private message: MessageService,
		private requestService: RequestService
	) {
		this.inputType = this.notSalesPerson ? new FormControl('stock') : new FormControl('model');
	}

	setCurrentRequest(request?: ReadTransferRequest, resubmit = false, prefilledItem?: ReadStoreItem) {
		this.resubmit = resubmit;
		let forumGroupFields = {
			purpose: new FormControl({value: request ? request.purpose.toString() : null, disabled: resubmit}, Validators.required),
			lastNote: new FormControl(null),
			receiverUserName: new FormControl({value: request ? request.receiverUserName : null, disabled: resubmit})
		};

		forumGroupFields['storeItemId'] = prefilledItem ? new FormControl(prefilledItem.id, Validators.required) :
			new FormControl(request ? request.storeItem.id : null, Validators.required);
		this.selectedItem = prefilledItem;

		if ((this.isAdmin || this.isTransferCoordinator) && !resubmit) {
			forumGroupFields['shipmentTimestamp'] = new FormControl(request && request.shipmentTimestamp ? request.shipmentTimestamp : (Date.now() / 1000 | 0));
			forumGroupFields['storeReceiverId'] = new FormControl(request ? request.storeReceiver.id : null, Validators.required);
		}
		if (request) {
			this.title = resubmit ? 'Resubmit TR' : 'Update a transfer request';
			this.dates = request.shipmentTimestamp ? [new Date(request.shipmentTimestamp * 1000)] : [new Date()];
			this.selectedItem = request.storeItem;
			forumGroupFields['id'] = new FormControl(request.id);
		} else {
			forumGroupFields['getEmailUpdates'] = new FormControl(true);
		}
		this.requestForm = new FormGroup(forumGroupFields);
	}

	ngOnInit() {
		this.getItems();
		if (this.isAdmin || this.isTransferCoordinator) {
			this.getLocations();
		}
		this.inputType.valueChanges.subscribe(_ => {
			this.getItems(this.prevSearchText);
		});
	}

	handleItemSeach(searchText?: string) {
		this.prevSearchText = searchText;
		this.getItems(searchText);
	}

	handleItemSelect(item: ReadStoreItem) {
		this.selectedItem = item;
		this.requestForm.get('storeItemId').setValue(item.id);
	}

	handleDateSelect(dates: Array<Date>) {
		this.dates = dates;
		this.requestForm.get('shipmentTimestamp').setValue(dates[0].getTime() / 1000);
	}

	getItems(searchText?: string) {
		let modelName;
		let stockNumber;
		switch (this.inputType.value) {
			case 'stock':
				stockNumber = searchText;
				break;
			case 'model':
				modelName = searchText;
				break;
			default:
				return;
		}
		let userStoreIds = this.auth.getUserStoreIds();
		this.itemsApi.itemsStockGet(modelName, stockNumber, [
			apiRequests.ReadTransferRequest.StatusEnum.PENDING.toString(),
			apiRequests.ReadTransferRequest.StatusEnum.APPROVED.toString() ],
			(!userStoreIds.length) ? undefined : userStoreIds, 100, 1).subscribe(response => {
			this.items = response.data;
		});
	}

	saveRequest({value, valid}: {value: any, valid: boolean}) {
		Object.keys(this.requestForm.controls).forEach(key => {
			this.requestForm.get(key).markAsDirty();
		});
		if (!valid) return;
		this.errors = {};
		let saveReqeust$: Observable<any>;
		if (value.id) {
			saveReqeust$ = this.resubmit ? this.requestService.resubmit(value.id, value) : this.requestService.updateTransferRequest(value);
		} else {
			saveReqeust$ = this.requestService.addTransferRequest(value);
		}
		saveReqeust$.subscribe(response => {
			this.dialog.close(response.data);
		}, errorResponse => {
			let error = <CommonError>errorResponse.json();
			if (!error.errorMessage.attributes) {
				this.message.showMessage(error.errorMessage.message);
				return;
			};
			error.errorMessage.attributes.forEach(fieldError => {
				let formControl = this.requestForm.get(fieldError.field);
				if (!formControl) {
					fieldError.errors.forEach(message => {
						this.message.showMessage(message);
					});
					return;
				}
				this.errors[fieldError.field] = fieldError.errors;
				formControl.setErrors({'serverError': true});
			});
		});
	}

	getLocations() {
		this.listStoresApi.storesGet().subscribe(response => {
			this.locations = response.data.filter(location =>
				(!this.selectedItem || this.selectedItem.store.id !== location.id) && location.status === apiStores.ReadStore.StatusEnum.ACTIVE);
		});
	}

}

type RequestForm = UpdateTransferRequest | WriteTransferRequest;
