import {
	Component, OnInit, ViewEncapsulation, ChangeDetectionStrategy, Input, OnChanges,
	SimpleChanges, EventEmitter, Output, ViewChild, ElementRef, AfterViewInit, ChangeDetectorRef
} from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ReadTransferRequest } from '../../../generated/model/ReadTransferRequest';
import { RequestService } from '../../../services/request.service';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';
import { AutoUnsubscribe } from '../../../decorators/auto-unsubscribe.decorator';
import { MessageService } from '../../../services/message.service';
import { CommonError } from '../../../generated/model/CommonError';

@Component({
	selector: 'app-request-new-comment',
	templateUrl: 'request-new-comment.component.html',
	styleUrls: ['request-new-comment.component.scss'],
	encapsulation: ViewEncapsulation.None,
	changeDetection: ChangeDetectionStrategy.OnPush
})

@AutoUnsubscribe()
export class RequestNewCommentComponent implements OnInit, AfterViewInit {

	@Input() currentListItem: ReadTransferRequest;
	@Output() onPostNote = new EventEmitter<any>();

	@ViewChild('noteInput') noteInput: ElementRef;

	inputBlurSubscription: Subscription;

	isButtonShown = false;

	noteForm = new FormGroup({
		lastNote: new FormControl('', Validators.required),
	});

	constructor(private requestService: RequestService, private cd: ChangeDetectorRef, private message: MessageService) {
	}

	ngOnInit() {}

	ngAfterViewInit() {
		this.inputBlurSubscription = Observable.fromEvent(this.noteInput.nativeElement, 'blur').debounceTime(200).subscribe(_ => {
			this.isButtonShown = false;
			this.cd.detectChanges();
		});
	}

	sendNote({value, valid}: {value: any, valid: boolean}) {
		if (!valid) return;
		this.requestService.sendNote(this.currentListItem.id, value).subscribe(response => {
			this.onPostNote.emit(response['data']);
			this.noteForm.reset({lastNote: ''});
		}, errorResponse => {
			let error = <CommonError>errorResponse.json();
			this.message.showMessage(error.errorMessage.message);
		});
	}
	focusTextarea() {
		this.isButtonShown = true;
	}
}
