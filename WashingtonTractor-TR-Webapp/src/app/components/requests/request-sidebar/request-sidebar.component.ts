import {
	Component, ViewEncapsulation, ChangeDetectionStrategy, EventEmitter, Input, Output,
	OnInit
} from '@angular/core';
import { RequestService } from '../../../services/request.service';
import { Router, ActivatedRoute } from '@angular/router';
import { ReadTransferRequest } from '../../../generated/model/ReadTransferRequest';
import { TransferRequestApi } from '../../../generated/api/TransferRequestApi';
import { MessageService } from '../../../services/message.service';
import { CommonError } from '../../../generated/model/CommonError';
import {ReadRequestEvent} from '../../../generated/model/ReadRequestEvent';
import {Observable} from 'rxjs/Observable';

@Component({
	selector: 'app-request-sidebar',
	templateUrl: './request-sidebar.component.html',
	styleUrls: ['./request-sidebar.component.scss'],
	encapsulation: ViewEncapsulation.None,
	changeDetection: ChangeDetectionStrategy.Default
})
export class RequestSidebarComponent implements OnInit {
	isOpenedAllInfo: boolean;
	currentListItem: ReadTransferRequest = null;

	constructor(
		private requestService: RequestService,
		private router: Router,
		private route: ActivatedRoute,
		private trApi: TransferRequestApi,
		private message: MessageService
	) {}

	closeSidebar() {
		this.isOpenedAllInfo = false;
		this.router.navigate(['/requests']);
	}

	addNote(note: ReadRequestEvent) {
		this.currentListItem['events'].push(note);
	}
	showAllInfoTrigger(action: boolean) {
		this.isOpenedAllInfo = !this.isOpenedAllInfo;
	}

	ngOnInit(): void {
		this.route.params.subscribe(params => {
			if (!params.id) {
				this.currentListItem = null;
				return;
			}
			Observable.forkJoin(this.trApi.transferRequestIdGet(params.id), this.trApi.transferRequestIdEventsGet(params.id)).subscribe(response => {
				this.currentListItem = response[0]['data'];
				Object.assign(this.currentListItem, {
					shipmentDate: new Date(this.currentListItem.shipmentTimestamp * 1000),
					requestDate: new Date(this.currentListItem.requestTimestamp * 1000),
				});
				this.currentListItem['events'] = response[1]['data'];
			}, errorResponse => {
				try {
					let error = <CommonError>errorResponse.json();
					this.message.showMessage(error.errorMessage.message);
				} catch (e) {
					this.message.showMessage(`Error getting TR #{params.id}`);
				} finally {
					this.router.navigate(['/requests']);
				}
			});
			this.trApi.transferRequestIdGet(params.id)
		});
	}

}
