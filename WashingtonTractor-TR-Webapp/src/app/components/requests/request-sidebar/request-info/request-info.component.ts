import {
	Component, ViewEncapsulation, ChangeDetectionStrategy, EventEmitter, Input, Output,
	OnInit
} from '@angular/core';
import {ReadTransferRequest} from '../../../../generated/model/ReadTransferRequest';
import {AuthService} from '../../../../services/auth.service';
import {PURPOSE_TITLES} from '../../../../models/transfer-purpose';
import {RequestService} from '../../../../services/request.service';
import * as requestApiAll from '../../../../generated/model/ReadTransferRequest';
import { MdDialog } from '@angular/material';
import {NewRequestComponent} from '../../new-request/new-request.component';

@Component({
	selector: 'app-request-info',
	templateUrl: './request-info.component.html',
	styleUrls: ['./request-info.component.scss'],
	encapsulation: ViewEncapsulation.None,
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class RequestInfoComponent implements OnInit {
	isOpenedAllInfo : boolean = false;
	@Input() currentListItem: ReadTransferRequest;
	@Output() onResubmit = new EventEmitter<boolean>();
	@Output() onCancelTransferRequest = new EventEmitter<boolean>();
	@Output() onShowAllInfo = new EventEmitter<boolean>();

	PURPOSE_TITLES = PURPOSE_TITLES;

	canResubmit: boolean;

	declined = requestApiAll.ReadTransferRequest.StatusEnum.DECLINED;

	resubmitRequest(item?: ReadTransferRequest) {
		let dialogRef = this.dialog.open(NewRequestComponent, {
			width: '550px',
		});
		dialogRef.componentInstance.setCurrentRequest(item, true);
		dialogRef.afterClosed().subscribe(result => {
			if (!result) return;
			this.onResubmit.emit(true);
		});
	}

	constructor(private requestService: RequestService,
							private dialog: MdDialog) {
	}

	ngOnInit(): void {
		this.canResubmit = this.currentListItem.status === this.declined && this.requestService.isRequester(this.currentListItem.requestedBy.id);
	}


	showAllInfo(action : boolean) {
		this.isOpenedAllInfo = !this.isOpenedAllInfo;
		this.onShowAllInfo.emit(action);
	};
}
