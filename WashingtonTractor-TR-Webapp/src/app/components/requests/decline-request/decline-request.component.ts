import { Component, OnInit, ViewEncapsulation, ChangeDetectionStrategy } from '@angular/core';
import { ReadTransferRequest } from '../../../generated/model/ReadTransferRequest';
import { MdDialogRef } from '@angular/material';
import { FormControl } from '@angular/forms';

@Component({
	selector: 'app-decline-request',
	templateUrl: './decline-request.component.html',
	styleUrls: ['./decline-request.component.scss'],
	encapsulation: ViewEncapsulation.None,
	changeDetection: ChangeDetectionStrategy.Default
})
export class DeclineRequestComponent implements OnInit {

	item: ReadTransferRequest;
	text: FormControl;

	constructor(private dialogRef: MdDialogRef<DeclineRequestComponent>) { }

	decline() {
		if (!this.text.value.trim().length) {
			this.text.setErrors({'required': true});
			return;
		}
		this.dialogRef.close(this.text.value);
	}

	ngOnInit() {
		this.text = new FormControl('');
	}

}
