import {Component, OnInit, ViewEncapsulation, ChangeDetectionStrategy, EventEmitter, Input, Output} from '@angular/core';
import { ReadTransferRequest } from '../../../generated/model/ReadTransferRequest';
import { MdDialog } from '@angular/material';
import { DeclineRequestComponent } from '../decline-request/decline-request.component';
import { TransferRequestApi } from '../../../generated/api/TransferRequestApi';
import { RequestService } from '../../../services/request.service';
import { MessageService } from '../../../services/message.service';
import * as requestApiAll from '../../../generated/model/ReadTransferRequest';
import { CommonError } from '../../../generated/model/CommonError';
import { CommonErrorAttribute } from '../../../generated/model/CommonErrorAttribute';
import { AuthService } from '../../../services/auth.service';
import { ReadTransferRequestTemplate } from '../read-transfer-requset-template.interface';

@Component({
	selector: 'app-request-status',
	templateUrl: './request-status.component.html',
	styleUrls: ['./request-status.component.scss'],
	encapsulation: ViewEncapsulation.None,
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class RequestStatusComponent implements OnInit {
	@Input() item: ReadTransferRequestTemplate;
	@Input() transferDetailsView: boolean;

	pending = requestApiAll.ReadTransferRequest.StatusEnum.PENDING;

	cancelled = requestApiAll.ReadTransferRequest.StatusEnum.CANCELLED;

	approved = requestApiAll.ReadTransferRequest.StatusEnum.APPROVED;

	completed = requestApiAll.ReadTransferRequest.StatusEnum.COMPLETED;

	inTransit = requestApiAll.ReadTransferRequest.StatusEnum.INTRANSIT;

	canApproveAndDecline: boolean;

	canCancel: boolean;

	canComplete: boolean;

	constructor(
		private dialog: MdDialog,
		private requestService: RequestService,
		private messageService: MessageService,
		private auth: AuthService
	) {}

	declineTransferRequest() {
		let dialogRef = this.dialog.open(DeclineRequestComponent, {
			width: '560px',
		});
		dialogRef.componentInstance.item = this.item;
		dialogRef.afterClosed().subscribe(text => {
			if (text === undefined) return;
			this.requestService.decline(this.item, text).subscribe(_ => {
				this.messageService.showMessage(`Request ${this.item.id} was declined`);
			});
		});
	}

	approveTransferRequest() {
		this.requestService.approve(this.item).subscribe(_ => {
			this.messageService.showMessage(`Request ${this.item.id} was approved`);
		});
	}

	cancelTransferRequest() {
		this.requestService.handleCancel(this.item);
		this.requestService.closeSidebar();
	}

	completeTransferRequest() {
		this.requestService.complete(this.item);
		this.requestService.closeSidebar();
	}

	restoreTransferRequest() {
		this.requestService.handleRestore(this.item);
	}

	isTheSameRequestLocation() {
		if (!this.item.storeItem.store.manager) return false;
		return this.item.storeItem.store.manager.id === this.auth.getUserId();
	}

	ngOnInit() {
		this.canApproveAndDecline = this.auth.isManagerStore() && this.item.status === this.pending && this.isTheSameRequestLocation();
		this.canCancel = this.auth.isAdmin() && this.item.status !== this.cancelled && !this.item['removed'] && this.item.status !== this.completed;
		this.canComplete = (this.auth.isAdmin() || this.auth.isTransferCoordinator()) && (this.item.status === this.approved || this.item.status === this.inTransit);
	}
}
