import { Component, OnInit, ViewEncapsulation, ChangeDetectionStrategy, Input } from '@angular/core';
import { ReadStoreItem } from '../../../generated/model/ReadStoreItem';

@Component({
	selector: 'app-item-search-item',
	template: `
		<div class="item-search-item">
			<span *ngIf="searchParam === 'model'"><strong>{{item.modelName}}</strong> {{item.stockNumber}} </span>
			<span *ngIf="searchParam === 'stock'"><strong>{{item.stockNumber}}</strong> {{item.modelName}}</span>
			<div>
				<span>{{item.store.name}}</span>
				<span>{{item.daysInStock}} days</span>
			</div>
		</div>
	`,
	styles: [`
		.item-search-item {
			display: flex;
			justify-content: space-between;
			color: #252525;
			font-size: 13.3333px;
		}
		.item-search-item div {
			display: flex;
			justify-content: space-between;
			min-width: 200px;
		}
		.item-search-item span {
			font-size: 12px;
			font-weight: 500;
			font-family: Montserrat;
			color: #252525;
			position: relative;
		}
		.item-search-item span:first-child {
			max-width: 125px;
			white-space: nowrap;
			text-overflow: ellipsis;
			overflow: hidden;
		}
		.item-search-item span:last-child:before {
			background-image: url(/assets/calendar.png);
			position: absolute;
			left: -25px;
			content: '';
			width: 16px;
			height: 15px;
			top: 49%;
			margin-top: -7.5px;
		}
	`],
	encapsulation: ViewEncapsulation.None,
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class ItemSearchItemComponent implements OnInit {

	@Input() searchParam: string;
	@Input() item: ReadStoreItem;

	constructor() { }

	ngOnInit() {
	}

}
