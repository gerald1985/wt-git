import {
	Component, OnInit, ViewEncapsulation, ChangeDetectionStrategy, Input,
	Output, EventEmitter, ViewChild, ElementRef, AfterViewInit, SimpleChanges
} from '@angular/core';

import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import { MdSelect, MdAutocomplete } from '@angular/material';
import { ChangeDetectorRef, OnChanges } from '@angular/core';
import { ReadStoreItem } from '../../../generated/model/ReadStoreItem';


@Component({
	selector: 'app-item-search',
	template: `
		<div class="item-search">
			<div (click)="triggerShowSelect()" class="item-result" *ngIf="!showSelect && !!selectedItem">
				<app-item-search-item [item]="selectedItem" [searchParam]="searchParam"></app-item-search-item>
			</div>
			<input (click)="triggerShowSelect()" class="form-control" type="text" #searchInput [formControl]="search" placeholder="Choose item">
			<app-item-select [items]="items" [show]="showSelect" (onSelect)="handleSelect($event)" [searchParam]="searchParam"></app-item-select>
		</div>
	`,
	styles: [`
		.item-result {
			position: absolute;
			padding: 10px 30px;
			height: 40px;
			box-sizing: border-box;
			top: 0;
			left: 0;
			right: 0;
			bottom: 0;
			background-color: #F9F9F9;
			border-radius: 3px;
			border: 2px solid #c8cfd4;
		}
		.item-search {
			position: relative;
			margin-bottom: 14px;
		}
		.item-search:after {
			position: absolute;
			right: 20px;
			top: 50%;
			content: '>';
			-webkit-transform: translateY(-50%) rotate(90deg) scaleY(2);
			transform: translateY(-50%) rotate(90deg) scaleY(2);
			font-size: 12px;
			font-weight: bold;
			font-family: "Open Sans";
			color: rgba(0, 0, 0, 0.38);
		}
		.item-search .form-control {
			display: block;
			width: 100%;
			box-sizing: border-box;
		}
	`],
	encapsulation: ViewEncapsulation.None,
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class ItemSearchComponent implements OnInit, AfterViewInit, OnChanges {

	@Input() searchParam: string;
	@Input() items: Array<ReadStoreItem> = [];
	@Input() selectedItem: ReadStoreItem;
	@Output() onSearch = new EventEmitter<string>();
	@Output() onItemSelect = new EventEmitter<ReadStoreItem>();

	search = new FormControl('');
	showSelect = false;

	selectedValue;

	@ViewChild('searchInput') searchInput: ElementRef;
	@ViewChild('searchSelect') searchSelect: MdAutocomplete;

	handleSelect(event) {
		this.showSelect = false;
		this.onItemSelect.emit(event);
	}

	triggerShowSelect() {
		this.showSelect = true;
		this.cd.detectChanges();
	}

	constructor(private cd: ChangeDetectorRef) {}

	ngOnInit() {
		this.search.valueChanges.debounceTime(500).subscribe(value => {
			this.onSearch.emit(value);
		});
	}

	ngOnChanges(changes: SimpleChanges) {}

	ngAfterViewInit() {
		Observable.fromEvent(this.searchInput.nativeElement, 'blur')
		.delay(200)
		.subscribe(e => {
			this.showSelect = false;
			this.cd.detectChanges();
		});
	}

}
