import {
	Component, OnInit, ViewEncapsulation, ChangeDetectionStrategy,
	Input, ContentChildren, QueryList, EventEmitter, Output, AfterContentInit
} from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { MdOption } from '@angular/material';
import { ElementRef, OnChanges } from '@angular/core';
import { ReadStoreItem } from '../../../generated/model/ReadStoreItem';

@Component({
	selector: 'app-item-select',
	template: `
		<div *ngIf="show" class="mat-select-panel">
			<md-option (onSelectionChange)="handleSelectionChange($event)" *ngFor="let item of items" [value]="item">
				<app-item-search-item [item]="item" [searchParam]="searchParam"></app-item-search-item>
			</md-option>
		</div>
	`,
	styles: [`
		.item-search .mat-select-panel {
			position: absolute;
			top: 40px;
			width: 100%;
			display: block;
			left: 0;
			background-color: #fff;
			z-index: 1;
			max-height: 300px;
			overflow-y: auto;
		}
	`],
	encapsulation: ViewEncapsulation.None,
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class ItemSelectComponent implements OnInit, OnChanges {

	@Input() searchParam: string;
	@Input() items: Array<ReadStoreItem> = [];
	@Input() show = false;

	@Output() onSelect = new EventEmitter();

	@ContentChildren(MdOption) options: QueryList<MdOption>;


	constructor(private element: ElementRef) { }

	ngOnInit() {}

	ngOnChanges() {
	}

	handleSelectionChange(value) {
		if (!value.isUserInput) return;
		this.onSelect.emit(value.source.value);
	}

}
