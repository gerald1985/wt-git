import { Component, Output, EventEmitter, OnInit, Input, Inject, ChangeDetectionStrategy, ViewEncapsulation, OnChanges } from '@angular/core';
import { ReadUser } from '../../../generated/model/ReadUser';
import * as userApi from '../../../generated/model/ReadUser';
import { ReadStore } from '../../../generated/model/ReadStore';
import { ListOfUsersApi } from '../../../generated/api/ListOfUsersApi';
import { ListOfStoresApi } from '../../../generated/api/ListOfStoresApi';
import { MessageService } from '../../../services/message.service';
import { PURPOSES } from '../../../models/transfer-purpose';
import { STATUSES } from '../../../models/transfer-status';
import { RequestsGetParams } from '../requset-get-params.interface';
import { ListOfItemsApi } from '../../../generated/api/ListOfItemsApi';
import { FormGroup, FormControl } from '@angular/forms';
import { RequestService } from '../../../services/request.service';
import { AuthService } from '../../../services/auth.service';

@Component({
	selector: 'app-requests-filter',
	templateUrl: './requests-filter.component.html',
	styleUrls: ['./requests-filter.component.scss'],
	changeDetection: ChangeDetectionStrategy.OnPush,
	encapsulation: ViewEncapsulation.None
})
export class RequestsFilterComponent implements OnInit, OnChanges {

	users: Array<ReadUser> = [];
	locations: Array<ReadStore>;
	purposes = PURPOSES;
	statuses = STATUSES;
	models: Array<any> = [];
	dates: Array<Date> = [];
	maxDate = new Date();

	filterForm: FormGroup;
	canAddRequest = (this.auth.isAdmin() || this.auth.isTransferCoordinator()) || this.auth.getUserStoreIds().length;
	@Input() params: RequestsGetParams = {};
	@Output() onAddClick = new EventEmitter();
	@Output() onClearFilter = new EventEmitter();

	constructor(
		private userList: ListOfUsersApi,
		@Inject('ListOfStoresApi') private storeList: ListOfStoresApi,
		private messages: MessageService,
		private itemsList: ListOfItemsApi,
		private requests: RequestService,
		private auth: AuthService
	) {
		this.filterForm = new FormGroup({
			itemStoreIds: new FormControl([]),
			receivingStoreIds: new FormControl([]),
			purposes: new FormControl([]),
			statuses: new FormControl([]),
			requestedByIds: new FormControl([]),
			requestTimestampFrom: new FormControl(),
			requestTimestampTo: new FormControl()
		});
	}

	private updateParams(params: RequestsGetParams) {
		this.filterForm.get('itemStoreIds').setValue(params.itemStoreIds || [], {emitEvent: false});
		this.filterForm.get('receivingStoreIds').setValue(params.receivingStoreIds || [], {emitEvent: false});
		this.filterForm.get('purposes').setValue(params.purposes || [], {emitEvent: false});
		this.filterForm.get('statuses').setValue(params.statuses || [], {emitEvent: false});
		this.filterForm.get('requestedByIds').setValue(params.requestedByIds || [], {emitEvent: false});
		this.filterForm.get('requestTimestampFrom').setValue(params.requestTimestampFrom, {emitEvent: false});
		this.filterForm.get('requestTimestampTo').setValue(params.requestTimestampTo, {emitEvent: false});
               
                

	}

	ngOnChanges() {
		this.updateParams(this.params);
	}

	ngOnInit() {
		this.getUsers(1);
		this.getLocations();
		this.getModels(1);

		this.filterForm.valueChanges.debounceTime(500).subscribe(formValues => {
			let newParams = {};
			Object.keys(formValues).forEach(key => {
				let value = formValues[key];
				newParams[key] = (!value || (value instanceof Array && !value.length)) ? undefined : value;
			});
			this.updateFilter(newParams);
		});
	}

	private updateFilter(newParams: RequestsGetParams) {
		newParams.page = 1;
		this.requests.updateGetParams(newParams);
	}

	clearFilter() {
		this.dates = [];
		let formValues = this.filterForm.value;
		let newParams: RequestsGetParams = {};
		Object.keys(formValues).forEach(key => {
			newParams[key] = undefined;
		});
		newParams.itemStockNumber = undefined;
		this.onClearFilter.emit();
		this.updateFilter(newParams);

	}

	clear(control, value) {
		this.filterForm.get(control).setValue(value);
	}

	clearDate() {
		this.dates = [];
		this.filterForm.get('requestTimestampFrom').setValue(undefined);
		this.filterForm.get('requestTimestampTo').setValue(undefined);
	}

	handleDateSelect(dates: Array<Date>) {
		this.dates = dates;
		let d: Date;
		if (dates[0]) {
			d = dates[0];
			let newDate = new Date(d.getFullYear(), d.getMonth(), d.getDate(), 0, 0, 0, 0);
			this.filterForm.get('requestTimestampFrom').setValue(newDate.getTime() / 1000);
		} else {
			this.clearDate();
			return;
		}
		d = dates[1] || dates[0];
		this.filterForm.get('requestTimestampTo').setValue(new Date(d.getFullYear(), d.getMonth(), d.getDate(), 23, 59, 59, 0).getTime() / 1000);
	}

	private getPrefilledRequesterField() {
		return this.auth.isAdmin() || this.auth.isTransferCoordinator() || this.auth.isManagerStore() ? null : [this.auth.getUserId()];
	}

	private getUsers(page) {
		this.userList.usersGet(100, page, [userApi.ReadUser.StatusEnum.REGISTERED.toString(), userApi.ReadUser.StatusEnum.DELETED.toString()])
			.subscribe(response => {
				this.users = [...this.users, ...response.data];
				this.filterForm.get('requestedByIds').setValue(this.getPrefilledRequesterField());
				let userStoreIds = this.auth.getUserStoreIds();
				this.filterForm.get('itemStoreIds').setValue(userStoreIds);
				this.filterForm.get('receivingStoreIds').setValue(userStoreIds);
				if (response.data.length === 100) this.getUsers(page++);
			}, error => {
				this.messages.showMessage('Error getting users for TR filter.');
			});
	}

	private getModels(page) {
		this.itemsList.itemsModelsGet(100, page).subscribe(response => {
			this.models = [...this.models, ...response.data];
			if (response.data.length === 100) this.getModels(page++);
		}, error => {
			this.messages.showMessage('Error getting item models for TR filter.');
		});
	}

	private getLocations() {
		this.storeList.storesGet().subscribe(response => {
			this.locations = response.data;
		}, error => {
			this.messages.showMessage('Error getting stores for TR filter.');
		});
	}

	addRequest() {
		this.onAddClick.emit();
	}
}
