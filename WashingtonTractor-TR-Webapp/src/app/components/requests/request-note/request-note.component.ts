import {Component, OnInit, ViewEncapsulation, ChangeDetectionStrategy, Input} from '@angular/core';

@Component({
	selector: 'app-request-note',
	templateUrl: 'request-note.component.html',
	styleUrls: ['request-note.component.scss'],
	encapsulation: ViewEncapsulation.None,
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class RequestNoteComponent implements OnInit {
	@Input() event: any;

	constructor() {
	}

	ngOnInit() {
		Object.assign(this.event, {
			shipmentDate: new Date(this.event.shipmentTimestamp * 1000),
			requestDate: new Date(this.event.requestTimestamp * 1000),
		});
	}

}
