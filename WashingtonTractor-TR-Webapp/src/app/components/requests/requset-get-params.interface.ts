export interface RequestsGetParams {
	itemModelName?: string;
	itemStockNumber?: string;
	itemStoreIds?: number;
	receivingStoreIds?: number;
	purposes?: string;
	statuses?: string;
	requestedByIds?: number;
	requestTimestampFrom?: number;
	requestTimestampTo?: number;
	perPage?: number;
	page?: number;
};
