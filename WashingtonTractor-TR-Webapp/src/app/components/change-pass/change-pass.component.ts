import {Component, OnInit, ViewEncapsulation, ChangeDetectionStrategy} from '@angular/core';
import {FormBuilder, FormGroup, FormControl, Validators, AbstractControl, ValidatorFn} from '@angular/forms';
import {UserApi} from '../../generated/api/UserApi';
import {MdDialogRef} from '@angular/material';
import {passwordMatchValidator} from '../../validators/password-match.validator';
import {ChangePassword} from '../../generated/model/ChangePassword';
import {MessageService} from '../../services/message.service';
import {CommonError} from "../../generated/model/CommonError";
@Component({
	selector: 'app-change-pass',
	templateUrl: './change-pass.component.html',
	styleUrls: ['./change-pass.component.scss'],
	encapsulation: ViewEncapsulation.None,
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class ChangePassComponent implements OnInit {

	changePasswordForm: FormGroup;
	invalidNewPassword: boolean;
	error: string;

	constructor(private userApi: UserApi, private dialogRef: MdDialogRef<ChangePassComponent>, private message: MessageService) {
	}

	changePassword({value, valid}: {value: ChangePassword, valid: boolean}) {
		Object.keys(this.changePasswordForm.controls).forEach(key => {
			this.changePasswordForm.get(key).markAsDirty();
		});
		this.checkNewPasswordValidation();
		if (!valid) return;
		this.userApi.userChangePasswordPut(value).subscribe(_ => {
			this.dialogRef.close();
		}, errorResponse => {
			let error = <CommonError>errorResponse.json();
			this.message.showMessage(error.errorMessage.message);
		});
	}

	checkNewPasswordValidation() {
		this.invalidNewPassword = this.changePasswordForm.get('newPassword').invalid;
	}

	ngOnInit() {
		this.changePasswordForm = new FormGroup({
			currentPassword: new FormControl('', Validators.required),
		});
		this.changePasswordForm.addControl('newPassword', new FormControl('', [Validators.required, passwordEqualityValidator(this.changePasswordForm.get('currentPassword')), Validators.pattern(new RegExp(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).{6,}$/))]));
		this.changePasswordForm.addControl('passwordRepeat', new FormControl('', [Validators.required, passwordMatchValidator(this.changePasswordForm.get('newPassword'))]));

		this.changePasswordForm.get('newPassword').valueChanges.subscribe(() => {
			this.changePasswordForm.get('passwordRepeat').setValue(this.changePasswordForm.get('passwordRepeat').value);
		});

		this.changePasswordForm.get('currentPassword').valueChanges.subscribe(() => {
			this.changePasswordForm.get('newPassword').setValue(this.changePasswordForm.get('newPassword').value);
		});
	}

}

export function passwordEqualityValidator(pass1: AbstractControl): ValidatorFn {
	return (control: AbstractControl): {[key: string]: any} => {
		if (!control.value) return null;
		return pass1.value === control.value ? {'passwordEqualityError': 'Passwords match'} : null;
	};
}
