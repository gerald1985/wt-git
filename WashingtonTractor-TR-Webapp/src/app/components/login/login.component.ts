import { ListOfUsersApi } from './../../generated/api/ListOfUsersApi';
import { AccountCreateComponent } from './account-create/account-create.component';
import { Component, OnInit, ViewEncapsulation, ChangeDetectionStrategy, Inject, ViewChild, ElementRef } from '@angular/core';
import { MdDialog, MdDialogRef } from '@angular/material';
import { PasswordRestoreComponent } from './password-restore/password-restore.component';
import { FormControl, Validators, FormGroup } from '@angular/forms';
import { AuthService } from '../../services/auth.service';
import { NewPasswordComponent } from './new-password/new-password.component';
import { ActivatedRoute, Router } from '@angular/router';
import { CommonError } from '../../generated/model/CommonError';
import { MessageService } from '../../services/message.service';

@Component({
	selector: 'app-login',
	templateUrl: './login.component.html',
	styleUrls: ['./login.component.scss'],
	encapsulation: ViewEncapsulation.None,
	changeDetection: ChangeDetectionStrategy.OnPush

})
export class LoginComponent implements OnInit {

	showRestore = false;
	showCreate = false;

	loginForm: FormGroup;

	onSubmit() {
		if (!this.loginForm.valid) return;
		let form = <LoginForm> this.loginForm.value;
		this.auth.login(encodeURIComponent(form.username), form.password).subscribe(response => {

		}, response => {
			try {
				let error: CommonError = response.json();
				if (error.errorMessage.message) {
					this.messages.showMessage(error.errorMessage.message);
				}
			} catch (e) {
				this.messages.showMessage('Unknown error');
			}
		});
	}

	openRestore() {
		let dialogRef = this.dialog.open(PasswordRestoreComponent, {
			width: '310px',
		});

		dialogRef.afterClosed().subscribe(result => {
			// console.log(result);
		});
	}

	openCreate() {
		let dialogRef = this.dialog.open(AccountCreateComponent, {
			width: '800px',
		});

		dialogRef.afterClosed().subscribe(result => {

		});
	}

	openNewPass(token) {
		let dialogRef = this.dialog.open(NewPasswordComponent, {
			width: '480px',
		});

		dialogRef.componentInstance.token = token;

		dialogRef.afterClosed().subscribe(result => {
			this.router.navigate(['/login']);
		});
	}

	constructor(
		private dialog: MdDialog,
		private auth: AuthService,
		private route: ActivatedRoute,
		private router: Router,
		private messages: MessageService
	) {
		this.loginForm = new FormGroup({
			username: new FormControl('', Validators.required),
			password: new FormControl('', Validators.required)
		});
	}

	ngOnInit() {
		this.route.params.subscribe((param: any) => {
			if (param.token) {
				this.openNewPass(param.token);
			}
		});
	}

}


export interface LoginForm {
	username: string;
	password: string;
}
