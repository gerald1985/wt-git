import {Component, ViewEncapsulation, ChangeDetectionStrategy, ChangeDetectorRef, OnInit} from '@angular/core';
import { MdDialog, MdDialogRef } from '@angular/material';
import { FormControl, Validators } from '@angular/forms';
import { UserApi } from '../../../generated/api/UserApi';

@Component({
	selector: 'app-password-restore',
	templateUrl: './password-restore.component.html',
	styleUrls: ['./password-restore.component.scss'],
	encapsulation: ViewEncapsulation.None,
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class PasswordRestoreComponent implements OnInit{

	email = new FormControl('', [Validators.required, Validators.email]);
	error: string;

	ngOnInit() {
		this.email.valueChanges.subscribe(() => {
			this.error = null;
		});
	};
	constructor(private dialogRef: MdDialogRef<PasswordRestoreComponent>, private userApi: UserApi, public cd: ChangeDetectorRef) {}

	submit() {
		this.email.markAsDirty();
		this.error = null;
		if (this.email.invalid) return;
		this.userApi.userResetPasswordPost({email: this.email.value}).subscribe(response => {
			this.dialogRef.close();
		}, error => {
			this.error = 'Error occured. Maybe this email address is not registered';
			this.cd.detectChanges();
		});
	}
}

