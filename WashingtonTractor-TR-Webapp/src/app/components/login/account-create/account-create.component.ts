import { MdDialogRef } from '@angular/material';
import { Component, ViewEncapsulation, ChangeDetectionStrategy, OnInit, Inject, ChangeDetectorRef } from '@angular/core';
import { ListOfStoresApi } from '../../../generated/api/ListOfStoresApi';
import { UserApi } from '../../../generated/api/UserApi';
import { ReadStore } from '../../../generated/model/ReadStore';
import { USER_ROLES } from '../../../models/user-level';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { WriteUser } from '../../../generated/model/WriteUser';
import { passwordMatchValidator } from '../../../validators/password-match.validator';
import * as apiUsers from '../../../generated/model/ReadUser';
import { MessageService } from '../../../services/message.service';
import { CommonError } from '../../../generated/model/CommonError';

@Component({
	selector: 'app-account-create',
	templateUrl: './account-create.component.html',
	styleUrls: ['./account-create.component.scss'],
	encapsulation: ViewEncapsulation.None,
	changeDetection: ChangeDetectionStrategy.Default
})
export class AccountCreateComponent implements OnInit {

	levels = USER_ROLES;
	locations: Array<ReadStore> = [];
	newUserForm: FormGroup;
	passwordRepeat: FormControl;
	showLocation = false;
	emailInvalid: boolean;
	passwordInvalid: boolean;

	errors: any = {};

	constructor(
		private dialogRef: MdDialogRef<AccountCreateComponent>,
		@Inject('ListOfStoresApi') private listStoresApi: ListOfStoresApi,
		private userApi: UserApi,
		private message: MessageService,
		private messageService: MessageService
	) { }

	createUser({value, valid}: {value: WriteUser, valid: boolean}) {
		Object.keys(this.newUserForm.controls).forEach(key => {
			this.newUserForm.get(key).markAsDirty();
		});
		this.checkEmailValidation();
		this.checkPasswordValidation();
		if (!valid) return;
		this.errors = {};
		if (value.password !== this.passwordRepeat.value) return;
		this.userApi.userPost(value).subscribe(response => {
			this.messageService.showMessage('Your request will be processed by the admin.');
			this.dialogRef.close(response.data);
		}, errorResponse => {
			let error = <CommonError>errorResponse.json();
			if (!error.errorMessage.attributes) {
				this.message.showMessage(error.errorMessage.message);
				return;
			}
			error.errorMessage.attributes.forEach(fieldError => {
				let formControl = this.newUserForm.get(fieldError.field);
				if (!formControl) {
					fieldError.errors.forEach(message => {
						this.message.showMessage(message);
					});
					return;
				}
				this.errors[fieldError.field] = fieldError.errors;
				formControl.setErrors({'serverError': true});
			});
		});
	}

	checkEmailValidation() {
		this.emailInvalid = this.newUserForm.get('email').invalid;
	}

	checkPasswordValidation() {
		this.passwordInvalid = this.newUserForm.get('password').invalid;
	}

	ngOnInit() {
		this.listStoresApi.storesGet().subscribe(response => {
			this.locations = response.data;
		});

		this.newUserForm = new FormGroup({
			email: new FormControl('', [Validators.required, Validators.email]),
			firstName: new FormControl('', Validators.required),
			lastName: new FormControl('', Validators.required),
			password: new FormControl('', [Validators.required, Validators.pattern(new RegExp(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).{6,}$/))]),
			role: new FormControl(null, Validators.required),
			storeId: new FormControl(null),
		});

		this.newUserForm.get('password').valueChanges.subscribe(() => {
			this.passwordRepeat.setValue(this.passwordRepeat.value);
		});

		this.newUserForm.get('role').valueChanges.subscribe(value => {
			if (value === apiUsers.ReadUser.RoleEnum.MANAGERSALE || value === apiUsers.ReadUser.RoleEnum.MANAGERSTORE) {
				this.showLocation = true;
			} else {
				this.showLocation = false;
				this.newUserForm.get('storeId').setValue(null);
			}
		});

		this.passwordRepeat = new FormControl('', [Validators.required, passwordMatchValidator(this.newUserForm.get('password'))]);
	}
}
