import { Component, ViewEncapsulation, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { MdDialogRef } from '@angular/material';
import { FormControl, Validators } from '@angular/forms';
import { UserApi } from '../../../generated/api/UserApi';
import { passwordMatchValidator } from '../../../validators/password-match.validator';

@Component({
	selector: 'app-new-password',
	templateUrl: './new-password.component.html',
	styleUrls: ['./new-password.component.scss'],
	encapsulation: ViewEncapsulation.None,
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class NewPasswordComponent {

	token: string;
	password = new FormControl('', [Validators.required, Validators.pattern(new RegExp(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).{6,}$/))]);
	password2 = new FormControl('', [Validators.required, passwordMatchValidator(this.password)]);
	invalidPassword: boolean

	error: string;

	constructor(private dialogRef: MdDialogRef<NewPasswordComponent>, private userApi: UserApi, public cd: ChangeDetectorRef) {}

	checkPasswordValidation() {
		this.invalidPassword = this.password.invalid;
	}

	submit() {
		this.error = null;
		if (this.password.invalid || this.password2.invalid) return;
		this.userApi.userChangePasswordWithTokenPut({
			password: this.password.value, token: this.token
		}).subscribe(response => {
			this.dialogRef.close();
		}, error => {
			this.error = 'Error updating password';
			this.cd.detectChanges();
		});
	}

}
