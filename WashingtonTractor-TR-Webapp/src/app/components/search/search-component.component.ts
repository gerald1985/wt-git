import {
	Component, OnInit, ViewEncapsulation, ChangeDetectionStrategy, Output, EventEmitter,
	Input
} from '@angular/core';
import {FormControl} from "@angular/forms";

@Component({
  selector: 'app-search-component',
  template: `
	<form class="search__wrapper">
		<input [formControl]="searchValue" class="search__input" type="text" placeholder="Search By"><i class="fa fa-search search__icon" aria-hidden="true"></i>
		<div  class="search__radio-container pull-right">
			<input class="search__input-radio" id="radioStock" value="stock" type="radio" [formControl]="searchType">
			<label class="search__label-radio" for="radioStock">Stock #</label>
			<input class="search__input-radio" id="radioModel" value="model" type="radio" [formControl]="searchType">
			<label class="search__label-radio" for="radioModel">Model #</label>
		</div>
	</form>`,
	styleUrls: ['./search-component.component.scss'],
	encapsulation: ViewEncapsulation.None,
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class SearchComponentComponent implements OnInit {

	@Output() onSearch = new EventEmitter<Search>();
	@Input() clearSearchText$;

	searchValue = new FormControl('');
	searchType = new FormControl('model');
	searchObject = new Search();

	constructor() {
	}

	ngOnInit() {
		if (this.clearSearchText$) {
			this.clearSearchText$.subscribe(_ => {
				this.searchValue.setValue(null, {emitEvent: false});
			});
		}

		this.searchValue.valueChanges.debounceTime(500).subscribe(value => {
			this.searchObject.value = value;
			this.searchObject.type = this.searchType.value;
			this.onSearch.emit(this.searchObject);
		});

		this.searchType.valueChanges.subscribe(_ => {
			this.searchValue.setValue(null);
		});
	}

}

export class Search {
	value?: string;
	type?: string;
};
