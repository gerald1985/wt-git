export const EQUIPMENT_TABLE_FIELDS = [
{
	name: 'stockNumber',
	display: true,
	displayName: 'Stock #',
	disabled: true
},
{
	name: 'locationName',
	display: true,
	displayName: 'Location'
},
{
	name: 'make',
	display: true,
	displayName: 'Make'
},
{
	name: 'category',
	display: false,
	displayName: 'Category'
},
{
	name: 'pin',
	display: false,
	displayName: 'PIN'
},
	{
		name: 'modelName',
		display: true,
		displayName: 'Model Name'
	},
{
	name: 'meter',
	display: true,
	displayName: 'Meter'
},
{
	name: 'yearMfrd',
	display: false,
	displayName: 'Year Manufactured'
},
{
	name: 'tagNumber',
	display: false,
	displayName: 'Tag No'
},
{
	name: 'daysInStock',
	display: true,
	displayName: 'Days in Stock'
},
{
	name: 'attachedTo',
	display: false,
	displayName: 'Attached To'
},
{
	name: 'usage',
	display: false,
	displayName: 'Usage'
},
{
	name: 'salesPerson',
	display: false,
	displayName: 'Sales Person'
},
{
	name: 'attachmentNumber',
	display: false,
	displayName: 'Attachment #'
},
	{
		name: 'orderCode',
		display: false,
		displayName: 'OrderCode'
	},
];
