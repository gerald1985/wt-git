import { Component, OnInit, ViewEncapsulation, ChangeDetectionStrategy, Output, EventEmitter, Input, OnChanges } from '@angular/core';

@Component({
	selector: 'app-equipment-settings',
	templateUrl: './equipment-settings.component.html',
	styleUrls: ['./equipment-settings.component.scss'],
	encapsulation: ViewEncapsulation.None,
	changeDetection: ChangeDetectionStrategy.OnPush
})

export class EquipmentSettingsComponent implements OnInit, OnChanges {

	@Output() onSave = new EventEmitter();

	@Input() fields: Array<ItemDisplayField> = [];

	filedsByColumns = [];

	constructor() {
	}

	buildColumns() {
		const maxInColumn = Math.ceil(this.fields.length / 4);
		this.filedsByColumns = [];
		let col = [];
		for (let i = 0, len = this.fields.length; i < len; i++) {
			col.push(this.fields[i]);
			if (col.length < maxInColumn) continue;
			this.filedsByColumns.push(col);
			col = [];
		}
		if (col.length) this.filedsByColumns.push(col);
	}

	changeAll(status: boolean) {
		this.fields.forEach(field => {
			if (field.disabled) return;
			field.display = status;
		});
	}

	ngOnInit() {
		// this.buildColumns();
	}

	ngOnChanges() {
		this.buildColumns();
	}

	saveFields() {
		this.onSave.emit(this.fields.slice());
	}
}

interface ItemDisplayField {
	name: string;
	display: boolean;
	displayName: string;
	disabled?: boolean;
}
