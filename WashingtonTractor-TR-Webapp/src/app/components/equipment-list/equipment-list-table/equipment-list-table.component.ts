import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import { Component, OnInit, ViewEncapsulation, ChangeDetectionStrategy, Input, OnChanges, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import { TableEquipmentItem } from '../../../interfaces/table-equipment-item.interface';
import {ReadStoreItem} from "../../../generated/model/ReadStoreItem";
import {NewRequestComponent} from "../../requests/new-request/new-request.component";
import {MdDialog} from "@angular/material";
import {Router} from "@angular/router";
import {AuthService} from "../../../services/auth.service";

import { EquipmentItemDetailsComponent } from '../equipment-item-details/equipment-item-details.component';
import {ItemApi} from '../../../generated/api/ItemApi';
@Component({
	selector: 'app-equipment-list-table',
	templateUrl: './equipment-list-table.component.html',
	styleUrls: ['./equipment-list-table.component.scss'],
	encapsulation: ViewEncapsulation.None,
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class EquipmentListTableComponent implements OnInit, OnChanges, AfterViewInit {

	@Input() fields = [];
	@Input() items: Array<TableEquipmentItem> = [];
	@Input() loadMore$: Subject<any>;
	@Input() resetScroll$ = new Subject();
	canAddRequest = (this.auth.isAdmin() || this.auth.isTransferCoordinator()) || this.auth.getUserStoreIds().length;
	prevScrollTop = 0;

	@ViewChild('scrollTable') scrollTable: ElementRef;

	displayFields = [];

	constructor(private dialog: MdDialog,
							private router: Router,
							private auth: AuthService,
							private itemApi: ItemApi) {
	}

	ngOnInit() {
		this.resetScroll$.subscribe(data => {
			this.prevScrollTop = 0;
		});
	}

	ngOnChanges() {
		this.displayFields = this.fields.filter(f => f.display);
	}

	handleNewRequest(item: ReadStoreItem) {
		let dialogRef = this.dialog.open(NewRequestComponent, {
			width: '550px',
			disableClose: true
		});
		dialogRef.componentInstance.setCurrentRequest(null, false, item);

		dialogRef.afterClosed().subscribe(result => {
			if (!result) return;
			this.router.navigate(['/requests']);
		});
	}



	ngAfterViewInit() {
		const table: HTMLElement = this.scrollTable.nativeElement;
		const offset = 50;
		const scrollSubscription = Observable.fromEvent(window, 'scroll').debounceTime(200);
		const resize$ = Observable.fromEvent(window, 'resize').debounceTime(200);
		const tableInitalHeight = table.clientHeight;
		Observable.merge(scrollSubscription, resize$).filter(() => {
			// if scroll to the top, do nothing
			if (this.prevScrollTop > window.scrollY) {
				return false;
			}
			this.prevScrollTop = window.scrollY;
			const windowBottomBorder = window.innerHeight + window.scrollY;
			const tableBottomBorder = table.clientHeight + table.offsetTop;
			return windowBottomBorder + offset > tableBottomBorder;
		}).subscribe(() => { this.loadMore$.next(); });
	}
	showItemDetails(item: ReadStoreItem) {
		this.itemApi.itemStockIdGet(item.id).subscribe(response => {
			let dialogRef = this.dialog.open(EquipmentItemDetailsComponent, {
				height: '600px',
				width: '548px',
			});
			dialogRef.componentInstance.item = response.data;
		});

	}
}
