import { Subscription } from 'rxjs/Subscription';
import { Subject } from 'rxjs/Subject';
import {Component, OnInit, ViewEncapsulation, ChangeDetectionStrategy} from '@angular/core';
import { TableEquipmentItem } from '../../interfaces/table-equipment-item.interface';
import { EQUIPMENT_TABLE_FIELDS } from './equipment-list-items.const';
import { ListOfItemsApi } from '../../generated/api/ListOfItemsApi';
import {BehaviorSubject} from "rxjs/BehaviorSubject";

@Component({
	selector: 'app-equipment-list',
	templateUrl: './equipment-list.component.html',
	styleUrls: ['./equipment-list.component.scss'],
	encapsulation: ViewEncapsulation.None
})
export class EquipmentListComponent implements OnInit {

	showSettings = false;
	items: Array<TableEquipmentItem> = [];
	fields = EQUIPMENT_TABLE_FIELDS;
	resetScroll$ = new Subject();
	params$ = new BehaviorSubject<any>({
		itemsPerPage: 20,
		page: 1
	});

	clearPrevItems = false;

	isLoading = false;
	scrollSubscription: Subscription;
	loadMore$ = new Subject<any>();

	constructor(public itemsListApi: ListOfItemsApi) {
	}

	subscribeToScroll() {
		this.scrollSubscription = this.loadMore$.subscribe(() => {
			this.clearPrevItems = false;
			let params = this.params$.getValue();
			this.updateGetParams(Object.assign({}, params, {page: params.page + 1}));
			this.loadItems();
		});
	}

	ngOnInit() {
		this.subscribeToScroll();
		this.loadItems();
	}

	updateSearchParams(searchObject) {
		this.updateGetParams({
			page: 1,
			modelName: searchObject.type === 'model' ? searchObject.value : undefined,
			stockNumber: searchObject.type === 'stock' ? searchObject.value : undefined
		});
		this.clearPrevItems = true;
		this.resetScroll$.next();
		this.loadItems();
	}

	updateGetParams(newParams: any = {}) {
		this.params$.next(Object.assign({}, this.params$.getValue(), newParams));
	}

	loadItems() {
		if (this.isLoading) return;
		let params = this.params$.getValue();
		this.isLoading = true;
		this.itemsListApi.itemsStockGet(params.modelName, params.stockNumber, undefined, undefined,  params.itemsPerPage, params.page).subscribe(response => {
			if (this.clearPrevItems) this.items = [];
			let items = response.data.map((item: TableEquipmentItem) => {
				item.locationName = item.store.name;
				return item;
			});
			this.items = [...this.items, ...items];
			this.isLoading = false;
		}, error => {
			this.isLoading = false;
		});
	}

	handleShowSettings() {
		this.showSettings = true;
	}

	saveFileds(fields) {
		this.fields = fields;
		this.showSettings = false;
	}

}
