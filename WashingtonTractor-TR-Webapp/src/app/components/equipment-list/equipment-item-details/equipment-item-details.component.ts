import {Component, OnInit, ViewEncapsulation, ChangeDetectionStrategy, Input} from '@angular/core';
import { MdDialogRef } from '@angular/material';
import {ReadStoreItem} from '../../../generated/model/ReadStoreItem';
import {FormControl} from '@angular/forms';

@Component({
	selector: 'app-equipment-item-details',
	templateUrl: './equipment-item-details.component.html',
	styleUrls: ['./equipment-item-details.component.scss'],
	encapsulation: ViewEncapsulation.None,
	// changeDetection: ChangeDetectionStrategy.OnPush
})
export class EquipmentItemDetailsComponent implements OnInit {

	@Input() item: ReadStoreItem;
	noteControl: FormControl;

	constructor(public dialogRef: MdDialogRef<EquipmentItemDetailsComponent>) {
	}

	selectItem(attachedItem) {
		this.item = attachedItem;
	}

	ngOnInit() {
		this.noteControl = new FormControl({value: this.item['note'], disabled: true});
	}
}
