import {
	Component, OnInit, ViewEncapsulation, ChangeDetectionStrategy, Output, EventEmitter,
	ElementRef, ViewChild, Inject
} from '@angular/core';
import { AuthService } from '../../../services/auth.service';
import {FormControl} from "@angular/forms";
import {AppHttpService} from "../../../services/app-http.service";
import {
	Http, RequestOptionsArgs, Response, Request, RequestOptions, Headers, RequestMethod,
	URLSearchParams
} from '@angular/http';
import {unescape} from "querystring";
import {BASE_PATH} from '../../../generated/variables';


@Component({
	selector: 'app-equipment-filter',
	templateUrl: './equipment-filter.component.html',
	styleUrls: ['./equipment-filter.component.scss'],
	encapsulation: ViewEncapsulation.None,
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class EquipmentFilterComponent implements OnInit {

	@ViewChild('downloadLink') downloadLink: ElementRef;

	@Output() onShowSettings = new EventEmitter();
	shipmentTimestampFrom = new FormControl();
	shipmentTimestampTo: number;
	dates: Array<Date> = [];
	maxDate: Date = new Date();

	isAdmin = this.auth.isAdmin();

	constructor(private auth: AuthService, protected http: AppHttpService, @Inject(BASE_PATH) private BASE_PATH) { }

	private fillDateFromAndTo() {
		let d = this.dates[0];
		this.shipmentTimestampFrom.setValue(new Date(d.getFullYear(), d.getMonth(), d.getDate(), 0, 0, 0, 0).getTime() / 1000);
		this.shipmentTimestampTo = new Date(d.getFullYear(), d.getMonth(), d.getDate(), 23, 59, 59, 0).getTime() / 1000;
	}

	handleDateSelect(dates: Array<Date>) {
		this.dates = dates;
		this.fillDateFromAndTo();
	}


	ngOnInit() {
		let date = new Date();
		date.setDate(date.getDate() - 1); // get previous date
		this.dates.push(date);
		this.maxDate.setDate(date.getDate());
		this.fillDateFromAndTo();
	}

	downloadDatabase() {
		document.location.href = this.BASE_PATH + `/transfer-requests/extract-equipment?
		shipmentTimestampFrom=${this.shipmentTimestampFrom.value}
		&shipmentTimestampTo=${this.shipmentTimestampTo.toString()}
		&access_token=${this.auth.getAccessToken()}`;
	}

	showSettings() {
		this.onShowSettings.emit();
	}

}
