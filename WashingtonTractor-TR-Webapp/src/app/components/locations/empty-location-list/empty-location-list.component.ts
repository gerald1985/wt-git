import { Component, ViewEncapsulation, ChangeDetectionStrategy, Output, EventEmitter } from '@angular/core';
import { MdDialog } from '@angular/material';
import { ReadStore } from '../../../generated/model/ReadStore';
import {AuthService} from '../../../services/auth.service';

@Component({
	selector: 'app-empty-location-list',
	templateUrl: './empty-location-list.component.html',
	styleUrls: ['./empty-location-list.component.scss'],
	encapsulation: ViewEncapsulation.None,
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class EmptyLocationListComponent {

	@Output() onCreate = new EventEmitter<ReadStore>();

	isAdmin = this.authService.isAdmin();

	constructor(public dialog: MdDialog, private authService: AuthService) {}

	addLocation() {
		this.onCreate.emit();
	}
}
