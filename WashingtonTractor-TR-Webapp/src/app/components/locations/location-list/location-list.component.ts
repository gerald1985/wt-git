import { Component, ViewEncapsulation, ChangeDetectionStrategy, Input, Output, EventEmitter } from '@angular/core';
import {MdDialog} from '@angular/material';
import {AddLocationComponent} from '../add-location/add-location.component';
import { ReadStore } from '../../../generated/model/ReadStore';
import {AuthService} from "../../../services/auth.service";

@Component({
	selector: 'app-location-list',
	templateUrl: './location-list.component.html',
	styleUrls: ['./location-list.component.scss'],
	encapsulation: ViewEncapsulation.None,
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class LocationListComponent {

	@Input() locations: Array<ReadStore> = [];
	@Output() onDelete = new EventEmitter<ReadStore>();
	@Output() onEdit = new EventEmitter<ReadStore>();
	@Output() onRestore = new EventEmitter<ReadStore>();

	editLocation: ReadStore;

	isAdmin = this.authService.isAdmin();

	constructor(private authService: AuthService) {}

	setEditLocation(location) {
		this.editLocation = location;
	}

	create() {
		this.onEdit.emit();
	}

	restore(location: ReadStore) {
		this.onRestore.emit(location);
	}

	edit() {
		if (!this.editLocation) return;
		this.onEdit.emit(this.editLocation);
		this.editLocation = null;
	}

	delete() {
		if (!this.editLocation) return;
		this.onDelete.emit(this.editLocation);
		this.editLocation = null;
	}

}
