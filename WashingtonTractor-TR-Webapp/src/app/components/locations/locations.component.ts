import { Component, OnInit, ViewEncapsulation, ChangeDetectionStrategy, Inject, OnDestroy } from '@angular/core';
import { ReadStore } from '../../generated/model/ReadStore';
// import * as readStoreApi from '../../generated/model/ReadStore';
import { MdDialog } from '@angular/material';
import { AddLocationComponent } from './add-location/add-location.component';
import { StoreApi } from '../../generated/api/StoreApi';
import { DeleteSubscriptionStore } from '../../classes/delete-subscription-store';
import { MessageService } from '../../services/message.service';
import { CommonError } from '../../generated/model/CommonError';
import {ListOfStoresApi} from '../../generated/api/ListOfStoresApi';

@Component({
	selector: 'app-locations',
	templateUrl: './locations.component.html',
	styleUrls: ['./locations.component.scss'],
	encapsulation: ViewEncapsulation.None
})
export class LocationsComponent implements OnInit, OnDestroy {

	locations: Array<ReadStore> = [];
	locationProcessing = true;

	private locationsToDelete = new DeleteSubscriptionStore<ReadStore>();

	constructor(
		@Inject('ListOfStoresApi') private listStoreApi: ListOfStoresApi,
		private dialog: MdDialog,
		private storeApi: StoreApi,
		private message: MessageService
	) {}

	handleEdit(location: ReadStore) {
		let dialogRef = this.dialog.open(AddLocationComponent, {
			width: '560px',
		});
		dialogRef.componentInstance.setLocation(location);

		dialogRef.afterClosed().subscribe(result => {
			this.loadStores();
		});
	}

	changeUserStatus(location: ReadStore) {
		this.locations = this.locations.map(listLocation => {
			if (listLocation.id !== location.id ) return listLocation;
			return this.locationsToDelete.assingRemovedStatus(listLocation);
		});
	}

	handleDelete(user: ReadStore) {
		this.locationsToDelete.subscribe(user);
		this.changeUserStatus(user);
	}

	handleRestore(user: ReadStore) {
		this.locationsToDelete.unsubscribe(user);
		this.changeUserStatus(user);
	}

	loadStores() {
		this.listStoreApi.storesGet().subscribe(response => {
			this.locations = response.data;
			this.locationProcessing = false;
		});
	}

	ngOnInit() {
		this.loadStores();
			this.locationsToDelete.onDelete().subscribe(location => {
			this.storeApi.storeIdDelete(location.id).subscribe(response => {
				this.loadStores();
			}, errorResponse => {
				try {
					let error = <CommonError>errorResponse.json();
					this.message.showMessage(error.errorMessage.message);
				} catch (e) {
					this.message.showMessage(`Unknown error while deleting location ${location.name}`);
				} finally {
					this.changeUserStatus(location);
				}
			});
		});
	}

	ngOnDestroy() {
		this.locationsToDelete.cancelAll();
	}

}
