import { Component, ViewEncapsulation, ChangeDetectionStrategy, OnInit, Inject } from '@angular/core';
import { MdDialogRef } from '@angular/material';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ReadStore } from '../../../generated/model/ReadStore';
import { WriteStore } from '../../../generated/model/WriteStore';
import { ListOfUsersApi } from '../../../generated/api/ListOfUsersApi';
import { ReadUser } from '../../../generated/model/ReadUser';
import { StoreApi } from '../../../generated/api/StoreApi';
import { Observable } from 'rxjs/Observable';
import { InlineResponse2005 } from '../../../generated/model/InlineResponse2005';
import { CommonError } from '../../../generated/model/CommonError';
import { ListOfStoresApi } from '../../../generated/api/ListOfStoresApi';
import { Response } from '@angular/http';
import { MessageService } from '../../../services/message.service';

@Component({
	selector: 'app-add-location',
	templateUrl: './add-location.component.html',
	styleUrls: ['./add-location.component.scss'],
	encapsulation: ViewEncapsulation.None,
	changeDetection: ChangeDetectionStrategy.Default
})
export class AddLocationComponent implements OnInit {

	locationForm: FormGroup;

	edit = false;

	managers: ReadUser = [];

	errors: any = {};

	private numbersRe = /[^0-9]+/;

	constructor(
		private dialogRef: MdDialogRef<AddLocationComponent>,
		private userListApi: ListOfUsersApi,
		private storeApi: StoreApi,
		private message: MessageService,
		@Inject('ListOfStoresApi') private listStoreApi: ListOfStoresApi
	) {
		this.userListApi.usersGet(100, 1, ['REGISTERED'], ['ROLE_MANAGER_STORE']).subscribe(response => {
			this.managers = response.data;
		});
	}

	ngOnInit() {}

	handleSubmit({value, valid}: {value: any, valid: boolean}) {
		Object.keys(this.locationForm.controls).forEach(key => {
			this.locationForm.get(key).markAsDirty();
		});
		if (!valid) return;
		this.errors = {};
		let save$: Observable<any>;
		value.number = parseInt(value.number, 10);
		if (value.id) {
			save$ = this.storeApi.storePut(value);
		} else {
			save$ = this.storeApi.storePost(value);
		}
		save$.subscribe((response: InlineResponse2005) => {
			this.dialogRef.close(response.data);
		}, errorResponse => {
			let error = <CommonError>errorResponse.json();
			if (!error.errorMessage.attributes) {
				this.message.showMessage(error.errorMessage.message);
				return;
			};
			error.errorMessage.attributes.forEach(fieldError => {
				let formControl = this.locationForm.get(fieldError.field);
				if (!formControl) {
					fieldError.errors.forEach(message => {
						this.message.showMessage(message);
					});
					return;
				}
				this.errors[fieldError.field] = fieldError.errors;
				formControl.setErrors({'serverError': true});
			});
		});
	};

	filterNumber(e?: KeyboardEvent) {
		if (e && e.key && this.numbersRe.test(e.key)) {
			if (e.keyCode === 46 || e.keyCode === 8) return; // do nothing if backspace or delete is pressed
			e.preventDefault();
			return;
		}
		let numberControl = this.locationForm.get('number');
		numberControl.setValue(numberControl.value.toString().replace(this.numbersRe, ''));
	}

	setLocation(location: ReadStore) {
		if (!location) {
			location = {
				number: null,
				name: '',
				address: '',
				manager: null
			};
			this.listStoreApi.storesMaxNumberGet().subscribe(response => {
				this.locationForm.get('number').setValue(response['data'].maxNumber + 1);
			});
		};
		this.edit = !!location.id;
		this.locationForm = new FormGroup({
			id: new FormControl(location.id || null),
			number: new FormControl(location.number || null, Validators.required),
			name: new FormControl(location.name || '', [Validators.required]),
			address: new FormControl(location.address || '', Validators.required),
			managerId: new FormControl(location.manager ? location.manager.id : null)
		});
	}

}
