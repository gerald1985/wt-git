import { Component, OnInit, ViewEncapsulation, ChangeDetectionStrategy, Input } from '@angular/core';

@Component({
	selector: 'app-tooltip',
	template: '<div class="tooltip-holder"><ng-content></ng-content><div class="mat-tooltip">{{tooltip}}</div></div>',
	styleUrls: ['./tooltip.component.scss'],
	encapsulation: ViewEncapsulation.None,
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class TooltipComponent implements OnInit {

	constructor() { }

	@Input() tooltip: string;

	ngOnInit() {
	}

}
