
import {
	Component, ViewEncapsulation, ChangeDetectionStrategy, Input, OnInit, ChangeDetectorRef,
	OnDestroy
} from '@angular/core';
import { MdDialog } from '@angular/material';
import { ChangePassComponent } from './../change-pass/change-pass.component';
import { AuthService } from '../../services/auth.service';
import {Subscription} from 'rxjs';

@Component({
	selector: 'app-user-menu',
	templateUrl: './user-menu.component.html',
	styleUrls: ['./user-menu.component.scss'],
	encapsulation: ViewEncapsulation.None,
	changeDetection: ChangeDetectionStrategy.OnPush
})
export class UserMenuComponent implements OnInit, OnDestroy {
	username: string;
	subscription: Subscription;

	constructor(public dialog: MdDialog, public auth: AuthService,  private cd: ChangeDetectorRef) {}

	openChangePass() {
		let dialogRef = this.dialog.open(ChangePassComponent, {
			width: '480px',
		});

		dialogRef.afterClosed().subscribe(result => {
			// console.log(result);
		});
	}

	ngOnInit(): void {
		this.subscription = this.auth.onTokenChange().subscribe(auth => {
			if (!auth) return;
			let user = auth.user;
			this.username = user.firstName + ' ' + user.lastName;
			this.cd.detectChanges();
		});
	}

	ngOnDestroy(): void {
		this.subscription.unsubscribe();
	}



	handleLogout() {
		this.auth.logout();
	}
	// ngOnInit() {}
}
