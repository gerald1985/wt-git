import { RequestsComponent } from './components/requests/requests.component';
import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {LoginComponent} from './components/login/login.component';
import {UserSetupComponent} from './components/user-setup/user-setup.component';
import { LocationsComponent } from './components/locations/locations.component';
import { EquipmentListComponent } from './components/equipment-list/equipment-list.component';
import { CanActivateViaAuthGuard } from './services/can-activate-via-auth-guard';

const routes: Routes = [
	{
		path: 'login',
		component: LoginComponent,
		canActivate: [CanActivateViaAuthGuard]
	},
	{
		path: 'login/:token',
		component: LoginComponent,
		canActivate: [CanActivateViaAuthGuard]
	},
	{
		path: 'requests',
		component: RequestsComponent,
		canActivate: [CanActivateViaAuthGuard]
	},
	{
		path: 'user-setup',
		component: UserSetupComponent,
		canActivate: [CanActivateViaAuthGuard]
	},
	{
		path: 'locations',
		component: LocationsComponent,
		canActivate: [CanActivateViaAuthGuard]
	},
	{
		path: 'equipment',
		component: EquipmentListComponent,
		canActivate: [CanActivateViaAuthGuard]
	},
	{
		path: '',
		redirectTo: '/requests', // '/login'
		pathMatch: 'full'
	}
];

@NgModule({
	imports: [RouterModule.forRoot(routes, {useHash: true})],
	exports: [RouterModule]
})
export class AppRoutingModule {}
