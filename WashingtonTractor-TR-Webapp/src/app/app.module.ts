import { AppRoutingModule } from './app-routing-module';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { DatepickerModule } from './datepicker/datepicker.module';

import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';

import { RequestsComponent } from './components/requests/requests.component';
import { UserMenuComponent } from './components/user-menu/user-menu.component';

import { MaterialModule } from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { PasswordRestoreComponent } from './components/login/password-restore/password-restore.component';
import { AccountCreateComponent } from './components/login/account-create/account-create.component';
import { NewPasswordComponent } from './components/login/new-password/new-password.component';
import { RequestListComponent } from './components/requests/request-list/request-list.component';
import { ChangePassComponent } from './components/change-pass/change-pass.component';
import { NewRequestComponent } from './components/requests/new-request/new-request.component';
import { TopBarComponent } from './components/top-bar/top-bar.component';
import { UserSetupComponent } from './components/user-setup/user-setup.component';
import { UserRegRequestComponent } from './components/user-setup/user-reg-request/user-reg-request.component';
import { UserListComponent } from './components/user-setup/user-list/user-list.component';
import { EditUserItemComponent } from './components/user-setup/user-list/edit-user-item/edit-user-item.component';
import { LocationsComponent } from './components/locations/locations.component';
import { LocationListComponent } from './components/locations/location-list/location-list.component';
import { EmptyLocationListComponent } from './components/locations/empty-location-list/empty-location-list.component';
import { AddLocationComponent } from './components/locations/add-location/add-location.component';
import { DeclineComponent } from './components/user-setup/user-reg-request/decline/decline.component';
import { EquipmentListComponent } from './components/equipment-list/equipment-list.component';
import { EquipmentSettingsComponent } from './components/equipment-list/equipment-settings/equipment-settings.component';
import { EquipmentFilterComponent } from './components/equipment-list/equipment-filter/equipment-filter.component';
import { EquipmentListTableComponent } from './components/equipment-list/equipment-list-table/equipment-list-table.component';
import { PopupNoticeComponent } from './components/user-setup/popup-notice/popup-notice.component';
import { DeclineRequestComponent } from './components/requests/decline-request/decline-request.component';


import { BASE_PATH, COLLECTION_FORMATS } from './generated/variables';
import { Configuration } from './generated/configuration';
import { ListOfUsersApi } from './generated/api/ListOfUsersApi';
import { APIS } from './generated/api/api';
import { AppHttpService } from './services/app-http.service';
import { UserApi } from './generated/api/UserApi';
import { ListOfStoresApi } from './generated/api/ListOfStoresApi';
import { AuthService } from './services/auth.service';
import { CookieService } from './services/cookie.service';

import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinct';
import 'rxjs/add/operator/delay';
import 'rxjs/add/operator/share';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/filter';
import 'rxjs/add/observable/of';
import 'rxjs/add/observable/from';
import 'rxjs/add/observable/fromEvent';
import 'rxjs/add/observable/interval';
import 'rxjs/add/observable/merge';

import { CanActivateViaAuthGuard } from './services/can-activate-via-auth-guard';
import { StoreApi } from './generated/api/StoreApi';
import { TooltipComponent } from './components/tooltip/tooltip.component';
import { RequestSidebarComponent } from './components/requests/request-sidebar/request-sidebar.component';
import { RequestStatusComponent } from './components/requests/request-status/request-status.component';
import { RequestInfoComponent } from './components/requests/request-sidebar/request-info/request-info.component';
import { MessageComponent } from './components/message/message.component';
import { MessageService } from './services/message.service';
import { RequestsFilterComponent } from './components/requests/requests-filter/requests-filter.component';
import { ItemSearchComponent } from './components/requests/item-search/item-search.component';
import { ItemSelectComponent } from './components/requests/item-search/item-select.component';
import { TransferRequestApi } from './generated/api/TransferRequestApi';
import { ListOfTransferRequestsApi } from './generated/api/ListOfTransferRequestsApi';
import { RequestService } from './services/request.service';
import { UsernameComponent } from './components/requests/username/username.component';
import { ItemSearchItemComponent } from './components/requests/item-search/item-search-item.component';
import { RequestNewCommentComponent } from './components/requests/request-new-comment/request-new-comment.component';
import { RequestNoteComponent } from './components/requests/request-note/request-note.component';
import { ListOfItemsApi } from './generated/api/ListOfItemsApi';
import { EquipmentItemDetailsComponent } from './components/equipment-list/equipment-item-details/equipment-item-details.component';
import {ItemApi} from './generated/api/ItemApi';
import { SearchComponentComponent } from './components/search/search-component.component';


@NgModule({
	declarations: [
		AppComponent,
		LoginComponent,
		SidebarComponent,
		RequestsComponent,
		UserMenuComponent,
		PasswordRestoreComponent,
		AccountCreateComponent,
		NewPasswordComponent,
		RequestsFilterComponent,
		RequestListComponent,
		ChangePassComponent,
		NewRequestComponent,
		TopBarComponent,
		UserSetupComponent,
		UserRegRequestComponent,
		UserListComponent,
		EditUserItemComponent,
		LocationsComponent,
		LocationListComponent,
		EmptyLocationListComponent,
		AddLocationComponent,
		DeclineComponent,
		EquipmentListComponent,
		EquipmentSettingsComponent,
		EquipmentFilterComponent,
		EquipmentListTableComponent,
		PopupNoticeComponent,
		TooltipComponent,
		RequestSidebarComponent,
		RequestStatusComponent,
		RequestInfoComponent,
		MessageComponent,
		DeclineRequestComponent,
		ItemSearchComponent,
		ItemSelectComponent,
		RequestNewCommentComponent,
		UsernameComponent,
		RequestNoteComponent,
		ItemSearchItemComponent,
		EquipmentItemDetailsComponent,
		SearchComponentComponent,
	],
	entryComponents: [
		PasswordRestoreComponent,
		AccountCreateComponent,
		NewPasswordComponent,
		ChangePassComponent,
		NewRequestComponent,
		EditUserItemComponent,
		AddLocationComponent,
		DeclineComponent,
		DeclineRequestComponent,
		EquipmentItemDetailsComponent
	],
	imports: [
		BrowserModule,
		BrowserModule,
		FormsModule,
		ReactiveFormsModule,
		HttpModule,
		AppRoutingModule,
		MaterialModule,
		BrowserAnimationsModule,
		DatepickerModule
	],
	providers: [
		Configuration,
		AppHttpService,
		AuthService,
		ListOfUsersApi,
		StoreApi,
		CookieService,
		CanActivateViaAuthGuard,
		ListOfItemsApi,
		MessageService,
		UserApi,
		ItemApi,
		TransferRequestApi,
		ListOfTransferRequestsApi,
		RequestService,
		{ provide: 'ListOfStoresApi', useClass: ListOfStoresApi },
		{ provide: BASE_PATH, useValue: BASE_PATH},
		{ provide: 'COLLECTION_FORMATS', useValue: COLLECTION_FORMATS},
		{ provide: 'APIS', useValue: APIS}
	],
	bootstrap: [AppComponent]
})
export class AppModule {  }
