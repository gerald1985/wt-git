import { ValidatorFn, AbstractControl, FormControl } from '@angular/forms';
export function passwordMatchValidator(pass1: AbstractControl): ValidatorFn {
	return (control: AbstractControl): {[key: string]: any} => {
		return pass1.value !== control.value ? {'passwordMatchError': 'Passwords do not match'} : null;
	};
}
