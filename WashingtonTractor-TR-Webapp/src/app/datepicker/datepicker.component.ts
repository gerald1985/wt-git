import {
  animate, Component, ElementRef, EventEmitter, Input, keyframes, OnChanges,
  OnInit, Output, Renderer, SimpleChange, state, style, transition, trigger,
  ViewEncapsulation
} from '@angular/core';
import { FormControl, Validators } from '@angular/forms';

import * as moment from 'moment';
import { DatepickerCalendarComponent } from './datepicker-calendar.component';
import { ViewChild, ChangeDetectorRef } from '@angular/core';
import { Observable } from 'rxjs/Observable';

interface ValidationResult {
  [key: string]: boolean;
}

@Component({
  selector: 'material-datepicker',
  encapsulation: ViewEncapsulation.None,
  styles: [`
    .datepicker {
      position: relative;
      display: inline-block;
      color: #2b2b2b;
      font-family: 'Helvetica Neue', 'Helvetica', 'Arial', 'Calibri', 'Roboto';
    }
    .datepicker__input {
      outline: none;
      border-radius: 0.1rem;
      padding: .2em .6em;
      font-size: 14px;
    }
  `],
  template: `
    <div class="datepicker">
      <input
        [disabled]="disabled"
        class="datepicker__input"
        [placeholder]="placeholder"
        (click)="onInputClick()"
        [(ngModel)]="inputText"
        readonly="true"
				[ngClass]="{selected: date[0] || date[1]}"
      >
      <material-datepicker-calendar
        [maxDate]="maxDate"
        [minDate]="minDate"
				[showRangeSwithcher]="showRangeSwithcher"
        [showMonths]="showMonths"
        [dates]="dates"
				[isRange]="isRange"
        (onSelect)="onSelectDay($event)"
				(onRangeChange)="handleIsRangeChange($event)"
        [showCalendar]="showCalendar"></material-datepicker-calendar>
    </div>
    `
})
export class DatepickerComponent implements OnInit, OnChanges {
  private readonly DEFAULT_FORMAT = 'MM/DD/YYYY';

  dates: Array<moment.Moment> = [];

  @Input() date: Array<Date> = [];
  // api bindings
  @Input() disabled: boolean;
  @Input() dateFormat: string = this.DEFAULT_FORMAT;
  @Input() maxDate: Date;
  @Input() minDate: Date;
  @Input() isRange: boolean = false;
	@Input() isRangeMonths = 2;
	@Input() isNotRangeMonths = 1;
  // data
  @Input() placeholder: string = 'Select a date';
  @Input() inputText: string;
  // view logic
  @Input() showCalendar: boolean = false;
  @Input() showMonths: number = 2;
  // events
  @Output() onSelect = new EventEmitter<Date>();
  @Output() onDateChange = new EventEmitter<Array<Date>>();
	@Input() showRangeSwithcher = false;

  // listeners
  clickListener: Function;

  constructor(private renderer: Renderer, private elementRef: ElementRef, private cd: ChangeDetectorRef) {
    // listeners
    this.clickListener = renderer.listenGlobal(
      'window',
      'click',
      (event: MouseEvent) => this.handleGlobalClick(event)
    );
		elementRef.nativeElement.addEventListener('click', e => {
			e.stopPropagation();
		});
  }

  ngOnInit() {}

  ngOnChanges(changes: { [propertyName: string]: SimpleChange }) {
    this.dates = (this.date || []).map(date => moment(date));
    this.syncVisualsWithDate();
  }

  ngOnDestroy() {
    this.clickListener();
  }

  closeCalendar(): void {
    this.showCalendar = false;
    this.syncVisualsWithDate();
  }

	handleIsRangeChange(value) {
		this.isRange = value;
		if (value) {
			this.showMonths = this.isRangeMonths;
		} else {
			this.showMonths = this.isNotRangeMonths;
		}
	}


  /**
  * Visually syncs calendar and input to selected date or current day
  */
  syncVisualsWithDate(): void {
    if (this.date) {
      this.setInputText(this.dates);
    } else {
      this.inputText = '';
    }
  }

  /**
  * Sets the visible input text
  */
  setInputText(dates: Array<moment.Moment>): void {
    let inputText = '';
    const format = this.dateFormat || this.DEFAULT_FORMAT;
    this.inputText = dates.map(date => {
      return date.format(format);
    }).join(' - ');
  }

  /**
  * Closes the calendar when the cancel button is clicked
  */
  onCancel(): void {
    this.closeCalendar();
  }

  /**
  * Toggles the calendar when the date input is clicked
  */
  onInputClick(): void {
    this.showCalendar = !this.showCalendar;
  }

  onSelectDay(day: moment.Moment): void {
    if (!this.isDateValid(day)) return;
    this.onSelect.emit(day.toDate());
    if (!this.isRange) {
      this.onDateChange.emit([day.toDate()]);
      this.closeCalendar();
      return;
    }

    let dates = this.dates.slice();
    if (!dates[0]) {
      dates[0] = day;
    } else if (day.isSameOrBefore(dates[0])) {
      dates = [day, dates[0]];
    } else {
      dates[1] = day;
    }
    this.onDateChange.emit(dates.map(date => date.toDate()));
  }

  isDateValid(day: moment.Moment): boolean {
    if (this.maxDate && this.minDate) {
      return day.isSameOrAfter(this.minDate, 'day') && day.isSameOrBefore(this.maxDate, 'day');
    }
    if (this.minDate) {
      return day.isSameOrAfter(this.minDate, 'day');
    }
    if (this.maxDate) {
      return day.isSameOrBefore(this.maxDate, 'day');
    }
    return true;
  }

  /**
  * Closes the calendar if a click is not within the datepicker component
  */
  handleGlobalClick(event: MouseEvent): void {
		this.closeCalendar();
		this.cd.detectChanges();
  }

}
